package com.pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;


public class AccountPage {

    private By apiUrl = By.id("qb_api");
    private By chatUrl = By.id("chat_api");

    public String getApiUrl() {return  $(apiUrl).getValue();}

    public String getChatUrl(){
        return  $(chatUrl).getValue();
    }

}
