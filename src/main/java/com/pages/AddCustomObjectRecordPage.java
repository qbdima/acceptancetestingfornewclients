package com.pages;


import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class AddCustomObjectRecordPage extends AbstractPage{
    private By parendIdField =By.id("new_record__parent_id");
    private By nameField =By.id("new_record_name");
    private By descriptionField =By.id("new_record_description");
    private By ratingField =By.id("new_record_rating");
    private By addRecordButton =By.xpath("//*[@id='record_add']/div[4]/input");


    public void enterParentId(String id){
        $(parendIdField).setValue(id);
    }
    public void enterName(String name){
        $(nameField).setValue(name);
    }
    public void enterDescription(String description){
        $(descriptionField).setValue(description);
    }
    public void enterRating(String rating){
        $(ratingField).setValue(rating);
    }
    public void clickAddRecord(){
        $(addRecordButton).click();
    }
    public void createRecord(String id, String description){
        enterParentId(id);
        enterDescription(description);
        clickAddRecord();
    }
}
