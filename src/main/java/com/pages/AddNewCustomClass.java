package com.pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;


public class AddNewCustomClass {

    private By className = By.xpath("//*[@id='custom_class_name']");
    private  By fieldName1 = By.xpath("//*[@id='custom_field__name']");
    private  By fieldName2 = By.xpath("//div[3]/div/input");
    private  By fieldName3 = By.xpath("//div[4]/div/input");
    private  By fieldType1 = By.xpath("//div[2]/select");
    private  By fieldType2 = By.xpath("//div[3]/div[2]/select");
    private  By fieldType3 = By.xpath("//div[4]/div[2]/select");
    private  By addField = By.xpath("//*[@id='add_button']");
    private  By createClass = By.xpath("//*[@id='new_custom_class_dialog']//div[3]/input");

    public void enterClassName(String name){
        $(className).setValue(name);
    }
    public void enterFieldName1(String name){
        $(fieldName1).setValue(name);
    }
    public void enterFieldName2(String name){
        $(fieldName2).setValue(name);
    }
    public void enterFieldName3(String name){
        $(fieldName3).setValue(name);
    }
    public void chooseFieldType1(String type){
        $(fieldType1).selectOption(type);
            }
    public void chooseFieldType2(String type){
        $(fieldType2).selectOption(type);
    }
    public void chooseFieldType3(String type){
        $(fieldType3).selectOption(type);
    }
    public void clickAddField(){
        $(addField).click();
    }
    public void clickCreateClass(){
        $(createClass).click();
    }

    public void createClass(String name, String fieldName1, String fieldName2, String fieldName3, String type1, String type2, String type3) {
        clickAddField();
        clickAddField();
        enterClassName(name);
        enterFieldName1(fieldName1);
        enterFieldName2(fieldName2);
        enterFieldName3(fieldName3);
        chooseFieldType1(type1);
        chooseFieldType2(type2);
        chooseFieldType3(type3);
        clickCreateClass();
    }

}
