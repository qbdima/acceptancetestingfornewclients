package com.pages;


import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;
import static com.codeborne.selenide.Selenide.switchTo;

public class ApplicationPage extends  BasePage{
    private By appName = By.cssSelector(".container ul.breadcrumb li.selected");
    private By appId = By.id("app_id");
    private By appAuthKey = By.id("app_auth_key");
    private By appSecretKey = By.id("app_auth_secret");
    private By removeButton = By.cssSelector("a.btn.btn-danger.pull-right");

    private By settingsTab = By.xpath("//*[@class='module-navigation-wrap']//ul//li[2]/a");
    private By userCheckbox = By.id("application_settings_users_index");
    private By saveButton = By.xpath("//*[@id='form-add-app']/input");



    public String getAppName(){ return  $(appName).text();}

    public String getAppId(){
        return  $(appId).getValue();
    }

    public String getAuthKey(){
        return  $(appAuthKey).getValue();
    }

    public String getAuthSecret(){
        return  $(appSecretKey).getValue();
    }


    public void openApplicationSettingTab(){
        $(settingsTab).click();   }

    public void setListUsersCheckbox(){
        SelenideElement checkbox = $(userCheckbox);

        if(!checkbox.isSelected()){
            checkbox.setSelected(true);
            saveSettings();
        }
    }

        public void saveSettings(){
            $(saveButton).click();
        }

        public void removeApp(){
            $(removeButton).click();
            switchTo().alert().accept();
        }
}
