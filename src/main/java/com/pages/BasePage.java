package com.pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class BasePage extends AbstractPage {

    private By chatPage = By.xpath("//*[@id='app-list']/a[2]");
    private By contentPage = By.xpath("//*[@id='app-list']/a[3]");
    private By customPage = By.xpath("//*[@id='app-list']/a[4]");
    private By locationPage = By.xpath("//*[@id='app-list']/a[5]");
    private By pushNotificationsPage = By.xpath("//*[@id='app-list']/a[6]");
    private By usersPage = By.xpath("//*[@id='app-list']/a[7]");
    public ChatPage chatPageClick(){
        $(chatPage).click();
        return page(ChatPage.class);
    }

    public ContentPage contentPageClick(){
        $(contentPage).click();
        return page(ContentPage.class);
    }

    public CustomPage customPageClick(){
        $(customPage).click();
        return page(CustomPage.class);
    }

    public LocationPage locationPageClick(){
        $(locationPage).click();
        return page(LocationPage.class);
    }

    public PushNotificationsPage pushNotificationsPageClick(){
        $(pushNotificationsPage).click();
        return page(PushNotificationsPage.class);
    }

    public UsersPage usersPageClick(){
        $(usersPage).click();
        return page(UsersPage.class);
    }

}
