package com.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;


public class ChatAlertPage {

    public enum ALERT_TEMPLATE {
        UNREAD_COUNT, SENDER_FULLNAME, CUSTOM
    }

    public enum BADGE_COUNTER {
        ONE, UNREAD, NO
    }

    public enum SOUND {
        DEFAULT, NO_SOUND, CUSTOM
    }

    private By unreadCountTemplate = By.id("chat_alert_type_template_1");
    private By senderFullnameTemplate = By.id("chat_alert_type_template_2");
    private By customTemplate = By.id("chat_alert_type_template_3");
    private By customTemplateText = By.id("chat_alert_text");

    private By badgeCounterOne = By.id("chat_alert_badge_1");
    private By badgeCounterUnread = By.id("chat_alert_badge_2");
    private By badgeCounterNo = By.id("chat_alert_badge_0");

    private By defaultSound = By.id("chat_alert_sound_1");
    private By noSound = By.id("chat_alert_sound_2");
    private By customSound = By.id("chat_alert_sound_3");
    private By customSoundFile = By.id("chat_alert_custom_sound");

    private By categoryName = By.xpath("//*[@id='chat_alert_category']/input");

    private By contentAvailableFlag = By.id("chat_alert_content_availible");

    private By saveBtn = By.xpath("//*[@id='save_btn_chat_alert']/input");

    private By successMessage = By.xpath("//*[@id='update-result']");


    private void save() {
        $(saveBtn).click();
    }

    public void setTemplate(ALERT_TEMPLATE alertTemplate) {
        switch (alertTemplate){
            case UNREAD_COUNT:
                $(unreadCountTemplate).click();
                save();
                break;
            case SENDER_FULLNAME:
                $(senderFullnameTemplate).click();
                save();
                break;
            case CUSTOM:
                $(customTemplate).click();
                save();
                break;
        }
    }

    public void setBadgeCounter(BADGE_COUNTER badgeCounter) {
        switch (badgeCounter) {
            case ONE:
                $(badgeCounterOne).click();
                save();
                break;
            case UNREAD:
                $(badgeCounterUnread).click();
                save();
                break;
            case NO:
                $(badgeCounterNo).click();
                save();
                break;
        }
    }

    public void setSound(SOUND sound) {
        switch (sound){
            case DEFAULT:
                $(defaultSound).click();
                save();
                break;
            case NO_SOUND:
                $(noSound).click();
                save();
                break;
            case CUSTOM:
                $(customSound).click();
                save();
                break;
        }
    }

    public void setCustomTemplate(String template) {
        $(customTemplateText).setValue(template);
        save();
    }

    public void setCustomSound(String sound) {
        $(customSoundFile).setValue(sound);
        save();
    }

    public void setCategory(String category) {
        $(categoryName).setValue(category);
        save();
    }

    public void setContentAvailable() {
        SelenideElement checkbox = $(contentAvailableFlag);

        if (!checkbox.isSelected()) {
            checkbox.setSelected(true);
            save();
        }
    }

//    public void setTemplate2() {
////        $(By.id("chat_alert_type_template_2")).click();
//        $(By.id("chat_alert_type_template_3")).click();
//        $(By.id("chat_alert_badge_2")).click();
//        //  $(By.xpath("//*[@id='save_btn_chat_alert']/input")).click();
//    }
//
//    public void setTemplateText() {
//        $(By.id("chat_alert_text")).setValue("Stage 3: You have %unread_count% new messages");
//        //  $(By.xpath("//*[@id='save_btn_chat_alert']/input")).click();
//    }
//
//    public void enterCategory(String category) {
//        $(By.xpath("//*[@id='chat_alert_category']/input")).setValue(category);
//        //  $(By.xpath("//*[@id='save_btn_chat_alert']/input")).click();
//    }


}
