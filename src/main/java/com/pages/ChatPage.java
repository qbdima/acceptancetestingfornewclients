package com.pages;


import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class ChatPage extends BasePage {

    private By alertsTab = By.xpath("//*[@class='module-navigation-wrap']//ul//li[2]/a");
    private By addDialogBut = By.xpath("//*[@id='dialogs_list_wrapper']/div[3]/input");
    private By nameField = By.xpath("//*[@id='chat_dialog_name']");
    private By publicRadioBut = By.id("chat_dialog_type_1");
    private By createButton = By.id("dialog_submit");
    private By groupRadioBut = By.id("chat_dialog_name");
    private By occupantsIDField = By.id("chat_dialog_occupants_ids");
    private By successMessage = By.cssSelector("div.content");
    private By lastChat = By.cssSelector("tbody tr:last-child td:first-child input");
    private By removeSelected = By.cssSelector(".remove");
    private By recordsNumber = By.name("dialogs_list_length");
    private By listOfChats = By.cssSelector("tbody td:nth-child(2)");


    public ChatAlertPage openAlertsTab(){
        $(alertsTab).click();
        return page(ChatAlertPage.class);
    }

    public void newChatPopUpOpen() {
        $(addDialogBut).click();
    }
    public Boolean newChatButtonVisible(){
        $(addDialogBut).isDisplayed();
        return true;
    }

    public void choose100records(String records){
       $(recordsNumber).selectOption(records);
    }

    public void enterName(String chatName) {
        $(nameField).setValue(chatName);
    }

    public void selectPublicChat() {
        $(publicRadioBut).click();
    }

    public void selectGroupChat() {
        $(groupRadioBut).click();
    }

    public void enterOccupantsIDs(String users) {
        $(occupantsIDField).setValue(users);
    }

    public void createChatBut() {
        $(createButton).click();
    }
    public String getSuccessMessage(){
        return $(successMessage).text();}

    public void createPublicChat(String name) {
        newChatPopUpOpen();
        enterName(name);
        selectPublicChat();
        createChatBut();
    }

    public void createGroupChat(String name,  String users) {
        newChatPopUpOpen();
        enterName(name);
        selectGroupChat();
        enterOccupantsIDs(users);
        createChatBut();
    }

    public int countChats(){
        return  $$(listOfChats).size();

    }

    public void removeChat() {
        $(lastChat).click();
        $(removeSelected).click();
        switchTo().alert().accept();
    }
}
