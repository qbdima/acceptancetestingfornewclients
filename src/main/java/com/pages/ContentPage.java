package com.pages;


import org.openqa.selenium.By;

import java.io.File;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.switchTo;

public class ContentPage extends BasePage {
    private By browseButton = By.id("blob_item");
    private By uploadButton = By.id("upload_submit");
    private By successMessage = By.id("uploading-result");
    private By allContent = By.cssSelector("tbody td:nth-child(2)");
    private By removeSelected = By.cssSelector(".remove");
    private By lastContent = By.cssSelector("#blobs_list tbody tr:last-child td:first-child input");
    private By recordsNumber = By.name("blobs_list_length");

    public Boolean browseButtonVisible(){
        $(browseButton).isDisplayed();
        return true;
    }

    public void choose100records(String records){
        $(recordsNumber).selectOption(records);
    }


    public void  uploadFile (String filePath){
        $(browseButton).uploadFile(new File(filePath));
        $(uploadButton).click();
    }
    public String getSuccessMessage(){return
            $(successMessage).text();}


    public void removeContent(){
        $(lastContent).click();
        $(removeSelected).click();
        switchTo().alert().accept();
    }


    public int countContent(){
        return  $$(allContent).size();

    }


}
