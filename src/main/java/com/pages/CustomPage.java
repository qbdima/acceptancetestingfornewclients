package com.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.utils.Consts;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.*;

public class CustomPage extends BasePage{
    private By addButton=By.id("add");
    private By newClass =By.xpath("//*[@id='add_menu']//li[1]/a");
    private By newRecord =By.cssSelector("#add_menu ul.dropdown-menu li:nth-of-type(2)");
    private By selectClass =By.id("class_name");
    private By exportTab = By.xpath("//li[2]/a/i");
    private By importTab = By.xpath("//li[3]/a/i");
    private By lastRecordCheckbox =By.cssSelector("tbody tr:last-child td:first-child input");
    private By recordName =By.cssSelector("tbody tr:last-child td:nth-child(5)");
    private By lastRecord =By.cssSelector("tbody tr:last-child td:nth-child(2) a");
    private By records = By.cssSelector("tbody td.sorting_1:nth-child(2)");
    private By removeSelectedRecord =By.cssSelector("a.remove");
    private By recordsNumber = By.xpath("//*[@id='custom_table_length']/label/select");
    private By listOfClasses = By.cssSelector("#class_name option");
    private By tableHead = By.cssSelector(".dataTables_scrollHead");

    public String getTableHeadText(){
      return   $(tableHead).text();
    }

    public Boolean addButtonVisible(){
        $(addButton).isDisplayed();
        return true;
    }

    public AddNewCustomClass addClass() {
        $(addButton).click();
        $(newClass).click();
        return page(AddNewCustomClass.class);
    }

    public AddCustomObjectRecordPage addRecord() {
        $(addButton).click();
        $(newRecord).click();
        return page(AddCustomObjectRecordPage.class);
    }

    public String getRecordName() {
        return $(recordName).getText();
    }

    public void clickLastRecord() {
        $(lastRecord).click();
    }

    public void clickExport() {
        $(exportTab).click();
    }

    public void clickImport() {
        $(importTab).click();
    }

    public boolean classIsCreated() {

    return $(By.xpath("//*[@id='class_name']")).exists();
    }

    public ElementsCollection getListOfClasses() {
        return $$(listOfClasses);
    }

    public boolean movieIsExists(String className) {

        $(listOfClasses).shouldBe(Condition.visible);

        ElementsCollection listOfClass = getListOfClasses();

        Boolean movieIsExist = false;

      for (SelenideElement element : listOfClass) {
            String name = element.getText();
            if (name.equals(Consts.CLASS_NAME)) {
             movieIsExist = true;
            }
        }

        return movieIsExist;
    }

    public int countRecords() {
        return $$(records).size();
    }

    public void removeRecord() {
        $(lastRecordCheckbox).click();
        $(removeSelectedRecord).click();
        switchTo().alert().accept();
    }

    public void chooseClass(String currentClass) {
        $(selectClass).selectOption(currentClass);

    }
    public void choose100records(String records){
        $(recordsNumber).selectOption(records);
    }

}
