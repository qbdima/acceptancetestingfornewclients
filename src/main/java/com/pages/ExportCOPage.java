package com.pages;


import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class ExportCOPage extends BasePage {

    private By chooseClass = By.id("class_name");
    private By exportCSV = By.id("export_btn_csv");
    private By exportJSON = By.id("export_btn_json");
    private By findFile = By.cssSelector("tbody tr:nth-child(1) td:nth-child(4) a");
    private By importTab = By.xpath("//li[3]/a/i");


    public void clickImport(){
        $(importTab).click();
    }
    public void selectClass(String className){
       $(chooseClass).selectOption(className);
    }
    public void exportFiles(){
        $(exportCSV).click();
        $(exportJSON).click();
    }
    public void clickCsv(){
        $(exportCSV).click();
    }
    public void clickJson(){
        $(exportJSON).click();
    }

    public void clickFile(){
        $(findFile).click();
    }
}
