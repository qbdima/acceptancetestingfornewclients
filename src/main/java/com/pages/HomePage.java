package com.pages;


import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.utils.Consts;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.page;

public class HomePage extends AbstractPage {

    private By addNewApp = By.cssSelector("img.app-icon.new-app");
    private  By homePageTitle = By.className("panel-title");
    private By apps = By.cssSelector("li.app a");

    public NewAppPage clickCreateNewAppButton(){
        $(addNewApp).click();
        return page(NewAppPage.class);
    }


    public ElementsCollection getApps(){
        return  $$(apps);
    }

    public void waitPage(){
        $(addNewApp).shouldBe(Condition.visible);
    }

    public boolean isAppExists(String appTitle){
        ElementsCollection apps = getApps();

        Boolean isExist = false;
        for (SelenideElement element : apps) {
            String app = element.getText();

            if (app.equals(appTitle)) {
                isExist = true;
            }
        }

        return isExist;
    }

    public ApplicationPage selectAppByTitle(String title){
        ElementsCollection apps = getApps();
        for (SelenideElement element : apps) {
            String app = element.getText();

            if (app.equals(title)) {
                element.click();
                return page(ApplicationPage.class);
            }
        }

        return null;
    }



    public String getPageTitle(){
        return  $(homePageTitle).text();
    }

}