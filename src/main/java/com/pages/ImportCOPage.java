package com.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.io.File;

import static com.codeborne.selenide.Selenide.$;

public class ImportCOPage  extends BasePage{

    private By chooseClass = By.id("class_name");
    private By browseButton = By.name("file");
    private By clickImport = By.cssSelector("#upload_submit");
    private By className = By.cssSelector("tbody tr:last-child td:nth-child(3)");

    public void selectClass(String className){
        $(chooseClass).selectOption(className);

    }
    public void enterPath(String path){
        $(browseButton).uploadFile(new File(path));
    }

    public void importFile(String name, String filePath){
        selectClass(name);
        enterPath(filePath);
        $(clickImport).click();
    }

    public String getClassName(){
        return $(className).text();
    }
}
