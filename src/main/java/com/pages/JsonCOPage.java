package com.pages;


import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class JsonCOPage extends BasePage {

    private By jsonContent =By.tagName("pre");

    public String getJsonContent(){
        return $(jsonContent).text();
    }
}

