package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import static com.codeborne.selenide.Selenide.$;


public class LocationPage {

    private  By statusMessage = By.cssSelector("tbody tr:first-child td:nth-child(5) span");
    private By location = By.cssSelector(".col-md-4");


    public String getText(){
       return $(location).text();
    }
    public String locationIsCreated() {
        String getStatusMessage = "";
        Boolean location = null;
        try {
            $(statusMessage);
            location = true;
        } catch (NoSuchElementException e) {
            location = false;
        }
        if (location = true) {
            getStatusMessage= $(statusMessage).text();
        }
        return getStatusMessage;
    }
}
