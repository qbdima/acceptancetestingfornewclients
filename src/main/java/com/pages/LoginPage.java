package com.pages;


import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.google.gson.JsonObject;
import com.utils.ConfigParser;
import com.utils.Mailbox;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.page;
import static com.utils.Consts.ADMIN_PANEL;

public class LoginPage extends AbstractPage{


    private By loginField = By.id("user_login");
    private By passwordField = By.id("user_password");
    private By loginButton = By.id("signin_submit");
    private By createAcc = By.xpath("//*[@class='panel-body']/p/a");


    public void enterLogin(String login){
        $(loginField).setValue(login);
    }

    public void enterPassword(String password){
        $(passwordField).setValue(password);
    }
    public HomePage clickLoginButton(){
        $(loginButton).click();
        return page(HomePage.class);
    }

    public HomePage adminLogin(String login, String password){
        enterLogin(login);
        enterPassword(password);
        clickLoginButton();
        return page(HomePage.class);
    }



    public NewAccountPage clickCreateAccount(){
        $(createAcc).click();
        return page(NewAccountPage.class);
    }

    public boolean isAdminUserExists(String login, String password){

        boolean isExist = false;
        enterLogin(login);
        enterPassword(password);
        HomePage home = clickLoginButton();

        Selenide.refresh();

        if (home.getPageTitle().contains("Application")){
            isExist = true;
        }

        return isExist;
    }

    public String checkLoginCredentials(String login, String password) throws Exception{

//        String login = ConfigParser.getQBLogin();
//        String password = ConfigParser.getQBPassword();

        if(!isAdminUserExists(login, password)){
            if(!isAdminUserExists(login, "testtest")){
                createAccount(login, password);
            }else {
                return "testtest";
            }
        }else {
            return password;
        }

        open(ADMIN_PANEL);
        if(!isAdminUserExists(login, password)){
            throw new Exception ("Account did not created");
        }

        return password;

    }

    private void createAccount(String login, String password) throws Exception{

        String email_for_registration;
        String password_for_email;

        String registrationCode = ConfigParser.getRegistrationCode();

        Mailbox mailbox = new Mailbox();

        JsonObject credentials = ConfigParser.getCredentials();

        if(credentials != null){
            try {
                email_for_registration =  credentials.get("email_for_registration").getAsString();
                password_for_email = credentials.get("password_for_email").getAsString();
            }catch (NullPointerException e){
                throw new Exception ("Can not get email or password");
            }
        } else throw new Exception ("Can not get email or password");

        mailbox.confirmSesEmail(email_for_registration, password_for_email);

        NewAccountPage newAccountPage = clickCreateAccount();
        newAccountPage.enterFullName("test123");
        newAccountPage.enterEmail(email_for_registration);
        newAccountPage.enterLogin(login);
        newAccountPage.enterPassword(password);
        newAccountPage.enterRegistrationCode(registrationCode);
        newAccountPage.clickAcceptCheckbox();
        newAccountPage.clickSignUpButton();

        try{
            mailbox.confirmEmail(email_for_registration, password_for_email);
        }catch (Exception e){
            e.printStackTrace();
            System.exit(1);
        }
    }

}
