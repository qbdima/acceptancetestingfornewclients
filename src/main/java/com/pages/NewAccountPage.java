package com.pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;


public class NewAccountPage extends BasePage {

    public void enterFullName(String fullName){
        $(By.id("user_full_name")).setValue(fullName);
    }

    public void enterEmail(String email){
        $(By.id("user_email")).setValue(email);
    }

    public void enterLogin(String login){
        $(By.id("user_login")).setValue(login);
    }

    public void enterPassword(String password){
        $(By.id("user_password")).setValue(password);
        $(By.id("user_password_confirmation")).setValue(password);
    }

    public void enterRegistrationCode(String regCode){
        $(By.id("user_registration_code")).setValue(regCode);
    }

    public void clickAcceptCheckbox(){
        $(By.id("signup_terms")).click();
    }

    public void clickSignUpButton(){
        $(By.id("signup_submit")).click();
    }

}
