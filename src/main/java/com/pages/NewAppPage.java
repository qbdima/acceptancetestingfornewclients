package com.pages;

import org.openqa.selenium.By;
import java.io.File;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class NewAppPage {
    private By newAppTitle = By.id("application_title");
    private By appAvatar = By.id("avatar_file");
    private By appWebsite = By.id("application_url");
    private By appStorelink = By.id("application_app_store_link");
    private By appType = By.id("application_app_type");
    private By appDescription = By.id("application_description");
    private By appFacebookID = By.id("facebook_key");
    private By appFacebookSecret = By.id("facebook_secret");
    private By appTwitterKey = By.id("twitter_key");
    private By appTwitterSecret = By.id("twitter_secret");
    private By createButton = By.xpath("//*[@class='btn-toolbar']/button");

    public void enterAppTitle(String appTitle){
        $(newAppTitle).setValue(appTitle);
    }
    public void enterAvatar(String avatar){
        $(appAvatar).uploadFile(new File(avatar));
    }
    public void enterWebsite(String website){
        $(appWebsite).setValue(website);
    }
    public void enterAppStoreLink(String storeLink){
        $(appStorelink).setValue(storeLink);
    }
    public void enterDescription(String description){
        $(appDescription).setValue(description);
    }
    public void enterFacebookID(String facebookID){
        $(appFacebookID).setValue(facebookID);
    }
    public void enterFacebookSecret(String facebookSecret){
        $(appFacebookSecret).setValue(facebookSecret);
    }
    public void enterTwitterKey(String twitterKey){
        $(appTwitterKey).setValue(twitterKey);
    }
    public void enterTwitterSecret(String twitterSecret){
        $(appTwitterSecret).setValue(twitterSecret);
    }
    public void chooseType(String type){
        $(appType).selectOption(type);

    }

    public void clickCreateAppButton(){
        $(createButton).click();
    }

    public ApplicationPage addNewApp(String title){
        enterAppTitle(title);
        clickCreateAppButton();
        return page(ApplicationPage.class);
    }

    public void newAppAllFields(String avatar, String title,String website,String storeLink , String type, String description, String facebookID, String facebookSecret, String twitterKey, String twitterSecret ){
        enterAvatar(avatar);
        enterAppTitle(title);
        enterWebsite(website);
        enterAppStoreLink(storeLink);
        chooseType(type);
        enterDescription(description);
        enterFacebookID(facebookID);
        enterFacebookSecret(facebookSecret);
        enterTwitterKey(twitterKey);
        enterTwitterSecret(twitterSecret);
        clickCreateAppButton();
    }
}
