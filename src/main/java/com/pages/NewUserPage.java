package com.pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;


public class NewUserPage {
    private By userLogin = By.id("user_login");
    private By userEmail = By.id("user_email");
    private By userPassword = By.id("user_password");
    private By userPasswordConfirm = By.id("user_password_confirmation");
    private By addUserButton = By.id("user_submit");
    private By successMessage = By.cssSelector("#form-messages");
    private By errorMessage = By.xpath("//*[@id='form-errors']/div");


    public void enterLogin(String login){
        $(userLogin).setValue(login);
    }
    public void enterEmail(String email){
        $(userEmail).setValue(email);
    }

    public void enterPassword(String password){
        $(userPassword).setValue(password);
        $(userPasswordConfirm).setValue(password);
    }
    public void createUser (String login,  String password){
//        enterEmail(email);
        enterLogin(login);
        enterPassword(password);
        clickAddUserButton();
        return;
    }

    public void clickAddUserButton(){
        $(addUserButton).click();
    }

    public String getSuccessMessage(){
        $(successMessage).waitUntil(Condition.visible, 4000);
        return $(successMessage).text();
    }
    public String getErrorMessage(){
        $(errorMessage).shouldBe(Condition.visible);
        return $(errorMessage).text();
    }

    public String getUserId(){
        return getSuccessMessage().split("\\D+")[1];
    }
}
