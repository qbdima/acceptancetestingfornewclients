package com.pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;


public class PushNotificationsPage extends BasePage{

    private By iosButton = By.cssSelector("button.option:nth-child(1)");
    private By textarea = By.xpath("//*[@id='advanced_send_text']");
    private By sendPushButton = By.id("sendPush");
    private By successMessage = By.id("msg_2");
    private By errorMessage = By.id("msg_2");
    private By settingsTab = By.xpath("//*[@class='module-navigation-wrap']//ul//li[4]/a");

    public PushSettingsPage openSettingTab(){
        $(settingsTab).click();
        return page(PushSettingsPage.class);
    }
    public Boolean iosButtonVisible(){
        $(iosButton).isDisplayed();
        return true;
    }

    public String getSuccesMesage(){
        return $(successMessage).text();
    }
    public String getErrorMessage(){
        return $(errorMessage).text();
    }
    public void sendIosPush(String push){
        $(iosButton).click();
        $(textarea).setValue(push);
        $(sendPushButton).click();
    }



}
