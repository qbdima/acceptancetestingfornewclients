package com.pages;


import org.openqa.selenium.By;

import java.io.File;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.screenshot;

public class PushSettingsPage {

    private By uploadCertField = By.id("upload_cert");
    private By uploadButton = By.id("upload_submit");


    public void uploadApnsCert(File cert){
//        $(uploadCertField).sendKeys(certPAth);
//        $(uploadCertField).uploadFile(new File("./src/test/resources/Certificates_Developer_APNS.p12"));
//        $(uploadCertField).uploadFile(new File("./src/test/resources/apns-dev-cert.pem"));
        $(uploadCertField).uploadFile(cert);
//        screenshot("my_file_name");
        $(uploadButton).click();
    }

    public void uploadVoipApnsCert(String certPAth){
        $(uploadCertField).sendKeys(certPAth);
        $(uploadButton).click();
    }
}
