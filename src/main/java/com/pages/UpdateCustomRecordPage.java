package com.pages;


import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class UpdateCustomRecordPage extends BasePage {
    private By parentdIdField =By.id("record__parent_id");
    private By nameField =By.id("record_name");
    private By descriptionField =By.id("record_description");
    private By ratingField =By.id("record_rating");
    private By updateButton =By.xpath("(//input[@name='commit'])[7]");


    public void enterParentId(String id){
        $(parentdIdField).setValue(id);
    }
    public void enterName(String name){
        $(nameField).setValue(name);
    }
    public void enterDescription(String description){
        $(descriptionField).setValue(description);
    }
    public void enterRating(String rating){
        $(ratingField).setValue(rating);
    }


    public void clickUpdateRecord(){
        $(updateButton).click();
    }
    public  void updateRecord(String name,String description){
        enterName(name);
        enterDescription(description);
        clickUpdateRecord();
    }
}

