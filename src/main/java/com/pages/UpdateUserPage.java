package com.pages;


import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class UpdateUserPage extends BasePage {

    private By userFullName = By.id("user_full_name");
    private By userEmail = By.id("user_email");
    private By userLogin = By.id("user_login");
    private By userPassword = By.id("user_password");
    private By confirmPassword = By.id("user_password_confirmation");
    private By userPhone = By.id("user_phone");
    private By userWebsite = By.id("user_website");
    private By userBlobId = By.id("user_blob_id");
    private By userFacebookId = By.id("user_facebook_id");
    private By userTwitterId = By.id("user_twitter_id");
    private By saveButton = By.id("user_submit");
    private By successMessage = By.id("form-results-success");


    public Boolean saveButtonVisible(){
        $(saveButton).shouldBe(Condition.visible);
        return true;
    }
    public void enterName(String name) {
        $(userFullName).setValue(name);
    }

    public void enterEmail(String email) {
        $(userEmail).setValue(email);
    }

    public void enterLogin(String login) {
        $(userLogin).setValue(login);
    }

    public void enterPassword(String password) {
        $(userPassword).setValue(password);
    }

    public void confirmation(String passwordConfirm) {
        $(confirmPassword).setValue(passwordConfirm);
    }

    public void enterPhone(String phone) {
        $(userPhone).setValue(phone);
    }

    public void enterWebsite(String website) {
        $(userWebsite).setValue(website);
    }

    public void enterBlobId(String blobId) {
        $(userBlobId).setValue(blobId);
    }

    public void enterFacebookId(String facebookId) {
        $(userFacebookId).setValue(facebookId);
    }

    public void enterTwitterId(String twitterId) {
       $ (userTwitterId).setValue(twitterId);
    }
    public void clickSaveButton(){$(saveButton).click();}

    public String getSuccessMessage(){return $(successMessage).text();}
}
