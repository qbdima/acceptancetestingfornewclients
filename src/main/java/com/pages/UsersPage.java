package com.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.utils.Consts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Random;

import static com.codeborne.selenide.Selenide.*;


public class UsersPage {

    private By addNewUser = By.cssSelector("a.btn.btn-primary");
    private By removeButton = By.className("remove");
    private By userList = By.name("users_list_length");
    private By successMessage = By.id("form-messages");
    private By lastUserCheckbox = By.cssSelector("tbody tr:last-child td:first-child input");
    private By lastUserID = By.cssSelector("tbody tr:last-child td:nth-child(2)");

    public Boolean browseButtonVisible(){
        $(addNewUser).shouldBe(Condition.visible);
        return true;
    }
    public void clickRemoveSelected() {
       $(removeButton).click();
       switchTo().alert().accept();
    }
    public String getSuccessMessage() {
        $(successMessage).waitUntil(Condition.visible, 5000);
        return $(successMessage).text();
    }
    public NewUserPage clickAddNewUserButton(){
        $(addNewUser).click();
        return page(NewUserPage.class);
    }

    public String getUserIdByLogin(String userLogin){

        $(removeButton).shouldBe(Condition.visible);

        List<SelenideElement> elements = $$(By.cssSelector("#users_list > tbody > tr > td:nth-child(3)"));

        for(int i = 0; i < elements.size(); i++){
            if (elements.get(i).text().equals(userLogin)) {
                int k = i + 1;
                return $(By.xpath("//table[@id='users_list']/tbody/tr[" + k + "]/td[2]/a/span")).text();
            }
        }

        return null;
    }

    public int countUsers() {
        return $$(By.cssSelector("tbody td:nth-child(2)")).size();
    }
    public void choose100records(String records){
        $(userList).waitUntil(Condition.visible, 5000);
       $(userList).selectOptionByValue(records);

    }

    public void editUser() {
        $(lastUserID).click();
    }

    public void deleteUser() {
        $(lastUserCheckbox).click();
        $(removeButton).click();
        switchTo().alert().accept();
    }
    public String  randomUser() {
        Random random = new Random();
        String ranLogin = Integer.toString(random.nextInt(1000));
        String userLogin1 = "test-user" + ranLogin;
        return userLogin1;
    }
}
