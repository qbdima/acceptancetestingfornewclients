package com.utils;

import java.util.List;

public class Distance {

    private static final int radius = 6372795;

    private static double getDistance(Float longitude1, Float latitude1, Float longitude2, Float latitude2){

        double lat1 = latitude1*Math.PI/180.;
        double lat2 = latitude2*Math.PI/180.;
        double long1 = longitude1*Math.PI/180.;
        double long2 = longitude2*Math.PI/180.;

        double cl1 = Math.cos(lat1);
        double cl2 = Math.cos(lat2);
        double sl1 = Math.sin(lat1);
        double sl2 = Math.sin(lat2);
        double delta = long2 - long1;
        double cdelta = Math.cos(delta);
        double sdelta = Math.sin(delta);

        double y = Math.sqrt(Math.pow(cl2*sdelta,2)+Math.pow(cl1*sl2-sl1*cl2*cdelta,2));
        double x = sl1*sl2+cl1*cl2*cdelta;
        double ad = Math.atan2(y,x);
        double dist = ad*radius;

        return dist;
    }

    public static boolean isLt(List<Float> a, List<Float> b, List<Float> fixPoint){

        boolean isLt = false;

        double distance1 = getDistance(a.get(0), a.get(1), fixPoint.get(0), fixPoint.get(1));
        double distance2 = getDistance(b.get(0), b.get(1), fixPoint.get(0), fixPoint.get(1));

        if(distance1 <= distance2){
            isLt = true;
        }

        return isLt;
    }

}
