package com.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.File;
import java.io.FileReader;
import java.util.HashMap;

public class InputParameters {

    private static final String restApiTestsConfig = "./src/test/resources/rest_api_tests_config.json";

    private static JSONObject parseConfig() throws Exception{

        JSONObject config = null;
//        String filePath = "./src/test/resources/test.json";

        if ((new File(restApiTestsConfig)).exists()) {
            try{
                JsonElement jelement = new JsonParser().parse(new FileReader(restApiTestsConfig));
                config = new JSONObject(jelement.getAsJsonObject().toString());
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            throw new Exception("Configuration file ./src/test/resources/configurations.json not found. " +
                    "Please create it and put here configurations properties. See configurations.json.example for details");
        }

        return config;

    }


    public static HashMap initParams(){

        JSONObject config = null;

        try {
            config = parseConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }

        HashMap<String, String> params = new HashMap<>();

//        String qbEnvironment = System.getenv("qb_environment");
//
//        String []tmp = qbEnvironment.split(":");
//        String []parts = tmp[1].split(",");
//
//        String apiEndpoint = "https://" + parts[0] + "/";
//        String qbAppId = parts[1];
//        String qbAppAuthKey = parts[2];
//        String qbAppAuthSecret = parts[3];
//        String adminLogin = parts[4];
//        String adminPassword = parts[5];
//        String chatEndpoint = parts[6];
//

        try {
            params.put("application_id", config.getString("app_id"));
            params.put("auth_key", config.getString("auth_key"));
            params.put("auth_secret", config.getString("auth_secret"));
            params.put("api_endpoint", config.getString("api_domain"));  ////// UNREAD message test
            params.put("admin_login", ConfigParser.getQBLogin());
            params.put("admin_password", ConfigParser.getQBPassword());
            params.put("chat_endpoint", config.getString("chat_domain"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


/*        params.put("application_id", "13319");
        params.put("auth_key", "fFnYMChK-hQZ-q5");
        params.put("auth_secret", "5SevUsLmVCnudjA");
        params.put("api_endpoint", "http://apistage2.quickblox.com/");  ////// UNREAD message test
        params.put("admin_login", "qmunicate_release");
        params.put("admin_password", "12345678");
        params.put("chat_endpoint", "chatstage2.quickblox.com");*/


        return params;
    }

}
