package com.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.codehaus.jettison.json.JSONObject;

import java.io.File;
import java.io.FileReader;

/**
 * Created by injoit on 12/9/16.
 */
public class RestApiConfigParser{

    private static final String restApiTestsConfig = "./src/test/resources/rest_api_tests_config.json";

    private JSONObject config;

    public void test() throws Exception{
//        String filePath = "./src/test/resources/test.json";

        if ((new File(restApiTestsConfig)).exists()) {
            try{
                JsonElement jelement = new JsonParser().parse(new FileReader(restApiTestsConfig));
                config = new JSONObject(jelement.getAsJsonObject().toString());
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            throw new Exception("Configuration file ./src/test/resources/configurations.json not found. " +
                    "Please create it and put here configurations properties. See configurations.json.example for details");
        }

        System.out.println(config.get("api_domain"));
        System.out.println(config);
    }

}
