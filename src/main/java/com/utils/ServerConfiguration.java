package com.utils;

import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.*;

/**
 * Created by injoit on 12/8/16.
 */
public class ServerConfiguration extends JSONObject {
    public ServerConfiguration() {
    }

    public ServerConfiguration(String string) throws JSONException {
        super(string);
    }

    @Override
    public String toString() {
        try {
            StringBuffer sb = new StringBuffer("{");
            keys().forEachRemaining(key -> {
                if (sb.length() > 1) {
                    sb.append(",\n");
                }else {
                    sb.append('\n');
                }
                sb.append("\t" + quote(key.toString()));
                sb.append(": ");

                String value = String.valueOf(opt(key.toString()));

                if (!value.contains("/")) {
                    value = quote(String.valueOf(opt(key.toString())));
                } else {
                    value = "\"" + value + "\"";
                }

                sb.append(value);
            });
            return sb.append("\n}").toString();
        } catch (Exception e) {
            return null;
        }
    }

    public ServerConfiguration createFromFile(File file){
        ServerConfiguration configuration = null;

        try {
            InputStream is = new FileInputStream(file);
            String jsonTxt = IOUtils.toString(is, "UTF-8");
            configuration = new ServerConfiguration(jsonTxt);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return configuration;
    }


}
