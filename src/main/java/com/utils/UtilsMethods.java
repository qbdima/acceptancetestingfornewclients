package com.utils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.SignatureException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class UtilsMethods {

    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";

    public static String calculateHMAC_SHA(String data, String key) throws SignatureException {
        String result = null;
        try {

            // get an hmac_sha1 key from the raw key bytes
            SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);

            // get an hmac_sha1 Mac instance and initialize with the signing key
            Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            mac.init(signingKey);

            byte[] digest = mac.doFinal(data.getBytes());

            StringBuilder sb = new StringBuilder(digest.length * 2);
            String s;
            for (byte b : digest) {
                s = Integer.toHexString(0xFF & b);
                if (s.length() == 1) {
                    sb.append('0');
                }

                sb.append(s);
            }

            result = sb.toString();

        } catch (Exception e) {
            throw new SignatureException("Failed to generate HMAC : " + e.getMessage());
        }

        return result;
    }

    public static HashMap<String, String> parseBlobObject(String str) {
        String[] tmpStr = str.split("\\?");
        String[] parts = tmpStr[1].split("&");
        String[] tmp;
        HashMap<String, String> arr = new HashMap<String, String>();

        for (int i = 0; i < parts.length; i++) {
            tmp = parts[i].split("=");
            arr.put(tmp[0], tmp[1]);
        }
        return arr;
    }

    public static String getBucketUrl(String str) {
        String[] parts = str.split("\\?");

        return parts[0];
    }

    public static boolean beforeDate(String dateString1, String dateString2, boolean isStrict) {
        boolean result = false;

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date = null;

        try {
            date = format.parse(dateString1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        Date date2 = null;
        try {
            date2 = format2.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(!isStrict){
            if(date.equals(date2)){
                result = true;
                return result;
            }
        }

        if (date.before(date2)) {
            result = true;
        }

        return result;
    }

}
