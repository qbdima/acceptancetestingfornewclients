package com.adminPanelTests;


import com.utils.Consts;
import org.testng.Assert;

import static com.codeborne.selenide.Selenide.back;
import static com.codeborne.selenide.Selenide.sleep;

public class AddNewAppTests extends TestBase {

    @org.testng.annotations.Test
    public void openHomePage() {
        back();
        sleep(2000);
        Assert.assertTrue(home.getPageTitle().contains("Application"));
    }

    // @Description("Enter all valid data")
    @org.testng.annotations.Test(priority = 1)
    public void createNewApp() {
        home.clickCreateNewAppButton();
        newApp.newAppAllFields(Consts.IMAGE_PATH, Consts.NEW_APP_NAME, "http://google.com", "test test test",
                "Fun", "test", "234234534", "345454353", "456546464", "4654574");
        sleep(2000);
        Assert.assertTrue(overview.getAppName().contains(Consts.NEW_APP_NAME));
    }

    @org.testng.annotations.Test(priority = 2)
    public void deleteNewApp() {
        overview.removeApp();
        sleep(4000);
        Assert.assertTrue(home.getPageTitle().contains("Application"));
    }


}
