package com.adminPanelTests;


import org.testng.Assert;

import static com.codeborne.selenide.Selenide.sleep;

public class ChatTests extends TestBase {


    // @Description("Open chat page")
    @org.testng.annotations.Test(alwaysRun = true)
    public void openChatPage() {
        overview.chatPageClick();
        Assert.assertTrue(chat.newChatButtonVisible());
    }


    // @Description("Create group chat")
    @org.testng.annotations.Test(alwaysRun = true, priority = 1)
    public void addGroupChat() {
        chat.createGroupChat("Group Chat", "1,2,3");
        sleep(2000);
        Assert.assertTrue(chat.getSuccessMessage().contains("Dialog has been successfully"));
    }

    // @Description("Create public chat")
    @org.testng.annotations.Test(alwaysRun = true, priority = 1)
    public void addPublicChat() {
        sleep(2000);
        chat.createPublicChat("Public Chat");
        sleep(2000);
        Assert.assertTrue(chat.getSuccessMessage().contains("Dialog has been successfully"));
    }


    //   @Description("Delete last chat in the chats list")
    @org.testng.annotations.Test(alwaysRun = true, priority = 2)
    public void deleteChatTest() {
        chat.choose100records("100");
        sleep(2000);
        int count = chat.countChats();
        chat.removeChat();
        sleep(2000);
        Assert.assertEquals(chat.countChats(), count - 1);
    }

}


