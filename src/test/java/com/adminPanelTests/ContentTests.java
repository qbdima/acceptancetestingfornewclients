package com.adminPanelTests;


import com.utils.Consts;
import org.testng.Assert;

import static com.codeborne.selenide.Selenide.sleep;

public class ContentTests extends TestBase {

    // @Description("Open Content page")
    @org.testng.annotations.Test(alwaysRun = true)
    public void openContentPage() {
        overview.contentPageClick();
        Assert.assertTrue(content.browseButtonVisible());
    }

    //@Description("Upload content")
    @org.testng.annotations.Test(alwaysRun = true, priority = 1)
    public void addContentTest() {
        sleep(2000);
        content.uploadFile(Consts.IMAGE_PATH);
        Assert.assertTrue(content.getSuccessMessage().contains("Success"));
    }

    // @Description("Delete last content in the content list")
    @org.testng.annotations.Test(alwaysRun = true, priority = 2)
    public void deleteContentTest() {
        content.choose100records("100");
        sleep(2000);
        int count = content.countContent();
        content.removeContent();
        sleep(2000);
        Assert.assertEquals(content.countContent(), count - 1);
    }

}

