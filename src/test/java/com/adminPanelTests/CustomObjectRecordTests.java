package com.adminPanelTests;


import com.codeborne.selenide.WebDriverRunner;
//import com.sun.org.glassfish.gmbal.Description;
import com.utils.Consts;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;

import java.io.File;

import static com.codeborne.selenide.Selenide.*;

public class CustomObjectRecordTests extends TestBase {

    // @Description("Open Custom page")
    @org.testng.annotations.Test
    public void openCustomPage() {
        overview.customPageClick();
        Assert.assertTrue(custom.addButtonVisible());
    }

    @org.testng.annotations.Test(priority = 1)
    public void createNewClass() {
        if (!custom.classIsCreated()) {
            custom.addClass();
            newClass.createClass(Consts.CLASS_NAME, Consts.FIELD_NAME1, Consts.FIELD_NAME2, Consts.FIELD_NAME3, Consts.FIELD_TYPE1, Consts.FIELD_TYPE2, Consts.FIELD_TYPE3);
        } else if (custom.classIsCreated()) {
            boolean movieIsExists = custom.movieIsExists(Consts.CLASS_NAME);
            if (!movieIsExists) {
                custom.addClass();
                newClass.createClass(Consts.CLASS_NAME, Consts.FIELD_NAME1, Consts.FIELD_NAME2, Consts.FIELD_NAME3, Consts.FIELD_TYPE1, Consts.FIELD_TYPE2, Consts.FIELD_TYPE3);
            }
        }
        sleep(2000);
        Assert.assertTrue(custom.movieIsExists(Consts.CLASS_NAME));
    }


    // @Description("Choose Movie Class")
    @org.testng.annotations.Test(priority = 2)
    public void chooseClass() {
        custom.chooseClass(Consts.CLASS_NAME);
        sleep(2000);
        Assert.assertTrue(custom.getTableHeadText().contains("description"));
    }


    //@Description("Add record test")
    @org.testng.annotations.Test(priority = 3)
    public void addRecordTest() {
        sleep(1000);
        custom.choose100records("100");
        sleep(2000);
        int count = custom.countRecords();
        sleep(2000);
        custom.addRecord();
        record.createRecord("111", "test");
        sleep(5000);
        Assert.assertEquals(custom.countRecords(), count + 1);
    }

    // @Description("Add one more record")
    @org.testng.annotations.Test(priority = 3)
    public void addRecordTest2() {
        custom.choose100records("100");
        sleep(2000);
        int count = custom.countRecords();
        sleep(2000);
        custom.addRecord();
        record.createRecord("222", "qwerty");
        sleep(5000);
        Assert.assertEquals(custom.countRecords(), count + 1);
    }


    // @Description("Update last record in the record list")
    @org.testng.annotations.Test(priority = 4)
    public void updateRecordTest() {
        custom.clickLastRecord();
        sleep(2000);
        updateRecord.updateRecord("test", "test test");
        sleep(5000);
        Assert.assertTrue(custom.getRecordName().contains("test"));
    }

    //@Description("Delete last record in the record list")
    @org.testng.annotations.Test(priority = 5)
    public void deleteRecordTest() {
        sleep(1000);
        custom.choose100records("100");
        int count = custom.countRecords();
        custom.removeRecord();
        sleep(4000);
        Assert.assertEquals(custom.countRecords(), count - 1);
    }

    //  @Description("Export to json test")
    @org.testng.annotations.Test(priority = 6)
    public void exportJsonTest() {
        custom.clickExport();
        export.clickJson();
        sleep(4000);
        refresh();
        sleep(1000);
        export.clickFile();
        Assert.assertTrue(jsonContent.getJsonContent().contains("created_at"));
        back();
    }

    // @Description("Import file test")
    @org.testng.annotations.Test(priority = 7)
    public void importTest() {
        export.clickImport();
        sleep(2000);
        importFile.importFile(Consts.CLASS_NAME, Consts.JSON_FILE_PATH);
        sleep(2000);
        Assert.assertTrue(importFile.getClassName().contains(Consts.CLASS_NAME));

    }

}
