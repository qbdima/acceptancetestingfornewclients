package com.adminPanelTests;

import org.testng.Assert;

import static com.codeborne.selenide.Selenide.sleep;


public class LocationTests extends TestBase {

    @org.testng.annotations.Test(alwaysRun = true)
    public void openLocationPage() {
        overview.locationPageClick();
        sleep(2000);
        Assert.assertTrue(location.getText().contains("last user location"));
    }

    // @Description("Check location is present")
    @org.testng.annotations.Test(alwaysRun = true, priority = 1)
    public void locationTest() {
        Assert.assertTrue(location.locationIsCreated().contains("world"));
    }

}
