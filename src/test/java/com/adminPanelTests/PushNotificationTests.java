package com.adminPanelTests;



import org.testng.Assert;


import static com.codeborne.selenide.Selenide.sleep;

public class PushNotificationTests extends TestBase {

    @org.testng.annotations.Test(alwaysRun = true)
    public void openPushPage() {
        overview.pushNotificationsPageClick();
        Assert.assertTrue(push.iosButtonVisible());
    }


//   // @Description("Send push notification")
//    @org.testng.annotations.Test(alwaysRun = true, priority = 1)
//    public void sendPushTest(){
//        push.sendIosPush("test");
//        sleep(2000);
//        Assert.assertTrue(push.getSuccesMesage().contains("Success"));
//    }


}
