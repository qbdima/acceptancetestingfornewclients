package com.adminPanelTests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.pages.*;
import com.utils.ConfigParser;
import com.utils.Consts;
import com.utils.ServerConfiguration;
import io.github.bonigarcia.wdm.*;
import org.codehaus.jettison.json.JSONException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static com.codeborne.selenide.Selenide.*;


public class Test {

    private static final String restApiTestsConfig = "./src/test/resources/rest_api_tests_config.json";
    private static final String snippetsConfig = "./src/test/resources/snippets_config.json";
    private static final String globalConfig = "./src/test/resources/configurations.json";

    private static final String pushAutotestCert = "./src/test/resources/push_autotest_dev.pem";
    private static final String supersampleCert = "./src/test/resources/supersample_dev.pem";

    private static final String hubURL = "http://tsung2.quickblox.com:4444/wd/hub";

    private String currentPassword;


    @BeforeSuite
    public void setUp() {

        if(ConfigParser.getMode().equals("0")){
            ChromeDriverManager.getInstance().version("2.25").setup();
            Configuration.browser = "chrome";
            Configuration.timeout = 10000;
        }else {
            Configuration.timeout = 10000;
//            Configuration.remote = hubURL;
            Configuration.remote = ConfigParser.getHubUrl();
            Configuration.browser = "chrome";
        }

//        Configuration.reportsFolder = "./src/test/resources";

//        System.setProperty("selenide.browser", "Chrome");
//        Configuration.timeout = 10000;
//        System.setProperty("webdriver.chrome.driver", "/Users/injoit/Downloads/chromedriver");
//                System.setProperty("webdriver.gecko.driver", "/Users/injoit/Downloads/geckodriver");


//        Configuration.timeout = 10000;
//        Configuration.remote = hubURL;
//        Configuration.browser = "chrome";


//        Configuration.browser = "marionette";

//        Configuration.browser = "firefox";
    }

    @AfterMethod
    public void afterTest() {
        WebDriverRunner.closeWebDriver();
    }

    @org.testng.annotations.Test
    public void checkAndCreateNewAccount(){
        LoginPage loginPage = open(Consts.ADMIN_PANEL, LoginPage.class);

        String login = ConfigParser.getQBLogin();
        String password = ConfigParser.getQBPassword();

        try {
            currentPassword = loginPage.checkLoginCredentials(login, password);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ServerConfiguration configuration = new ServerConfiguration().createFromFile(new File(globalConfig));
        try {
            configuration.put("qb_password", currentPassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try (FileWriter file = new FileWriter(globalConfig)) {
            file.write(configuration.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @org.testng.annotations.Test(dependsOnMethods = "checkAndCreateNewAccount")
    public void createRestApiTestsApp () throws JSONException {
        ServerConfiguration serverConfiguration = new ServerConfiguration();

        LoginPage loginPage = open(Consts.ADMIN_PANEL, LoginPage.class);

        loginPage.adminLogin(ConfigParser.getQBLogin(), currentPassword);

        HomePage homePage = new HomePage();
        homePage.waitPage();

        boolean isExist = homePage.isAppExists(Consts.APP_NAME2);

        if (isExist) {
            homePage.selectAppByTitle(Consts.APP_NAME2);
        } else {
            NewAppPage newAppPage = homePage.clickCreateNewAppButton();
            newAppPage.addNewApp(Consts.APP_NAME2);
        }

        ApplicationPage applicationPage = new ApplicationPage();
        serverConfiguration.put("app_id", applicationPage.getAppId());
        serverConfiguration.put("auth_key", applicationPage.getAuthKey());
        serverConfiguration.put("auth_secret", applicationPage.getAuthSecret());

        System.out.println(applicationPage.getAppId());
        System.out.println(applicationPage.getAuthKey());
        System.out.println(applicationPage.getAuthSecret());

        applicationPage.openApplicationSettingTab();
        applicationPage.setListUsersCheckbox();

        PushNotificationsPage pushNotificationsPage = applicationPage.pushNotificationsPageClick();
        PushSettingsPage pushSettings = pushNotificationsPage.openSettingTab();

        File cert = new File(pushAutotestCert);
        pushSettings.uploadApnsCert(cert);

        ChatPage chatPage = applicationPage.chatPageClick();
        chatPage.openAlertsTab();
        ChatAlertPage chatAlerts = new ChatAlertPage();
        chatAlerts.setTemplate(ChatAlertPage.ALERT_TEMPLATE.CUSTOM);
        chatAlerts.setCustomTemplate(ConfigParser.getClientName() + ": You have %unread_count% new messages");
        chatAlerts.setBadgeCounter(ChatAlertPage.BADGE_COUNTER.UNREAD);
        chatAlerts.setSound(ChatAlertPage.SOUND.DEFAULT);
        chatAlerts.setCategory("test");
        chatAlerts.setContentAvailable();

        AccountPage accountPage = open(Consts.ADMIN_PANEL + "account/settings", AccountPage.class);

        serverConfiguration.put("api_domain", accountPage.getApiUrl());
        serverConfiguration.put("chat_domain", accountPage.getChatUrl());

        System.out.println(accountPage.getApiUrl());
        System.out.println(accountPage.getChatUrl());

        System.out.println(serverConfiguration.toString());

        try (FileWriter file = new FileWriter(restApiTestsConfig)) {
            file.write(serverConfiguration.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }


//        RestApiConfigParser restApiConfigParser = new RestApiConfigParser();
//        try {
//            restApiConfigParser.test();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    @org.testng.annotations.Test(dependsOnMethods = "checkAndCreateNewAccount")
    public void createTestApp() throws JSONException {
        ServerConfiguration serverConfiguration = new ServerConfiguration();

        LoginPage loginPage = open(Consts.ADMIN_PANEL, LoginPage.class);

        loginPage.adminLogin(ConfigParser.getQBLogin(), currentPassword);

        HomePage homePage = new HomePage();
        homePage.waitPage();

        boolean isExist = homePage.isAppExists(Consts.APP_NAME);

        if (isExist) {
            homePage.selectAppByTitle(Consts.APP_NAME);
        } else {
            NewAppPage newAppPage = homePage.clickCreateNewAppButton();
            newAppPage.addNewApp(Consts.APP_NAME);
        }

        ApplicationPage applicationPage = new ApplicationPage();
        serverConfiguration.put("app_id", applicationPage.getAppId());
        serverConfiguration.put("auth_key", applicationPage.getAuthKey());
        serverConfiguration.put("auth_secret", applicationPage.getAuthSecret());

        System.out.println(applicationPage.getAppId());
        System.out.println(applicationPage.getAuthKey());
        System.out.println(applicationPage.getAuthSecret());

        applicationPage.openApplicationSettingTab();
        applicationPage.setListUsersCheckbox();


        PushNotificationsPage pushNotificationsPage = applicationPage.pushNotificationsPageClick();
        PushSettingsPage pushSettings = pushNotificationsPage.openSettingTab();

        File cert = new File(supersampleCert);
        pushSettings.uploadApnsCert(cert);


        ChatPage chatPage = applicationPage.chatPageClick();
        chatPage.openAlertsTab();
        ChatAlertPage chatAlerts = new ChatAlertPage();
        chatAlerts.setTemplate(ChatAlertPage.ALERT_TEMPLATE.SENDER_FULLNAME);
        chatAlerts.setBadgeCounter(ChatAlertPage.BADGE_COUNTER.UNREAD);

        // создание юзеров
        UsersPage usersPage = applicationPage.usersPageClick();

        String userID1 = usersPage.getUserIdByLogin(Consts.USER1_LOGIN);
        String userID2 = usersPage.getUserIdByLogin(Consts.USER2_LOGIN);


        if (userID1 == null) {
            NewUserPage newUserPage = usersPage.clickAddNewUserButton();
            newUserPage.createUser(Consts.USER1_LOGIN, Consts.USER1_PASSWORD);
            System.out.println(newUserPage.getSuccessMessage());
            System.out.println(newUserPage.getUserId());
            serverConfiguration.put("test_user_id1", newUserPage.getUserId());
            serverConfiguration.put("test_user_login1", Consts.USER1_LOGIN);
            serverConfiguration.put("test_user_password1", Consts.USER1_PASSWORD);
            Selenide.back();
        } else {
            serverConfiguration.put("test_user_id1", userID1);
            serverConfiguration.put("test_user_login1", Consts.USER1_LOGIN);
            serverConfiguration.put("test_user_password1", Consts.USER1_PASSWORD);
        }


        if (userID2 == null) {
            Selenide.refresh();
            NewUserPage newUserPage = usersPage.clickAddNewUserButton();
            newUserPage.createUser(Consts.USER2_LOGIN, Consts.USER2_PASSWORD);
            System.out.println(newUserPage.getSuccessMessage());
            System.out.println(newUserPage.getUserId());
            serverConfiguration.put("test_user_id2", newUserPage.getUserId());
            serverConfiguration.put("test_user_login2", Consts.USER2_LOGIN);
            serverConfiguration.put("test_user_password2", Consts.USER2_PASSWORD);
        } else {
            serverConfiguration.put("test_user_id2", userID2);
            serverConfiguration.put("test_user_login2", Consts.USER2_LOGIN);
            serverConfiguration.put("test_user_password2", Consts.USER2_PASSWORD);
        }
        // создание кастом класса
        CustomPage customPage = applicationPage.customPageClick();
        boolean classCreated = customPage.classIsCreated();
        if (!classCreated) {
            AddNewCustomClass addNewClass = customPage.addClass();
            addNewClass.createClass(Consts.CLASS_NAME, Consts.FIELD_NAME1, Consts.FIELD_NAME2, Consts.FIELD_NAME3, Consts.FIELD_TYPE1, Consts.FIELD_TYPE2, Consts.FIELD_TYPE3);
        } else if (classCreated = true) {
            boolean movieIsExists = customPage.movieIsExists(Consts.CLASS_NAME);
            if (!movieIsExists) {
                AddNewCustomClass addNewClass = customPage.addClass();
                addNewClass.createClass(Consts.CLASS_NAME, Consts.FIELD_NAME1, Consts.FIELD_NAME2, Consts.FIELD_NAME3, Consts.FIELD_TYPE1, Consts.FIELD_TYPE2, Consts.FIELD_TYPE3);
            }
        }

        sleep(2000);

        AccountPage accountPage = open(Consts.ADMIN_PANEL + "account/settings", AccountPage.class);

        serverConfiguration.put("api_domain", accountPage.getApiUrl());
        serverConfiguration.put("chat_domain", accountPage.getChatUrl());

        System.out.println(accountPage.getApiUrl());
        System.out.println(accountPage.getChatUrl());

        System.out.println(serverConfiguration.toString());

        try (FileWriter file = new FileWriter(snippetsConfig)) {
            file.write(serverConfiguration.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

//        RestApiConfigParser restApiConfigParser = new RestApiConfigParser();
//        try {
//            restApiConfigParser.test();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    @org.testng.annotations.Test(dependsOnMethods = "checkAndCreateNewAccount")
    public void createAdminPanelTestsApp() {

        LoginPage loginPage = open(Consts.ADMIN_PANEL, LoginPage.class);

        loginPage.enterLogin(ConfigParser.getQBLogin());
        loginPage.enterPassword(currentPassword);
        HomePage homePage = loginPage.clickLoginButton();
        homePage.waitPage();

        boolean isExist = homePage.isAppExists(Consts.APP_NAME3);

        if (isExist) {
            homePage.selectAppByTitle(Consts.APP_NAME3);
        } else {
            NewAppPage newAppPage = homePage.clickCreateNewAppButton();
            newAppPage.addNewApp(Consts.APP_NAME3);
        }

    }

}
