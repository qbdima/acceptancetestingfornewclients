package com.adminPanelTests;

import com.codeborne.selenide.*;
import com.codeborne.selenide.Configuration;
import com.pages.*;
//import com.sun.org.glassfish.gmbal.Description;
import com.utils.ConfigParser;
import com.utils.Consts;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;


import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;
import static com.utils.Consts.ADMIN_PANEL;


public class TestBase {
    public LoginPage login;
    public HomePage home;
    public ApplicationPage overview;
    public NewAppPage newApp;
    //  public OverviewPage overview;
    public UsersPage users;
    public NewUserPage createUser;
    public UpdateUserPage updateUser;
    public ContentPage content;
    public AddCustomObjectRecordPage record;
    public UpdateCustomRecordPage updateRecord;
    public CustomPage custom;
    public AddNewCustomClass newClass;
    public ExportCOPage export;
    public ImportCOPage importFile;
    public JsonCOPage jsonContent;
    public ChatPage chat;
    public ChatAlertPage chatAlert;
    public LocationPage location;
    public PushNotificationsPage push;


    {
        if (ConfigParser.getMode().equals("0")) {
            ChromeDriverManager.getInstance().version("2.25").setup();
            Configuration.browser = "chrome";
            Configuration.timeout = 10000;
        } else {
            Configuration.timeout = 10000;
            Configuration.remote = ConfigParser.getHubUrl();
            Configuration.browser = "chrome";
        }
    }


    @BeforeClass
    public void loginAndChooseApp() {
        login = open(ADMIN_PANEL, LoginPage.class);
        home = login.adminLogin(ConfigParser.getQBLogin(), ConfigParser.getQBPassword());
        sleep(2000);
        Assert.assertTrue(home.getPageTitle().contains("Application"));
        sleep(2000);
        overview = home.selectAppByTitle(Consts.APP_NAME3);
        sleep(2000);
        Assert.assertTrue(overview.getAppName().contains(Consts.APP_NAME3));
    }


    @BeforeMethod
    public void beforeAllTests() {

        login = new LoginPage();
        home = new HomePage();
        overview = new ApplicationPage();
        chatAlert = new ChatAlertPage();
        newApp = new NewAppPage();
        chat = new ChatPage();
        content = new ContentPage();
        custom = new CustomPage();
        newClass = new AddNewCustomClass();
        record = new AddCustomObjectRecordPage();
        updateRecord = new UpdateCustomRecordPage();
        push = new PushNotificationsPage();
        export = new ExportCOPage();
        importFile = new ImportCOPage();
        jsonContent = new JsonCOPage();
        location = new LocationPage();
        users = new UsersPage();
        createUser = new NewUserPage();
        updateUser = new UpdateUserPage();
    }

    @AfterClass
    public void afterTest() {
        WebDriverRunner.closeWebDriver();
    }

}
