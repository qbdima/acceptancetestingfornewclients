package com.adminPanelTests;


import com.utils.Consts;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.back;
import static com.codeborne.selenide.Selenide.refresh;
import static com.codeborne.selenide.Selenide.sleep;

public class UserTests extends TestBase {


    @org.testng.annotations.Test(alwaysRun = true)
    public void openUserPage() {
        overview.usersPageClick();
        Assert.assertTrue(users.browseButtonVisible());
    }

    //@Description("Create user with obligatory fields")
    @org.testng.annotations.Test(priority = 1, alwaysRun = true)
    public void createUser() {
        users.clickAddNewUserButton();
        createUser.createUser(users.randomUser(), Consts.USER_PASSWORD);
        sleep(2000);
        Assert.assertTrue(createUser.getSuccessMessage().contains("Success"));
        back();
    }

    @org.testng.annotations.Test(priority = 2, alwaysRun = true)
    public void chooseUserForUpdate() {
        users.choose100records("100");
        sleep(2000);
        users.editUser();
        Assert.assertTrue(updateUser.saveButtonVisible());
    }

    // @Description("Update user full name")
    @org.testng.annotations.Test(priority = 3, alwaysRun = true)
    public void updateUserName() {
        updateUser.enterName("autotest1");
        updateUser.clickSaveButton();
        sleep(2000);
        Assert.assertTrue(updateUser.getSuccessMessage().contains("Success"));
        refresh();
    }

    //@Description("Update user phone")
    @org.testng.annotations.Test(priority = 3, alwaysRun = true)
    public void updateUserPhone() {
        updateUser.enterPhone("876666666");
        updateUser.clickSaveButton();
        sleep(2000);
        Assert.assertTrue(updateUser.getSuccessMessage().contains("Success"));
        refresh();
    }

    // @Description("Update user website")
    @org.testng.annotations.Test(priority = 3, alwaysRun = true)
    public void updateUserWebsite() {
        updateUser.enterWebsite("http://google.com.ua");
        updateUser.clickSaveButton();
        sleep(2000);
        Assert.assertTrue(updateUser.getSuccessMessage().contains("Success"));
        back();
        sleep(2000);
    }

    //  @Description("Delete user which was created in the create user test")
    @org.testng.annotations.Test(priority = 4, alwaysRun = true)
    public void deleteUserTest() {
        users.choose100records("100");
        sleep(2000);
        int count = users.countUsers();
        users.deleteUser();
        sleep(2000);
        Assert.assertTrue(users.getSuccessMessage().contains("Success"));
        Assert.assertEquals(users.countUsers(), count - 1);
    }
}
