package com.boshAndWebSocketTests.modules;

import rocks.xmpp.core.XmppException;
import rocks.xmpp.core.session.XmppClient;
import rocks.xmpp.extensions.httpbind.BoshConnection;
import rocks.xmpp.extensions.httpbind.BoshConnectionConfiguration;

public class BoshModule{

    public static String connect(String url, boolean isSecure, String login, String password){

        int port;

        if(isSecure){
            port = 5281;
        }else port = 5280;

        BoshConnectionConfiguration boshConfiguration = BoshConnectionConfiguration.builder()
                .hostname(url)
                .port(port)
                .path("")
                .secure(isSecure)
                .build();

        try (XmppClient xmppClient = XmppClient.create(url, boshConfiguration)) {

            xmppClient.connect();
            // Login
            xmppClient.login(login, password, "resource");

            BoshConnection boshConnection = (BoshConnection) xmppClient.getActiveConnection();

//             Gets the session id (sid) of the BOSH connection.
            String sessionId = boshConnection.getSessionId();

            // Detaches the BOSH session, without terminating it.
            long rid = boshConnection.detach();

            System.out.println("JID: " + xmppClient.getConnectedResource());
            System.out.println("SID: " + sessionId);
            System.out.println("RID: " + rid);

            return sessionId;

        } catch (XmppException e) {
            e.printStackTrace();
        }

        return null;
    }

}
