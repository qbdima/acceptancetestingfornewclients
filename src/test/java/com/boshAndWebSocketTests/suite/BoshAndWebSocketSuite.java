package com.boshAndWebSocketTests.suite;

import com.boshAndWebSocketTests.modules.BoshModule;
import com.boshAndWebSocketTests.modules.WebSocketModule;
import com.neovisionaries.ws.client.WebSocket;
import com.restApiTests.chat.suite.BaseChatTest;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;


public class BoshAndWebSocketSuite extends BaseChatTest{

    final static String password = "11111111";

    @Test(groups = { "BoshAndWebSocketTest" }, dependsOnGroups = {"baseTest"}, timeOut = 10000, alwaysRun = true)
    public void checkNotSecureWebSocket() throws JSONException {

//        logger.info("Connect to server: " + "ws://" + chat + ":5290");
        System.out.println("Connect to server: " + "ws://" + chatURL + ":5290");
        WebSocket ws = WebSocketModule.connect(chatURL, false);
        boolean isConnected;
        if(ws.getState().toString().equals("OPEN")) {
            isConnected = true;
//            logger.info("Connection status : connected" + "\n");
            System.out.println("Connection status : connected" + "\n");
        }else {
            isConnected = false;
//            logger.info("Connection status : not connected" + "\n");
            System.out.println("Connection status : not connected" + "\n");
        }
        Assert.assertTrue(isConnected);
    }

    @Test(groups = { "BoshAndWebSocketTest" }, dependsOnMethods = {"checkNotSecureWebSocket"}, alwaysRun = true, timeOut = 10000)
    public void checkSecureWebSocket() throws JSONException {

//        logger.info("Connect to server: " + "wss://" + chat + ":5291");
        System.out.println("Connect to server: " + "wss://" + chatURL + ":5291");
        WebSocket ws = WebSocketModule.connect(chatURL, true);
        boolean isConnected;
        if(ws.getState().toString().equals("OPEN")) {
            isConnected = true;
//            logger.info("Connection status : connected" + "\n");
            System.out.println("Connection status : connected" + "\n");
        }else {
            isConnected = false;
//            logger.info("Connection status : not connected" + "\n");
            System.out.println("Connection status : not connected" + "\n");
        }

        Assert.assertTrue(isConnected);
    }

    @Test(groups = { "BoshAndWebSocketTest" }, dependsOnMethods = {"checkSecureWebSocket"}, alwaysRun = true, timeOut = 20000)
    public void checkNotSecureBosh() {
        Assert.assertNotNull(BoshModule.connect(chatURL, false, userID1 + "-" + appID, password), "Error during connection via bosh on port 5080");
    }

    @Test(groups = { "BoshAndWebSocketTest" }, dependsOnMethods = {"checkNotSecureBosh"}, alwaysRun = true, timeOut = 20000)
    public void checkSecureBosh() {
        Assert.assertNotNull(BoshModule.connect(chatURL, true, userID1 + "-" + appID, password), "Error during connection via bosh on port 5081");
    }

}
