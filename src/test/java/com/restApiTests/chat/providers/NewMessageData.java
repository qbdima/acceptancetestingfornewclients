package com.restApiTests.chat.providers;


import org.testng.annotations.DataProvider;

import java.util.HashMap;
import java.util.Random;

public class NewMessageData {

    @DataProvider(name = "newMessageForGroupDialog")
    public static Object[][] newMessageForGroupDialog() {

        Random random = new Random();

        String messageText = "my_message = " + Integer.toString(random.nextInt(Integer.MAX_VALUE) + 1);

        HashMap<String,Object> message = new HashMap<>();
        message.put("message", messageText);

        return new Object[][]{{message}};
    }

    @DataProvider(name = "newMessageForPublicGroupDialog")
    public static Object[][] newMessageForPublicGroupDialog() {

        Random random = new Random();

        String login = Integer.toString(random.nextInt(Integer.MAX_VALUE) + 1);

        HashMap<String,Object> message = new HashMap<>();
        message.put("message", "11111111");

        return new Object[][]{{message}};
    }

    @DataProvider(name = "newMessageForGroupDialogWithAttachment")
    public static Object[][] newMessageForGroupDialogWithAttachment() {

        Random random = new Random();

        String login = Integer.toString(random.nextInt(Integer.MAX_VALUE) + 1);

        HashMap<String,Object> message = new HashMap<>();
        message.put("message", "with attachment");
        message.put("attachments[0][id]", 47863);
        message.put("attachments[0][type]", "image");

        return new Object[][]{{message}};
    }

    @DataProvider(name = "newMessageForPrivateDialog")
    public static Object[][] newMessageForPrivateDialog() {

        Random random = new Random();

        String login = Integer.toString(random.nextInt(Integer.MAX_VALUE) + 1);

        HashMap<String,Object> message = new HashMap<>();
        message.put("message", "private chat message");

        return new Object[][]{{message}};
    }

}
