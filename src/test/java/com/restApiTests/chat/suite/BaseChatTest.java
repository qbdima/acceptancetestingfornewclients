package com.restApiTests.chat.suite;


import com.restApiTests.core.BaseTest;
import org.apache.commons.io.IOUtils;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeTest;

import java.io.IOException;

public class BaseChatTest extends BaseTest {

    protected String dialogId;
    protected String dialogId2;
    protected String groupChatMessageID;
    protected String groupChatMessageID2;
    protected String privateChatMessageID;
    protected String userLogin1;
    protected String userLogin2;
    protected int userID1;
    protected int userID2;

    protected String groupDialogId;
    protected String publicGroupDialogID;
    protected String privateDialogId;

    protected String dialog_schema;
    protected String dialogs_schema;
    protected String dialog_with_custom_fields_schema;

    protected String message_schema;
    protected String messages_schema;

    protected String chat_notifications_schema;

    protected final static String PERMISSION_ERROR = "You don't have appropriate permissions to perform this operation";

    @AfterGroups(groups = {"baseTest"})
    public void setUp() {
        userLogin1 = (String) baseParams.get("userLogin1");
        userLogin2 = (String) baseParams.get("userLogin2");
        userID1 = (int) baseParams.get("userID1");
        userID2 = (int) baseParams.get("userID2");

        try {
            dialog_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(DIALOG_JSON_SCHEMA), "UTF-8");
            dialogs_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(DIALOGS_JSON_SCHEMA), "UTF-8");
            dialog_with_custom_fields_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(DIALOG_WITH_CUSTOM_FIELDS_JSON_SCHEMA), "UTF-8");

            message_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(MESSAGE_JSON_SCHEMA), "UTF-8");
            messages_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(MESSAGES_JSON_SCHEMA), "UTF-8");

            chat_notifications_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(CHAT_NOTIFICATIONS_JSON_SCHEMA), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
