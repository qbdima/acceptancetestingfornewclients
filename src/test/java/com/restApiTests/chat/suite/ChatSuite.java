package com.restApiTests.chat.suite;


import com.restApiTests.chat.providers.NewMessageData;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;


public class ChatSuite extends BaseChatTest {

    @Test(dependsOnGroups = {"baseTest"})
    public void user1CreateGroupDialog() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 2)
                .queryParam("name", "test_REST_chat")
                .queryParam("occupants_ids", userID2)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(dialog_schema))
                .assertThat().body("type", equalTo(2))
                .and()
                .assertThat().body("name", equalTo("test_REST_chat"))
                .assertThat().body("occupants_ids", contains(userID1, userID2)));


        groupDialogId = response.path("_id");
    }

    @Test(dependsOnMethods = {"user1CreateGroupDialog"})
    public void user1CreatePublicGroupDialog() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 1)
                .queryParam("name", "public_REST_chat")
                .queryParam("occupants_ids", 11631209)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(dialog_schema))
                .assertThat().body("type", equalTo(1))
                .assertThat().body("name", equalTo("public_REST_chat")));

        ArrayList occupants = response.path("occupants_ids");

        Assert.assertEquals(occupants.size(), 0);

        publicGroupDialogID = response.path("_id");
    }

    @Test(dependsOnMethods = {"user1CreateGroupDialog"})
    public void user1GetDialogs() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("limit", 100)
                .when()
                .get(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(dialogs_schema)));

        ArrayList dialogs = response.path("items");
        Assert.assertEquals(dialogs.size(), 2);

    }

    @Test(dependsOnMethods = {"user1GetDialogs"})
    public void user1PullOccupants() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("pull_all[occupants_ids][]", userID2)
                .when()
                .put(baseURL + "chat/Dialog/" + groupDialogId + ".json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(dialog_schema)));

        ArrayList occupants = response.path("occupants_ids");

        Assert.assertTrue(occupants.contains(userID1), "Dialog occupants does not contain all expected users");
        Assert.assertFalse(occupants.contains(userID2), "Dialog occupants does not contain all expected users");
        Assert.assertEquals(occupants.size(), 1);
    }

    @Test(dependsOnMethods = {"user1PullOccupants"})
    public void user1PushOccupantsAndUpdateName() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("push_all[occupants_ids][]", userID2)
                .queryParam("name", "updated_chat")
                .when()
                .put(baseURL + "chat/Dialog/" + groupDialogId + ".json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(dialog_schema))
                .assertThat().body("name", equalTo("updated_chat")));

        ArrayList occupants = response.path("occupants_ids");

        Assert.assertTrue(occupants.contains(userID1), "Dialog occupants does not contain all expected users");
        Assert.assertTrue(occupants.contains(userID2), "Dialog occupants does not contain all expected users");
        Assert.assertEquals(occupants.size(), 2);
    }

    @Test(dependsOnMethods = {"user1CreateGroupDialog"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class)
    public void user1СreateMessageForGroupDialog(HashMap message) {

        message.put("chat_dialog_id", groupDialogId);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

        groupChatMessageID = response.path("_id");
    }

    @Test(dependsOnMethods = {"user1СreateMessageForGroupDialog"}, dataProvider = "newMessageForPrivateDialog", dataProviderClass = NewMessageData.class)
    public void user1CreateMessageForPrivateDialog(HashMap message) {

        message.put("recipient_id", userID2);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

        privateDialogId = response.path("chat_dialog_id");
        privateChatMessageID = response.path("_id");
    }

    @Test(dependsOnMethods = {"user1CreateMessageForPrivateDialog"})
    public void user1UpdatePrivateChatMessage() {

        this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", privateDialogId)
                .queryParam("message", "updated private message")
                .when()
                .put(baseURL + "chat/Message/" + privateChatMessageID + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"user1CreateGroupDialog"}, dataProvider = "newMessageForGroupDialogWithAttachment", dataProviderClass = NewMessageData.class)
    public void user1CreateMessageForGroupDialogWithAttachment(HashMap message) {

        message.put("chat_dialog_id", groupDialogId);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

        ArrayList attachments = response.path("attachments");

        Assert.assertEquals(((HashMap) attachments.get(0)).get("id").toString(), ((HashMap) message).get("attachments[0][id]").toString());
        Assert.assertEquals(((HashMap) attachments.get(0)).get("type").toString(), ((HashMap) message).get("attachments[0][type]").toString());
    }

    @Test(dependsOnMethods = {"user1СreateMessageForGroupDialog"})
    public void user1GetMessagesForGroupDialog() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", groupDialogId)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList messages = response.path("items");
        Assert.assertEquals(messages.size(), 2);

    }

    @Test(dependsOnMethods = {"user1GetMessagesForGroupDialog"})
    public void user1GetGroupChatMessageById() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", groupDialogId)
                .queryParam("_id", groupChatMessageID)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        Assert.assertEquals(response.path("items[0]._id"), groupChatMessageID);
        Assert.assertEquals(response.path("items[0].chat_dialog_id"), groupDialogId);
        Assert.assertEquals((int) response.path("items[0].recipient_id"), 0);
        Assert.assertEquals((int) response.path("items[0].read"), 0);
    }

    @Test(dependsOnMethods = {"user1GetGroupChatMessageById"})
    public void user1GetPrivateChatMessageById() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", privateDialogId)
                .queryParam("_id", privateChatMessageID)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        Assert.assertEquals(response.path("items[0]._id"), privateChatMessageID);
        Assert.assertEquals(response.path("items[0].chat_dialog_id"), privateDialogId);
        Assert.assertEquals((int) response.path("items[0].recipient_id"), userID2);
        Assert.assertEquals((int) response.path("items[0].read"), 0);
        Assert.assertEquals(response.path("items[0].message"), "updated private message");
    }

    @Test(dependsOnMethods = {"user1GetPrivateChatMessageById"})
    public void user1DeleteMessageById() {
        super.initSpec();
        this.extractResponseAndPrintLog(spec
                .when()
                .delete(baseURL + "chat/Message/" + privateChatMessageID + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"user1DeleteMessageById"})
    public void user1GetDeletedPrivateChatMessageById() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", privateDialogId)
                .queryParam("_id", privateChatMessageID)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200));

        ArrayList items = response.path("items");
        Assert.assertEquals(items.size(), 0);

    }

    @Test(dependsOnMethods = {"user1GetDeletedPrivateChatMessageById"})
    public void user2GetDeletedPrivateChatMessageById() {

        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", privateDialogId)
                .queryParam("_id", privateChatMessageID)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200));

        ArrayList items = response.path("items");
        Assert.assertEquals(items.size(), 1);

    }

    @Test(dependsOnMethods = {"user1GetPrivateChatMessageById"})
    public void user1DeleteGroupChatMessageByIdUsingForce() {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Message/" + groupChatMessageID + ".json")
                .then()
                .statusCode(200));

    }

    @Test(dependsOnMethods = {"user1DeleteGroupChatMessageByIdUsingForce"})
    public void user2GetDeletedGroupChatMessageById() {

        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", groupDialogId)
                .queryParam("_id", groupChatMessageID)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200));

        ArrayList items = response.path("items");
        Assert.assertEquals(items.size(), 0);

    }

    @Test(dependsOnMethods = {"user1DeleteGroupChatMessageByIdUsingForce"})
    public void user1GetDeletedGroupChatMessageById() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", groupDialogId)
                .queryParam("_id", groupChatMessageID)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200));

        ArrayList items = response.path("items");
        Assert.assertEquals(items.size(), 0);

    }

    @Test(dependsOnMethods = {"user1GetDeletedGroupChatMessageById"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class)
    public void user1CreateMessageForGroupDialog2(HashMap message) {

        message.put("chat_dialog_id", groupDialogId);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));
    }

    @Test(dependsOnMethods = {"user1CreatePublicGroupDialog"}, dataProvider = "newMessageForPublicGroupDialog", dataProviderClass = NewMessageData.class)
    public void user1CreateMessageForPublicGroupDialog(HashMap message) {

        message.put("chat_dialog_id", publicGroupDialogID);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));
    }

    @Test(dependsOnMethods = {"user1CreateMessageForGroupDialog2"})
    public void user2GetUnreadMessageCount() {

        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_ids", groupDialogId + "," + publicGroupDialogID)
                .when()
                .get(baseURL + "chat/Message/unread.json")
                .then()
                .statusCode(200));

        Assert.assertEquals((int) response.path("total"), 2);
        Assert.assertEquals((int) response.path(groupDialogId), 2);
        Assert.assertEquals((int) response.path(publicGroupDialogID), 0);
    }

    @Test(dependsOnMethods = {"user2GetUnreadMessageCount"})
    public void user2GetMessages() {
        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", groupDialogId)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"user2GetMessages"})
    public void user2GetUnreadMessageCountAfterGetMessages() {

        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_ids", groupDialogId + "," + publicGroupDialogID)
                .when()
                .get(baseURL + "chat/Message/unread.json")
                .then()
                .statusCode(200));

        Assert.assertEquals((int) response.path("total"), 0);
        Assert.assertEquals((int) response.path(groupDialogId), 0);
        Assert.assertEquals((int) response.path(publicGroupDialogID), 0);
    }

    @Test(dependsOnMethods = {"user2GetUnreadMessageCountAfterGetMessages"})
    public void user1GetNotificationsSettings() {

        Response response = this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "chat/Dialog/" + groupDialogId + "/notifications.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(chat_notifications_schema)));

        int status = response.path("notifications.enabled");
        Assert.assertEquals(status, 1);
    }

    @Test(dependsOnMethods = {"user1GetNotificationsSettings"})
    public void user1UpdateNotificationsSettings() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("enabled", 0)
                .when()
                .put(baseURL + "chat/Dialog/" + groupDialogId + "/notifications.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(chat_notifications_schema)));

        int status = response.path("notifications.enabled");
        Assert.assertEquals(status, 0);
    }

    @Test(dependsOnMethods = {"user1UpdateNotificationsSettings"}, alwaysRun = true)
    public void user1DeleteGroupDialog() {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + groupDialogId + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"user1UpdateNotificationsSettings"}, alwaysRun = true)
    public void user1DeletePrivateDialog() {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + privateDialogId + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"user1UpdateNotificationsSettings"}, alwaysRun = true)
    public void user1DeletePublicGroupDialog() {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + publicGroupDialogID + ".json")
                .then()
                .statusCode(200));
    }

//    @Test(dependsOnMethods = {"pullOccupants"})
//    public void deleteDialog() throws IOException {
//
//        Response response = this.extractResponseAndPrintLog(spec
//                .queryParam("force", 1)
//                .when()
//                .delete(baseURL + "chat/Dialog/" + dialogId + ".json")
//                .then()
//                .statusCode(200));
//    }

//    @Test(dependsOnMethods = {"deleteDialog"})
//    public void deleteAllDialogs() throws IOException {
//
//        Response response = given()
//                .contentType("application/json")
//                .header("QB-Token", (String) GlobalParams.INSTANCE.get("admin_token"))
//                .queryParam("limit", 100)
//                .when()
//                .get(baseURL + "chat/Dialog.json")
//                .then()
//                .statusCode(200)
//                .extract()
//                .response();
//
//        dialogs = response.path("items");
//
//        for (int i = 0; i < dialogs.size(); i++) {
//            String user_id = (String) ((HashMap)dialogs.get(i)).get("_id");
//            Response response2 =
//                    given()
//                            .contentType("application/json")
//                            .header("QB-Token", (String) GlobalParams.INSTANCE.get("admin_token"))
//                        //    .given()
//                            .queryParam("force", 1)
//                            .when()
//                            .delete(baseURL + "chat/Dialog/" + user_id + ".json")
//                            .then()
//                            .extract()
//                            .response();
//        }
//
//    }

}
