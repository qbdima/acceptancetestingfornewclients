package com.restApiTests.chat.suite;

import com.restApiTests.core.BaseTestWithAdminUser;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.Matchers.equalTo;


public class DialogWithCustomDateField extends BaseTestWithAdminUser {

    protected int userID2;
    private String dialogID;

    @AfterGroups(groups = {"baseTest"})
    public void setUp(){
        userID2 = (int) baseParams.get("userID2");
    }

    private static final String CustomObjectsClass = "TestAPI";

    private Pattern z = Pattern.compile("\\d{4}\\-\\d{2}\\-\\d{2}T\\d{2}\\:\\d{2}\\:\\d{2}Z");

    @Test(dependsOnGroups = {"baseTestWithAdminUser"}, groups = "DialogWithCustomDateField")
    public void createCustomObjectsClass() throws IOException {

        Response response = this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "data/" + CustomObjectsClass + ".json")
                .then());

        if(response.statusCode() == 403){

            this.initSpec("admin_token");

            this.extractResponseAndPrintLog(spec
                    .queryParam("name", CustomObjectsClass)
                    .queryParam("fields[name]", "String")
                    .queryParam("fields[test_date]", "Date")
                    .when()
                    .post(baseURL + "class.json")
                    .then()
                    .statusCode(201));
        }
    }

    @Test(dependsOnMethods = {"createCustomObjectsClass"}, groups = "DialogWithCustomDateField")
    public void createDialogWithCustomDateField() throws IOException {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 2)
                .queryParam("name", "Custom_date_test")
                .queryParam("occupants_ids", userID2)
                .queryParam("data[class_name]", CustomObjectsClass)
                .queryParam("data[test_date]", "2016-04-09T12:04:00")

                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)

                .assertThat().body("type", equalTo(2))
                .assertThat().body("name", equalTo("Custom_date_test")));
        dialogID = response.path("_id");

        String date_format = response.path("data.test_date");
        String tmp = null;
        Matcher x = z.matcher(date_format);
        if (x.find()) {
            tmp = x.group(0);
        }
        Assert.assertNotNull(tmp);
    }

    @Test(dependsOnMethods = {"createDialogWithCustomDateField"}, groups = "DialogWithCustomDateField")
    public void updateDateFieldFullCorrectFormat() throws IOException {
        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("data[class_name]", CustomObjectsClass)
                .queryParam("data[test_date]", "2016-04-09T12:04:00")
                .when()
                .put(baseURL + "chat/Dialog/" + dialogID + ".json")
                .then()
                .statusCode(200)
                .assertThat().body("name", equalTo("Custom_date_test")));
        String date_format = response.path("data.test_date");
        String tmp = null;
        Matcher x = z.matcher(date_format);
        if (x.find()) {
            tmp = x.group(0);
        }
        Assert.assertNotNull(tmp);
    }

    @Test(dependsOnMethods = {"createDialogWithCustomDateField"}, groups = "DialogWithCustomDateField")
    public void updateDateFieldShortFormatWithDash() throws IOException {
        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("data[class_name]", CustomObjectsClass)
                .queryParam("data[test_date]", "2016-04-16")
                .when()
                .put(baseURL + "chat/Dialog/" + dialogID + ".json")
                .then()
                .statusCode(200)
                .assertThat().body("name", equalTo("Custom_date_test")));
        String date_format = response.path("data.test_date");
        String tmp = null;
        Matcher x = z.matcher(date_format);
        if (x.find()) {
            tmp = x.group(0);
        }
        Assert.assertNotNull(tmp);
    }
    @Test(dependsOnMethods = {"createDialogWithCustomDateField"}, groups = "DialogWithCustomDateField")
    public void updateDateFieldShortFormatWithoutSymbols() throws IOException {
        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("data[class_name]", CustomObjectsClass)
                .queryParam("data[test_date]", "20160420")
                .when()
                .put(baseURL + "chat/Dialog/" + dialogID + ".json")
                .then()
                .statusCode(200)
                .assertThat().body("name", equalTo("Custom_date_test")));
        String date_format = response.path("data.test_date");
        String tmp = null;
        Matcher x = z.matcher(date_format);
        if (x.find()) {
            tmp = x.group(0);
        }
        Assert.assertNotNull(tmp);
    }
    @Test(dependsOnMethods = {"createDialogWithCustomDateField"}, groups = "DialogWithCustomDateField")
    public void updateDateFieldShortFormatWithSlash() throws IOException {
        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("data[class_name]", CustomObjectsClass)
                .queryParam("data[test_date]", "2016/04/23")
                .when()
                .put(baseURL + "chat/Dialog/" + dialogID + ".json")
                .then()
                .statusCode(200)
                .assertThat().body("name", equalTo("Custom_date_test")));
        String date_format = response.path("data.test_date");
        String tmp = null;
        Matcher x = z.matcher(date_format);
        if (x.find()) {
            tmp = x.group(0);
        }
        Assert.assertNotNull(tmp);
    }
    @Test(dependsOnMethods = {"createDialogWithCustomDateField"}, groups = "DialogWithCustomDateField")
    public void updateDateFieldShortFormatWithAsterisk() throws IOException {
        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("data[class_name]", CustomObjectsClass)
                .queryParam("data[test_date]", "2016*04*23")
                .when()
                .put(baseURL + "chat/Dialog/" + dialogID + ".json")
                .then()
                .statusCode(200)
                .assertThat().body("name", equalTo("Custom_date_test")));
        Assert.assertEquals(response.path("data.test_date"), "2016*04*23");
    }

    @Test(dependsOnMethods = {"createDialogWithCustomDateField"}, groups = "DialogWithCustomDateField")
    public void updateDateFieldShortFormatWithDots() throws IOException {
        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("data[class_name]", CustomObjectsClass)
                .queryParam("data[test_date]", "2016.04.13")
                .when()
                .put(baseURL + "chat/Dialog/" + dialogID + ".json")
                .then()
                .statusCode(200)
                .assertThat().body("name", equalTo("Custom_date_test")));
        String date_format = response.path("data.test_date");
        String tmp = null;
        Matcher x = z.matcher(date_format);
        if (x.find()) {
            tmp = x.group(0);
        }
        Assert.assertNotNull(tmp);
    }
    @Test(dependsOnMethods = {"createDialogWithCustomDateField"}, groups = "DialogWithCustomDateField")
    public void updateDateFieldWithYearTimeFormat() throws IOException {
        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("data[class_name]", CustomObjectsClass)
                .queryParam("data[test_date]", "2016-04-30 16:40")
                .when()
                .put(baseURL + "chat/Dialog/" + dialogID + ".json")
                .then()
                .statusCode(200)
                .assertThat().body("name", equalTo("Custom_date_test")));
        String date_format = response.path("data.test_date");
        String tmp = null;
        Matcher x = z.matcher(date_format);
        if (x.find()) {
            tmp = x.group(0);
        }
        Assert.assertNotNull(tmp);
    }
    @Test(dependsOnMethods = {"createDialogWithCustomDateField"}, groups = "DialogWithCustomDateField")
    public void updateDateFieldWithYearTimeSecFormat() throws IOException {
        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("data[class_name]", CustomObjectsClass)
                .queryParam("data[test_date]", "2016-04-21 16:40:00")
                .when()
                .put(baseURL + "chat/Dialog/" + dialogID + ".json")
                .then()
                .statusCode(200)
                .assertThat().body("name", equalTo("Custom_date_test")));
        String date_format = response.path("data.test_date");
        String tmp = null;
        Matcher x = z.matcher(date_format);
        if (x.find()) {
            tmp = x.group(0);
        }
        Assert.assertNotNull(tmp);
    }
    @Test(dependsOnMethods = {"createDialogWithCustomDateField"}, groups = "DialogWithCustomDateField")
    public void updateDateFieldFullFormatWithTimezone1() throws IOException {
        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("data[class_name]", CustomObjectsClass)
                .queryParam("data[test_date]", "2016-04-30T15:00:00-0300")
                .when()
                .put(baseURL + "chat/Dialog/" + dialogID + ".json")
                .then()
                .statusCode(200)
                .assertThat().body("name", equalTo("Custom_date_test")));
        Assert.assertEquals(response.path("data.test_date"), "2016-04-30T18:00:00Z");
    }
    @Test(dependsOnMethods = {"createDialogWithCustomDateField"}, groups = "DialogWithCustomDateField")
    public void updateDateFieldFullFormatWithTimezone2() throws IOException {
        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("data[class_name]", CustomObjectsClass)
                .queryParam("data[test_date]", "2016-04-30T15:00:00+0300")
                .when()
                .put(baseURL + "chat/Dialog/" + dialogID + ".json")
                .then()
                .statusCode(200)
                .assertThat().body("name", equalTo("Custom_date_test")));
        Assert.assertEquals(response.path("data.test_date"), "2016-04-30T12:00:00Z");
    }

    @Test(dependsOnGroups = {"DialogWithCustomDateField"}, alwaysRun = true)
    public void user1DeleteGroupDialog() throws IOException {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + dialogID + ".json")
                .then()
                .statusCode(200));
    }
}