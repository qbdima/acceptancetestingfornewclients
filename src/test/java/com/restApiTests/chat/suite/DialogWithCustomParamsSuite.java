package com.restApiTests.chat.suite;



import com.restApiTests.core.BaseTestWithAdminUser;
import io.restassured.response.Response;
import org.apache.commons.io.IOUtils;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;

public class DialogWithCustomParamsSuite extends BaseTestWithAdminUser {

    private static final String CustomObjectsClass = "CustomClassForDialog";

    protected String userLogin1;
    protected String userLogin2;
    protected int userID1;
    protected int userID2;
    private String groupDialogID;
    private String groupDialogIDForCheckLocationField;
    private String dialogIdForSearchUsingNear;

    @AfterGroups(groups = {"baseTest"})
    public void setUp(){
        userLogin1 = (String) baseParams.get("userLogin1");
        userLogin2 = (String) baseParams.get("userLogin2");
        userID1 = (int) baseParams.get("userID1");
        userID2 = (int) baseParams.get("userID2");
    }

    @Test(dependsOnGroups = {"baseTestWithAdminUser"})
    public void user1InitCustomObjectsClass() throws IOException {

        Response response = this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "data/" + CustomObjectsClass + ".json")
                .then());

        if(response.statusCode() == 403){

            this.initSpec("admin_token");

            this.extractResponseAndPrintLog(spec
                    .queryParam("name", CustomObjectsClass)
                    .queryParam("fields[integer]", "Integer")
                    .queryParam("fields[float]", "Float")
                    .queryParam("fields[boolean]", "Boolean")
                    .queryParam("fields[string]", "String")
                    .queryParam("fields[file]", "File")
                    .queryParam("fields[position]", "Location")
                    .queryParam("fields[date]", "Date")
                    .queryParam("fields[integer_array]", "Integer_a")
                    .queryParam("fields[float_array]", "Float_a")
                    .queryParam("fields[boolean_array]", "Boolean_a")
                    .queryParam("fields[string_array]", "String_a")
                    .queryParam("permissions[create][access]", "open")
                    .queryParam("permissions[read][access]", "open")
                    .queryParam("permissions[update][access]", "open")
                    .when()
                    .post(baseURL + "class.json")
                    .then()
                    .statusCode(201));
        }
    }

    @Test(dependsOnMethods = {"user1InitCustomObjectsClass"})
    public void user1CreateDialogWithCustomParamsForCheckLocationField() throws IOException {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 2)
                .queryParam("name", "Custom_data_test_without_location")
                .queryParam("occupants_ids", userID2)
                .queryParam("data[class_name]", CustomObjectsClass)
                .queryParam("data[string]", "Custom_data_test_without_location")
                .queryParam("data[boolean]", true)
                .queryParam("data[integer]", 1233333)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
                .assertThat().body("type", equalTo(2))
                .assertThat().body("name", equalTo("Custom_data_test_without_location")));

        Assert.assertEquals(response.path("data.string"), "Custom_data_test_without_location");
        Assert.assertEquals((boolean)response.path("data.boolean"), true);
        Assert.assertEquals((int)response.path("data.integer"), 1233333);
        Assert.assertEquals(response.path("data.class_name"), CustomObjectsClass);
        HashMap customData = response.path("data");
        Assert.assertEquals(customData.size(), 4);

        ArrayList locations = response.path("data.position");
        Assert.assertNull(locations);

        groupDialogIDForCheckLocationField = response.path("_id");

    }

    @Test(dependsOnMethods = {"user1InitCustomObjectsClass"})
    public void user1CreateDialogWithCustomParams() throws IOException {
        String schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(DIALOG_JSON_SCHEMA));

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 2)
                .queryParam("name", "Custom_data_test")
                .queryParam("occupants_ids", userID2)
                .queryParam("data[class_name]", CustomObjectsClass)
                .queryParam("data[string]", "My_dialog")
                .queryParam("data[position]", "16.5,36.8")
                .queryParam("data[boolean]", true)
                .queryParam("data[integer]", 1233333)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
     //           .assertThat().body(matchesJsonSchema(schema))
                .assertThat().body("type", equalTo(2))
                .assertThat().body("name", equalTo("Custom_data_test")));

        Assert.assertEquals(response.path("data.string"), "My_dialog");
        Assert.assertEquals((boolean)response.path("data.boolean"), true);
        Assert.assertEquals((int)response.path("data.integer"), 1233333);
        Assert.assertEquals(response.path("data.class_name"), CustomObjectsClass);

        ArrayList locations = response.path("data.position");
        Assert.assertTrue(locations.contains(16.5f));
        Assert.assertTrue(locations.contains(36.8f));
        Assert.assertEquals(locations.size(), 2);

        groupDialogID = response.path("_id");
    }

    @Test(dependsOnMethods = {"user1CreateDialogWithCustomParams"})
    public void user1UpdateDialogWithCustomParamsUsingIncorrectData(){

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("data[class_name]", CustomObjectsClass)
                .queryParam("data[string]", "My_updated_dialog")
                .queryParam("data[position]", "112.5,156.8")
                .queryParam("data[boolean]", false)
                .queryParam("data[integer]", 1)
                .when()
                .put(baseURL + "chat/Dialog/" + groupDialogID + ".json")
                .then()
                .statusCode(200)
                //           .assertThat().body(matchesJsonSchema(schema))
                .assertThat().body("type", equalTo(2))
                .assertThat().body("name", equalTo("Custom_data_test")));

        Assert.assertEquals(response.path("data.string"), "My_updated_dialog");
        Assert.assertEquals((boolean)response.path("data.boolean"), false);
        Assert.assertEquals((int)response.path("data.integer"), 1);
        Assert.assertEquals(response.path("data.class_name"), CustomObjectsClass);

        ArrayList locations = response.path("data.position");
        Assert.assertTrue(locations.contains(112.5f));
        Assert.assertTrue(locations.contains(156.8f));
        Assert.assertEquals(locations.size(), 2);
    }

    @Test(dependsOnMethods = {"user1UpdateDialogWithCustomParamsUsingIncorrectData"})
    public void user1UpdateDialogWithCustomParams(){

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("data[class_name]", CustomObjectsClass)
                .queryParam("data[string]", "My_updated_dialog")
                .queryParam("data[position]", "12.5,56.8")
                .queryParam("data[boolean]", false)
                .queryParam("data[integer]", 1)
                .when()
                .put(baseURL + "chat/Dialog/" + groupDialogID + ".json")
                .then()
                .statusCode(200)
                //           .assertThat().body(matchesJsonSchema(schema))
                .assertThat().body("type", equalTo(2))
                .assertThat().body("name", equalTo("Custom_data_test")));

        Assert.assertEquals(response.path("data.string"), "My_updated_dialog");
        Assert.assertEquals((boolean)response.path("data.boolean"), false);
        Assert.assertEquals((int)response.path("data.integer"), 1);
        Assert.assertEquals(response.path("data.class_name"), CustomObjectsClass);

        ArrayList locations = response.path("data.position");
        Assert.assertTrue(locations.contains(12.5f));
        Assert.assertTrue(locations.contains(56.8f));
        Assert.assertEquals(locations.size(), 2);
    }

    @Test(dependsOnMethods = {"user1InitCustomObjectsClass"})
    public void user1CreateDialogWithCustomParamsLocationsType() throws IOException {
        String schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(DIALOG_WITH_CUSTOM_FIELDS_JSON_SCHEMA));

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 2)
                .queryParam("name", "Custom_data_locations_test")
                .queryParam("occupants_ids", userID2)
                .queryParam("data[class_name]", CustomObjectsClass)
                .queryParam("data[position]", "20.5,15.8")
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(schema))
                .assertThat().body("type", equalTo(2))
                .assertThat().body("name", equalTo("Custom_data_locations_test")));

        Assert.assertEquals(response.path("data.class_name"), CustomObjectsClass);

        ArrayList locations = response.path("data.position");
        Assert.assertTrue(locations.contains(20.5f));
        Assert.assertTrue(locations.contains(15.8f));
        Assert.assertEquals(locations.size(), 2);

        dialogIdForSearchUsingNear = response.path("_id");
    }

    @Test(dependsOnMethods = {"user1CreateDialogWithCustomParamsLocationsType", "user1UpdateDialogWithCustomParams"})
    public void getDialogsWithFilteringByLocaions(){
        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("data[class_name]", CustomObjectsClass)
                .queryParam("data[position][near]", "20.4,15.8;100000")
                .queryParam("type", 2)
                .when()
                .get(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(200));

        ArrayList<HashMap <String, Object>> items = response.path("items");

        //all dialogs should return. filter NEAR should be ignore
        Assert.assertEquals((int)response.path("total_entries"), 3, "total_entries param has incorrect value");
        Assert.assertEquals(items.size(), 3, "response object contain incorrect items count");
        boolean contains = true;
        for(HashMap item : items){
            contains &= item.containsValue(dialogIdForSearchUsingNear) || item.containsValue(groupDialogID)
                    || item.containsValue(groupDialogIDForCheckLocationField);
        }
        Assert.assertTrue(contains, "items array does not contain expected dialogs");
    }

    @Test(dependsOnMethods = {"getDialogsWithFilteringByLocaions"}, alwaysRun = true)
    public void user1DeleteGroupDialog() throws IOException {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + groupDialogID + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"getDialogsWithFilteringByLocaions"}, alwaysRun = true)
    public void user1DeleteGroupDialog2() throws IOException {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + dialogIdForSearchUsingNear + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"getDialogsWithFilteringByLocaions"}, alwaysRun = true)
    public void user1DeleteGroupDialogWithCustomParamsForCheckLocationField() throws IOException {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + groupDialogIDForCheckLocationField + ".json")
                .then()
                .statusCode(200));
    }
}
