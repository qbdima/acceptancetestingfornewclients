package com.restApiTests.chat.suite;


import com.restApiTests.chat.providers.NewMessageData;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;

public class OperationsWithFiltersForDialogsSuite extends BaseChatTest {

    @Test(dependsOnGroups = {"baseTest"})
    public void user1CreateGroupDialog()  {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 2)
                .queryParam("name", "filters_test")
                .queryParam("occupants_ids", userID2)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(dialog_schema))
                .assertThat().body("type", equalTo(2))
                .assertThat().body("name", equalTo("filters_test")));

        dialogId = response.path("_id");
    }

    @Test(dependsOnGroups = {"baseTest"})
    public void user1CreateGroupDialog2()  {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 2)
                .queryParam("name", "filters_test2")
                .queryParam("occupants_ids", userID2)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(dialog_schema))
                .assertThat().body("type", equalTo(2))
                .assertThat().body("name", equalTo("filters_test2")));

        dialogId2 = response.path("_id");
    }

    @Test(dependsOnGroups = {"baseTest"})
    public void user1CreatePublicGroupDialog()  {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 1)
                .queryParam("name", "public_REST_chat")
                .queryParam("occupants_ids", 11631209)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(dialog_schema))
                .assertThat().body("type", equalTo(1))
                .assertThat().body("name", equalTo("public_REST_chat")));

        ArrayList occupants = response.path("occupants_ids");

        Assert.assertEquals(occupants.size(), 0);

        publicGroupDialogID = response.path("_id");
    }

    @Test(dependsOnGroups = {"baseTest"})
    public void user1CreatePrivateDialog()  {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 3)
                .queryParam("occupants_ids", userID2)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(dialog_schema))
                .assertThat().body("type", equalTo(3))
                .assertThat().body("name", equalTo(userLogin2))
                .assertThat().body("xmpp_room_jid", equalTo(null)));

        privateDialogId = response.path("_id");

    }

    @Test(dependsOnMethods = {"user1CreatePublicGroupDialog","user1CreatePrivateDialog","user1CreateGroupDialog","user1CreateGroupDialog2"}, enabled = false)
    public void user1GetDialogsUsingFilterLT()  {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type[lt]", 2)
                .when()
                .get(baseURL + "chat/Dialog.json")
                .then()
                //  .assertThat().body(matchesJsonSchema(schema))
                .statusCode(200));

        ArrayList<Map<String, Object>> dialogs = response.path("items");

        Assert.assertEquals(dialogs.size(), 1);

        for (Map<String, Object> dialog : dialogs){
            Assert.assertTrue(dialog.get("type").equals(1), "Dialogs list contains dialogs with not appropriate type");
        }

    }

    @Test(dependsOnMethods = {"user1CreatePublicGroupDialog","user1CreatePrivateDialog","user1CreateGroupDialog","user1CreateGroupDialog2"}, enabled = false)
    public void user1GetDialogsUsingFilterLTE()  {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type[lte]", 2)
                .when()
                .get(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(dialogs_schema)));

        ArrayList<Map<String, Object>> dialogs = response.path("items");

        Assert.assertEquals(dialogs.size(), 3);

        for (Map<String, Object> dialog : dialogs){
            Assert.assertTrue(dialog.get("type").equals(1) || dialog.get("type").equals(2), "Dialogs list contains dialogs with not appropriate type");
        }

    }

    @Test(dependsOnMethods = {"user1CreatePublicGroupDialog","user1CreatePrivateDialog","user1CreateGroupDialog","user1CreateGroupDialog2"}, enabled = false)
    public void user1GetDialogsUsingFilterGT()  {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type[gt]", 2)
                .when()
                .get(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(dialogs_schema)));

        ArrayList<Map<String, Object>> dialogs = response.path("items");

        Assert.assertEquals(dialogs.size(), 1);

        for (Map<String, Object> dialog : dialogs){
            Assert.assertTrue(dialog.get("type").equals(3), "Dialogs list contains dialogs with not appropriate type");
        }

    }

    @Test(dependsOnMethods = {"user1CreatePublicGroupDialog","user1CreatePrivateDialog","user1CreateGroupDialog","user1CreateGroupDialog2"}, enabled = false)
    public void user1GetDialogsUsingFilterGTE()  {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type[gte]", 2)
                .when()
                .get(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(dialogs_schema)));

        ArrayList<Map<String, Object>> dialogs = response.path("items");

        Assert.assertEquals(dialogs.size(), 3);

        for (Map<String, Object> dialog : dialogs){
            Assert.assertTrue(dialog.get("type").equals(2) || dialog.get("type").equals(3), "Dialogs list contains dialogs with not appropriate type");
        }

    }

    @Test(dependsOnMethods = {"user1CreatePublicGroupDialog","user1CreatePrivateDialog","user1CreateGroupDialog","user1CreateGroupDialog2"}, enabled = false)
    public void user1GetDialogsUsingFilterNE()  {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type[ne]", 2)
                .when()
                .get(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(dialogs_schema)));

        ArrayList<Map<String, Object>> dialogs = response.path("items");

        Assert.assertEquals(dialogs.size(), 2);

        for (Map<String, Object> dialog : dialogs){
            Assert.assertTrue(dialog.get("type").equals(1) || dialog.get("type").equals(3), "Dialogs list contains dialogs with not appropriate type");
        }

    }


    @Test(dependsOnMethods = {"user1CreatePublicGroupDialog","user1CreatePrivateDialog","user1CreateGroupDialog","user1CreateGroupDialog2"})
    public void user1GetDialogsUsingFilterIN()  {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type[in]", "1,3")
                .when()
                .get(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(dialogs_schema)));

        ArrayList<Map<String, Object>> dialogs = response.path("items");

        Assert.assertEquals(dialogs.size(), 2);

        for (Map<String, Object> dialog : dialogs){
            Assert.assertTrue(dialog.get("type").equals(1) || dialog.get("type").equals(3), "Dialogs list contains dialogs with not appropriate type");
        }

    }

    @Test(dependsOnMethods = {"user1CreatePublicGroupDialog","user1CreatePrivateDialog","user1CreateGroupDialog","user1CreateGroupDialog2"}, enabled = false)
    public void user1GetDialogsUsingFilterNIN()  {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type[nin]", "1,3")
                .when()
                .get(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(dialogs_schema)));

        ArrayList<Map<String, Object>> dialogs = response.path("items");

        Assert.assertEquals(dialogs.size(), 2);

        for (Map<String, Object> dialog : dialogs){
            Assert.assertTrue(dialog.get("type").equals(2), "Dialogs list contains dialogs with not appropriate type");
        }

    }

    @Test(dependsOnMethods = {"user1CreatePublicGroupDialog", "user1CreatePrivateDialog", "user1CreateGroupDialog", "user1CreateGroupDialog2"}, enabled = false)
    public void user1GetDialogsUsingFilterALL()  {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type[all]", "1,3")
                .when()
                .get(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(dialogs_schema)));

        ArrayList<Map<String, Object>> dialogs = response.path("items");

        Assert.assertEquals(dialogs.size(), 2);

        for (Map<String, Object> dialog : dialogs){
            Assert.assertTrue(dialog.get("type").equals(2), "Dialogs list contains dialogs with not appropriate type");
        }

    }

    @Test(dependsOnMethods = {"user1CreatePublicGroupDialog", "user1CreatePrivateDialog", "user1CreateGroupDialog", "user1CreateGroupDialog2"})
    public void user1GetDialogsUsingDialogName()  {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("name", "public_REST_chat")
                .when()
                .get(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(200)
                .assertThat().body("items[0].type", equalTo(1))
                .assertThat().body("items[0].name", equalTo("public_REST_chat"))
                .assertThat().body(matchesJsonSchema(dialogs_schema)));

        ArrayList<Map<String, Object>> dialogs = response.path("items");

        Assert.assertEquals(dialogs.size(), 1);

    }

    @Test(dependsOnMethods = {"user1CreateGroupDialog"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class)
    public void createMessageForGroupDialog(HashMap message)  {

        message.put("chat_dialog_id", dialogId);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

        groupChatMessageID = response.path("_id");
    }

    @Test(dependsOnMethods = {"user1CreatePublicGroupDialog"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class)
    public void createMessageForPublicGroupDialog(HashMap message)  {

        message.put("chat_dialog_id", publicGroupDialogID);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

        groupChatMessageID = response.path("_id");
    }

    @Test(dependsOnMethods = {"user1CreateGroupDialog2"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class)
    public void createMessageForGroupDialog2(HashMap message)  {

        message.put("chat_dialog_id", dialogId2);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

        groupChatMessageID = response.path("_id");
    }

    @Test(dependsOnMethods = {"user1CreatePrivateDialog"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class)
    public void createMessageForPrivateDialog(HashMap message)  {

        message.put("chat_dialog_id", privateDialogId);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

        groupChatMessageID = response.path("_id");
    }

    @Test(dependsOnMethods = {"createMessageForPrivateDialog", "createMessageForGroupDialog2", "createMessageForGroupDialog", "createMessageForPublicGroupDialog"})
    public void user1GetDialogsUsingSortDesc()  {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("sort_desc", "last_message_date_sent")
                .when()
                .get(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(dialogs_schema)));

        ArrayList<Map<String, Object>> dialogs = response.path("items");

        Assert.assertEquals(dialogs.size(), 4);

        for (int i = 0; i < dialogs.size() - 1; i++){
            boolean tmp = false;
            if ((Integer)dialogs.get(i).get("last_message_date_sent") >= (Integer)dialogs.get(i+1).get("last_message_date_sent")){
                tmp = true;
            }
            Assert.assertTrue(tmp, "Dialogs sort work wrong");
        }

    }

    @Test(dependsOnMethods = {"createMessageForPrivateDialog", "createMessageForGroupDialog2", "createMessageForGroupDialog", "createMessageForPublicGroupDialog"})
    public void user1GetDialogsUsingSortAsc()  {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("sort_asc", "last_message_date_sent")
                .when()
                .get(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(dialogs_schema)));

        ArrayList<Map<String, Object>> dialogs = response.path("items");

        Assert.assertEquals(dialogs.size(), 4);

        for (int i = 0; i < dialogs.size() - 1; i++){
            boolean tmp = false;
            if ((Integer)dialogs.get(i).get("last_message_date_sent") <= (Integer)dialogs.get(i+1).get("last_message_date_sent")){
                tmp = true;
            }
            Assert.assertTrue(tmp, "Dialogs sort work wrong");
        }

    }

    @Test(dependsOnMethods = {"user1GetDialogsUsingSortDesc"}, alwaysRun = true)
    public void user1DeleteGroupDialog()  {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + dialogId + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"user1GetDialogsUsingSortDesc"}, alwaysRun = true)
    public void user1DeleteGroupDialog2()  {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + dialogId2 + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"user1GetDialogsUsingSortDesc"}, alwaysRun = true)
    public void user1DeletePrivateDialog()  {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + privateDialogId + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"user1GetDialogsUsingSortDesc"}, alwaysRun = true)
    public void user1DeletePublicGroupDialog()  {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + publicGroupDialogID + ".json")
                .then()
                .statusCode(200));
    }

}
