package com.restApiTests.chat.suite;


import com.restApiTests.chat.providers.NewMessageData;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;

public class OperationsWithFiltersForMessagesSuite extends BaseChatTest {

    @Test(dependsOnGroups = {"baseTest"})
    public void user1CreateGroupDialog() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 2)
                .queryParam("name", "filters_test")
                .queryParam("occupants_ids", userID2)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(dialog_schema))
                .assertThat().body("type", equalTo(2))
                .assertThat().body("name", equalTo("filters_test")));

        dialogId = response.path("_id");
    }

    @Test(dependsOnMethods = {"user1CreateGroupDialog"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class)
    public void createMessageForGroupDialog(HashMap message) {

        message.put("chat_dialog_id", dialogId);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

        groupChatMessageID = response.path("_id");
    }

    @Test(dependsOnMethods = {"user1CreateGroupDialog"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class)
    public void createMessageForGroupDialog2(HashMap message) {

        message.put("chat_dialog_id", dialogId);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

        groupChatMessageID = response.path("_id");
    }

    @Test(dependsOnMethods = {"user1CreateGroupDialog"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class)
    public void createMessageForGroupDialog3(HashMap message) {

        this.initSpec("token2");

        message.put("chat_dialog_id", dialogId);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

        groupChatMessageID = response.path("_id");
    }

    @Test(dependsOnMethods = {"createMessageForGroupDialog", "createMessageForGroupDialog2", "createMessageForGroupDialog3"})
    public void user1GetMessagesUsingSortDesc() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("sort_desc", "date_sent")
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList<Map<String, Object>> messages = response.path("items");

        Assert.assertEquals(messages.size(), 3);

        for (int i = 0; i < messages.size() - 1; i++) {
            boolean tmp = false;
            if ((Integer) messages.get(i).get("date_sent") >= (Integer) messages.get(i + 1).get("date_sent")) {
                tmp = true;
            }
            Assert.assertTrue(tmp, "Messages sort work wrong");
        }

    }

    @Test(dependsOnMethods = {"createMessageForGroupDialog", "createMessageForGroupDialog2", "createMessageForGroupDialog3"})
    public void user1GetMessagesUsingSortAsc() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("sort_asc", "date_sent")
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList<Map<String, Object>> messages = response.path("items");

        Assert.assertEquals(messages.size(), 3);

        for (int i = 0; i < messages.size() - 1; i++) {
            boolean tmp = false;
            if ((Integer) messages.get(i).get("date_sent") <= (Integer) messages.get(i + 1).get("date_sent")) {
                tmp = true;
            }
            Assert.assertTrue(tmp, "Messages sort work wrong");
        }

    }

    @Test(dependsOnMethods = {"createMessageForGroupDialog", "createMessageForGroupDialog2", "createMessageForGroupDialog3"})
    public void user1GetMessagesUsingFilterIN() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("sender_id[in]", "1,3," + userID2)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList<Map<String, Object>> messages = response.path("items");

        Assert.assertEquals(messages.size(), 1);

        for (Map<String, Object> message : messages) {
            Assert.assertTrue(message.get("sender_id").equals(userID2), "Messages list contains messages with not appropriate sender_id");
        }

    }

    @Test(dependsOnMethods = {"createMessageForGroupDialog", "createMessageForGroupDialog2", "createMessageForGroupDialog3"})
    public void user1GetMessagesUsingFilterNIN() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("sender_id[nin]", userID2)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList<Map<String, Object>> messages = response.path("items");

        Assert.assertEquals(messages.size(), 2);

        for (Map<String, Object> message : messages) {
            Assert.assertTrue(message.get("sender_id").equals(userID1), "Messages list contains messages with not appropriate sender_id");
        }

    }

    @Test(dependsOnMethods = {"createMessageForGroupDialog", "createMessageForGroupDialog2", "createMessageForGroupDialog3"})
    public void user1GetMessagesUsingFilterLT() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("sender_id[lt]", userID2)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList<Map<String, Object>> messages = response.path("items");

        Assert.assertEquals(messages.size(), 2);

        for (Map<String, Object> message : messages) {
            Assert.assertTrue(message.get("sender_id").equals(userID1), "Messages list contains messages with not appropriate sender_id");
        }

    }

    @Test(dependsOnMethods = {"createMessageForGroupDialog", "createMessageForGroupDialog2", "createMessageForGroupDialog3"})
    public void user1GetMessagesUsingFilterLTE() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("sender_id[lte]", userID2)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList<Map<String, Object>> messages = response.path("items");

        Assert.assertEquals(messages.size(), 3);

        for (Map<String, Object> message : messages) {
            Assert.assertTrue(message.get("sender_id").equals(userID1) || message.get("sender_id").equals(userID2), "Messages list contains messages with not appropriate sender_id");
        }

    }

    @Test(dependsOnMethods = {"createMessageForGroupDialog", "createMessageForGroupDialog2", "createMessageForGroupDialog3"})
    public void user1GetMessagesUsingFilterGT() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("sender_id[gt]", userID1)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList<Map<String, Object>> messages = response.path("items");

        Assert.assertEquals(messages.size(), 1);

        for (Map<String, Object> message : messages) {
            Assert.assertTrue(message.get("sender_id").equals(userID2), "Messages list contains messages with not appropriate sender_id");
        }

    }

    @Test(dependsOnMethods = {"createMessageForGroupDialog", "createMessageForGroupDialog2", "createMessageForGroupDialog3"})
    public void user1GetMessagesUsingFilterGTE() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("sender_id[gte]", userID1)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList<Map<String, Object>> messages = response.path("items");

        Assert.assertEquals(messages.size(), 3);

        for (Map<String, Object> message : messages) {
            Assert.assertTrue(message.get("sender_id").equals(userID1) || message.get("sender_id").equals(userID2), "Messages list contains messages with not appropriate sender_id");
        }

    }

    @Test(dependsOnMethods = {"createMessageForGroupDialog", "createMessageForGroupDialog2", "createMessageForGroupDialog3"})
    public void user1GetMessagesBySenderID() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("sender_id", userID1)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList<Map<String, Object>> messages = response.path("items");

        Assert.assertEquals(messages.size(), 2);

        for (Map<String, Object> message : messages) {
            Assert.assertTrue(message.get("sender_id").equals(userID1), "Messages list contains messages with not appropriate sender_id");
        }

    }

    @Test(dependsOnMethods = {"createMessageForGroupDialog", "createMessageForGroupDialog2", "createMessageForGroupDialog3"})
    public void user1CountForGetMessagesBySenderID() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("sender_id", userID1)
                .queryParam("count", 1)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200));

        Assert.assertEquals((int) response.path("items.count"), 2);

    }

    @Test(dependsOnMethods = {"user1GetMessagesUsingFilterIN"}, alwaysRun = true)
    public void user1DeleteGroupDialog() {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + dialogId + ".json")
                .then()
                .statusCode(200));
    }

}
