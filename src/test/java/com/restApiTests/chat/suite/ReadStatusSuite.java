package com.restApiTests.chat.suite;


import com.restApiTests.chat.providers.NewMessageData;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;

public class ReadStatusSuite extends BaseChatTest {

    private String messageID;

    @Test(dependsOnGroups = {"baseTest"})
    public void user1CreatePrivateDialog() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 3)
                .queryParam("occupants_ids", userID2)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(dialog_schema))
                .assertThat().body("type", equalTo(3))
                .assertThat().body("name", equalTo(userLogin2))
                .assertThat().body("xmpp_room_jid", equalTo(null)));

        dialogId = response.path("_id");
    }

    @Test(dependsOnMethods = {"user1CreatePrivateDialog"}, dataProvider = "newMessageForPrivateDialog", dataProviderClass = NewMessageData.class)
    public void user1CreateMessageForPrivateDialog(HashMap message) {

        message.put("recipient_id", userID2);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema))
                .assertThat().body("chat_dialog_id", equalTo(dialogId))
                .assertThat().body("read", equalTo(0))
                .assertThat().body("recipient_id", equalTo(userID2))
                .assertThat().body("sender_id", equalTo(userID1)));

        ArrayList delivered_ids = response.path("delivered_ids");
        ArrayList read_ids = response.path("read_ids");

        Assert.assertEquals(delivered_ids.size(), 1);
        Assert.assertEquals(delivered_ids.get(0), userID1);
        Assert.assertEquals(read_ids.size(), 1);
        Assert.assertEquals(read_ids.get(0), userID1);

        messageID = response.path("_id");
    }

    @Test(dependsOnMethods = {"user1CreateMessageForPrivateDialog"})
    public void user2GetChatMessageByIdAndMarkMessageAsUnread() {

        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("_id", messageID)
                .queryParam("mark_as_read", 0)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList read_ids = response.path("items[0].read_ids");
        ArrayList delivered_ids = response.path("items[0].delivered_ids");

        Assert.assertEquals(delivered_ids.size(), 1);

        Assert.assertEquals(read_ids.size(), 1, "Array read_ids not contain expected 1 user id");
        Assert.assertTrue(read_ids.contains(userID1));
        Assert.assertFalse(read_ids.contains(userID2));

        Assert.assertEquals(response.path("items[0]._id"), messageID);
        Assert.assertEquals(response.path("items[0].chat_dialog_id"), dialogId);
        Assert.assertEquals((int) response.path("items[0].sender_id"), userID1);
        Assert.assertEquals((int) response.path("items[0].recipient_id"), userID2);
        Assert.assertEquals((int) response.path("items[0].read"), 1);
    }

    @Test(dependsOnMethods = {"user2GetChatMessageByIdAndMarkMessageAsUnread"})
    public void user1GetChatMessageByIdWhenUser2MarkMessageAsUnread() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("_id", messageID)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList read_ids = response.path("items[0].read_ids");
        ArrayList delivered_ids = response.path("items[0].delivered_ids");

        Assert.assertEquals(delivered_ids.size(), 2);

        Assert.assertEquals(read_ids.size(), 1, "Array read_ids not contain expected 1 user ids");
        Assert.assertTrue(read_ids.contains(userID1));
        Assert.assertFalse(read_ids.contains(userID2));

        Assert.assertEquals(response.path("items[0]._id"), messageID);
        Assert.assertEquals(response.path("items[0].chat_dialog_id"), dialogId);
        Assert.assertEquals((int) response.path("items[0].sender_id"), userID1);
        Assert.assertEquals((int) response.path("items[0].recipient_id"), userID2);
        Assert.assertEquals((int) response.path("items[0].read"), 0);
    }

    @Test(dependsOnMethods = {"user1GetChatMessageByIdWhenUser2MarkMessageAsUnread"})
    public void user2GetChatMessageById() {

        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("_id", messageID)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList read_ids = response.path("items[0].read_ids");
        ArrayList delivered_ids = response.path("items[0].delivered_ids");

        Assert.assertEquals(delivered_ids.size(), 2);

        Assert.assertEquals(read_ids.size(), 1, "Array read_ids not contain expected 1 user id");
        Assert.assertTrue(read_ids.contains(userID1));
        Assert.assertFalse(read_ids.contains(userID2));

        Assert.assertEquals(response.path("items[0]._id"), messageID);
        Assert.assertEquals(response.path("items[0].chat_dialog_id"), dialogId);
        Assert.assertEquals((int) response.path("items[0].sender_id"), userID1);
        Assert.assertEquals((int) response.path("items[0].recipient_id"), userID2);
        Assert.assertEquals((int) response.path("items[0].read"), 1);
    }

    @Test(dependsOnMethods = {"user2GetChatMessageById"})
    public void user1GetChatMessageById() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("_id", messageID)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList read_ids = response.path("items[0].read_ids");
        ArrayList delivered_ids = response.path("items[0].delivered_ids");

        Assert.assertEquals(delivered_ids.size(), 2);

        Assert.assertEquals(read_ids.size(), 2, "Array read_ids not contain expected 2 user ids");
        Assert.assertTrue(read_ids.contains(userID1));
        Assert.assertTrue(read_ids.contains(userID2));

        Assert.assertEquals(response.path("items[0]._id"), messageID);
        Assert.assertEquals(response.path("items[0].chat_dialog_id"), dialogId);
        Assert.assertEquals((int) response.path("items[0].sender_id"), userID1);
        Assert.assertEquals((int) response.path("items[0].recipient_id"), userID2);
        Assert.assertEquals((int) response.path("items[0].read"), 1);
    }

    @Test(dependsOnMethods = {"user1GetChatMessageById"}, alwaysRun = true)
    public void user1DeletePrivateDialog() {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + dialogId + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"user1DeletePrivateDialog"})
    public void user1CreateGroupDialog() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 2)
                .queryParam("name", "check_read_status")
                .queryParam("occupants_ids", userID2)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(dialog_schema))
                .assertThat().body("type", equalTo(2))
                .assertThat().body("name", equalTo("check_read_status")));

        dialogId = response.path("_id");

    }

    @Test(dependsOnMethods = {"user1CreateGroupDialog"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class)
    public void user1CreateMessageForGroupDialog(HashMap message) {

        message.put("chat_dialog_id", dialogId);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema))
                .assertThat().body("chat_dialog_id", equalTo(dialogId))
                .assertThat().body("read", equalTo(0))
                .assertThat().body("recipient_id", equalTo(0))
                .assertThat().body("sender_id", equalTo(userID1)));

        ArrayList delivered_ids = response.path("delivered_ids");
        ArrayList read_ids = response.path("read_ids");

        Assert.assertEquals(delivered_ids.size(), 1);
        Assert.assertEquals(delivered_ids.get(0), userID1);
        Assert.assertEquals(read_ids.size(), 1);
        Assert.assertEquals(read_ids.get(0), userID1);

        groupChatMessageID = response.path("_id");
    }

    @Test(dependsOnMethods = {"user1CreateMessageForGroupDialog"})
    public void user2GetGroupChatMessageById() {

        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("_id", groupChatMessageID)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList read_ids = response.path("items[0].read_ids");
        ArrayList delivered_ids = response.path("items[0].delivered_ids");

        Assert.assertEquals(delivered_ids.size(), 1);

        Assert.assertEquals(read_ids.size(), 1, "Array read_ids not contain expected 1 user id");
        Assert.assertTrue(read_ids.contains(userID1));
        Assert.assertFalse(read_ids.contains(userID2));

        Assert.assertEquals(response.path("items[0]._id"), groupChatMessageID);
        Assert.assertEquals(response.path("items[0].chat_dialog_id"), dialogId);
        Assert.assertEquals((int) response.path("items[0].sender_id"), userID1);
        Assert.assertEquals((int) response.path("items[0].recipient_id"), 0);
        Assert.assertEquals((int) response.path("items[0].read"), 0);
    }

    @Test(dependsOnMethods = {"user2GetGroupChatMessageById"})
    public void user1GetGroupChatMessageById() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("_id", groupChatMessageID)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList read_ids = response.path("items[0].read_ids");
        ArrayList delivered_ids = response.path("items[0].delivered_ids");

        Assert.assertEquals(delivered_ids.size(), 2);

        Assert.assertEquals(read_ids.size(), 2, "Array read_ids not contain expected 2 user id");
        Assert.assertTrue(read_ids.contains(userID1));
        Assert.assertTrue(read_ids.contains(userID2));

        Assert.assertEquals(response.path("items[0]._id"), groupChatMessageID);
        Assert.assertEquals(response.path("items[0].chat_dialog_id"), dialogId);
        Assert.assertEquals((int) response.path("items[0].sender_id"), userID1);
        Assert.assertEquals((int) response.path("items[0].recipient_id"), 0);
        Assert.assertEquals((int) response.path("items[0].read"), 0);
    }

    @Test(dependsOnMethods = {"user1GetGroupChatMessageById"}, alwaysRun = true)
    public void user1DeleteGroupDialog() {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + dialogId + ".json")
                .then()
                .statusCode(200));
    }

}
