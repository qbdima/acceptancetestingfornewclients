package com.restApiTests.chat.suite;

import com.restApiTests.chat.providers.NewMessageData;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;

public class UnreadMessageTest extends BaseChatTest {


    //PUBLIC DIALOGS

    @Test(dependsOnGroups = {"baseTest"})
    public void user1CreatePublicGroupDialog() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 1)
                .queryParam("name", "public_REST_chat")
                .queryParam("occupants_ids", 11631209)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(dialog_schema))
                .assertThat().body("type", equalTo(1))
                .assertThat().body("name", equalTo("public_REST_chat")));

        ArrayList occupants = response.path("occupants_ids");

        Assert.assertEquals(occupants.size(), 0);

        publicGroupDialogID = response.path("_id");
    }

    @Test(dependsOnMethods = {"user1CreatePublicGroupDialog"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class)
    public void createMessageForPublicGroupDialog(HashMap message) {

        message.put("chat_dialog_id", publicGroupDialogID);
        //   message.put("send_to_chat", 1);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

        groupChatMessageID2 = response.path("_id");
    }

    @Test(dependsOnMethods = {"createMessageForPublicGroupDialog"})
    public void user2GetPublicGroupChatMessageById() {

        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", publicGroupDialogID)
                .queryParam("_id", groupChatMessageID2)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

//        ((ValidatableResponse)response).assertThat().body(matchesJsonSchema(schema));

        Assert.assertEquals(response.path("items[0]._id"), groupChatMessageID2);
        Assert.assertEquals(response.path("items[0].chat_dialog_id"), publicGroupDialogID);
        Assert.assertEquals((int) response.path("items[0].recipient_id"), 0);
        Assert.assertEquals((int) response.path("items[0].read"), 0);

        ArrayList readIds = response.path("items[0].read_ids");
        ArrayList deliveredIds = response.path("items[0].delivered_ids");

        Assert.assertEquals(readIds.size(), 1); //incorrect, should be 0
        Assert.assertEquals(deliveredIds.size(), 1); //incorrect, should be 0

    }

    @Test(dependsOnMethods = {"user2GetPublicGroupChatMessageById"}, alwaysRun = true)
    public void user1DeletePublicGroupDialog() {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + publicGroupDialogID + ".json")
                .then()
                .statusCode(200));
    }


    //GROUP DIALOGS

    @Test(dependsOnGroups = {"baseTest"})
    public void createGroupDialog() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 2)
                .queryParam("name", "unread_message_test")
                .queryParam("occupants_ids", userID2)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(dialog_schema))
                .assertThat().body("type", equalTo(2))
                .assertThat().body("name", equalTo("unread_message_test")));

        dialogId = response.path("_id");
    }

    @Test(dependsOnMethods = {"createGroupDialog"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class)
    public void createMessageForGroupDialog(HashMap message) {
//        String schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(MESSAGE_JSON_SCHEMA));

        message.put("chat_dialog_id", dialogId);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

        groupChatMessageID = response.path("_id");
    }

    @Test(dependsOnMethods = {"createMessageForGroupDialog"})
    public void user2GetGroupChatMessageById() {

        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("_id", groupChatMessageID)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList readIds = response.path("items[0].read_ids");
        ArrayList deliveredIds = response.path("items[0].delivered_ids");

        Assert.assertEquals(response.path("items[0]._id"), groupChatMessageID);
        Assert.assertEquals(response.path("items[0].chat_dialog_id"), dialogId);
        Assert.assertEquals((int) response.path("items[0].recipient_id"), 0);
        Assert.assertEquals((int) response.path("items[0].read"), 0);

        Assert.assertEquals(readIds.size(), 1);
        Assert.assertEquals(deliveredIds.size(), 1);

        Assert.assertTrue(readIds.contains(userID1));
        Assert.assertTrue(deliveredIds.contains(userID1));
    }

    @Test(dependsOnMethods = {"user2GetGroupChatMessageById"})
    public void user1UnreadGroupMessage() {
        this.extractResponseAndPrintLog(spec
                .queryParam("read", 0)
                .queryParam("chat_dialog_id", dialogId)
                .when()
                .put(baseURL + "chat/Message/" + groupChatMessageID + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"user1UnreadGroupMessage"})
    public void user1GetGroupChatMessageById() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("_id", groupChatMessageID)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList readIds = response.path("items[0].read_ids");
        ArrayList deliveredIds = response.path("items[0].delivered_ids");

        Assert.assertEquals(response.path("items[0]._id"), groupChatMessageID);
        Assert.assertEquals(response.path("items[0].chat_dialog_id"), dialogId);
        Assert.assertEquals((int) response.path("items[0].recipient_id"), 0);
        Assert.assertEquals((int) response.path("items[0].read"), 0);
        Assert.assertFalse(readIds.contains(userID1));
        Assert.assertTrue(readIds.contains(userID2));
        Assert.assertTrue(deliveredIds.contains(userID1));
        Assert.assertTrue(deliveredIds.contains(userID2));

    }

    @Test(dependsOnMethods = {"user1GetGroupChatMessageById"}, alwaysRun = true)
    public void user1DeleteGroupDialog() {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + dialogId + ".json")
                .then()
                .statusCode(200));
    }


    //PRIVATE DIALOGS

    @Test(dependsOnGroups = {"baseTest"})
    public void createPrivateDialog() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 3)
                .queryParam("occupants_ids", userID2)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .assertThat().body(matchesJsonSchema(dialog_schema))
                .statusCode(201));

        privateDialogId = response.path("_id");

    }

    @Test(dependsOnMethods = {"createPrivateDialog"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class)
    public void createMessageForPrivateDialog(HashMap message) {

        message.put("chat_dialog_id", privateDialogId);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

        privateChatMessageID = response.path("_id");
    }

    @Test(dependsOnMethods = {"createMessageForPrivateDialog"})
    public void user2GetPrivateChatMessageById() {

        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", privateDialogId)
                .queryParam("_id", privateChatMessageID)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList readIds = response.path("items[0].read_ids");
        ArrayList deliveredIds = response.path("items[0].delivered_ids");

        Assert.assertEquals(response.path("items[0]._id"), privateChatMessageID);
        Assert.assertEquals(response.path("items[0].chat_dialog_id"), privateDialogId);
        Assert.assertEquals((int) response.path("items[0].recipient_id"), userID2);
        Assert.assertEquals((int) response.path("items[0].read"), 1);

        Assert.assertEquals(readIds.size(), 1);
        Assert.assertEquals(deliveredIds.size(), 1);

        Assert.assertTrue(readIds.contains(userID1));
        Assert.assertTrue(deliveredIds.contains(userID1));

    }

    @Test(dependsOnMethods = {"user2GetPrivateChatMessageById"})
    public void user1UnreadPrivateMessage() {
        this.extractResponseAndPrintLog(spec
                .queryParam("read", 0)
                .queryParam("chat_dialog_id", privateDialogId)
                .when()
                .put(baseURL + "chat/Message/" + privateChatMessageID + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"user1UnreadPrivateMessage"})
    public void user1GetPrivateChatMessageById() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", privateDialogId)
                .queryParam("_id", privateChatMessageID)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        ArrayList readIds = response.path("items[0].read_ids");
        ArrayList deliveredIds = response.path("items[0].delivered_ids");

        Assert.assertEquals(response.path("items[0]._id"), privateChatMessageID);
        Assert.assertEquals(response.path("items[0].chat_dialog_id"), privateDialogId);
        Assert.assertEquals((int) response.path("items[0].recipient_id"), userID2);
        Assert.assertEquals((int) response.path("items[0].read"), 1);
        Assert.assertFalse(readIds.contains(userID1));
        Assert.assertTrue(readIds.contains(userID2));
        Assert.assertTrue(deliveredIds.contains(userID1));
        Assert.assertTrue(deliveredIds.contains(userID2));

    }

    @Test(dependsOnMethods = {"user1GetPrivateChatMessageById"}, alwaysRun = true)
    public void user1DeletePrivateDialog() {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + privateDialogId + ".json")
                .then()
                .statusCode(200));
    }
}
