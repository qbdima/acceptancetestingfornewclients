package com.restApiTests.chat.suite;


import com.restApiTests.chat.providers.NewMessageData;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;


public class UnreadMessagesCountSuite extends BaseChatTest {

    @Test(dependsOnGroups = {"baseTest"})
    public void createGroupDialog() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 2)
                .queryParam("name", "unread_message_test")
                .queryParam("occupants_ids", userID2)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(dialog_schema))
                .assertThat().body("type", equalTo(2))
                .assertThat().body("name", equalTo("unread_message_test")));

        dialogId = response.path("_id");
    }

    @Test(dependsOnMethods = {"createGroupDialog"})
    public void createGroupDialog2() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 2)
                .queryParam("name", "unread_message_test2")
                .queryParam("occupants_ids", userID2)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(dialog_schema))
                .assertThat().body("type", equalTo(2))
                .assertThat().body("name", equalTo("unread_message_test2")));

        dialogId2 = response.path("_id");
    }

    @Test(dependsOnMethods = {"createGroupDialog"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class)
    public void createMessageForGroupDialog(HashMap message) {

        message.put("chat_dialog_id", dialogId);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

        groupChatMessageID = response.path("_id");
    }

    @Test(dependsOnMethods = {"createGroupDialog2"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class)
    public void createMessageForGroupDialog2(HashMap message) {

        message.put("chat_dialog_id", dialogId2);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

        groupChatMessageID2 = response.path("_id");
    }

    @Test(dependsOnMethods = {"createMessageForGroupDialog"})
    public void getUnreadMessageCount() {

        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_ids", dialogId + "," + dialogId2)
                .when()
                .get(baseURL + "chat/Message/unread.json")
                .then()
                .statusCode(200));

        Assert.assertEquals((int) response.path("total"), 2);
        Assert.assertEquals((int) response.path(dialogId), 1);
        Assert.assertEquals((int) response.path(dialogId2), 1);
    }

    @Test(dependsOnMethods = {"getUnreadMessageCount"})
    public void getDialogsAndCheckUnreadMessageCount() {

        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(200));

        List<HashMap> items = response.path("items");

        HashMap<String, Object> dialog1 = null;
        HashMap<String, Object> dialog2 = null;

        for (HashMap<String, Object> item : items) {
            if (item.get("_id").equals(dialogId)) {
                dialog1 = item;
                continue;
            }
            if (item.get("_id").equals(dialogId2)) {
                dialog2 = item;
            }
        }

        Assert.assertEquals(dialog1.get("unread_messages_count"), 1);
        Assert.assertEquals(dialog2.get("unread_messages_count"), 1);
    }


    @Test(dependsOnMethods = {"getDialogsAndCheckUnreadMessageCount"})
    public void getMessages() {

        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

    }

    @Test(dependsOnMethods = {"getMessages"})
    public void user2GetUnreadMessageCount() {

        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_ids", dialogId + "," + dialogId2)
                .when()
                .get(baseURL + "chat/Message/unread.json")
                .then()
                .statusCode(200));

        Assert.assertEquals((int) response.path("total"), 1);
        Assert.assertEquals((int) response.path(dialogId), 0);
        Assert.assertEquals((int) response.path(dialogId2), 1);
    }

    @Test(dependsOnMethods = {"user2GetUnreadMessageCount"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class)
    public void user1Create3MessagesForGroupDialog(HashMap message) {

        message.put("chat_dialog_id", dialogId);
        message.put("message", "first_message");

        this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

        message.put("message", "second_message");

        this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

        message.put("message", "third_message");

        this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(message_schema)));

    }

    @Test(dependsOnMethods = {"user1Create3MessagesForGroupDialog"})
    public void user2GetUnreadMessageCountAfterCreating3Messages() {

        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_ids", dialogId + "," + dialogId2)
                .when()
                .get(baseURL + "chat/Message/unread.json")
                .then()
                .statusCode(200));

        Assert.assertEquals((int) response.path("total"), 4);
        Assert.assertEquals((int) response.path(dialogId), 3);
        Assert.assertEquals((int) response.path(dialogId2), 1);
    }

    @Test(dependsOnMethods = {"user2GetUnreadMessageCountAfterCreating3Messages"}, alwaysRun = true)
    public void user2DeleteDialog() {

        this.initSpec("token2");

        this.extractResponseAndPrintLog(spec
                .when()
                .delete(baseURL + "chat/Dialog/" + dialogId + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"user2DeleteDialog"})
    public void addUser2ToDialog() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("push_all[occupants_ids][]", userID2)
                .when()
                .put(baseURL + "chat/Dialog/" + dialogId + ".json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(dialog_schema)));

    }

    @Test(dependsOnMethods = {"addUser2ToDialog"})
    public void user2GetUnreadMessageCountAfterReturnToDialog() {

        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_ids", dialogId + "," + dialogId2)
                .when()
                .get(baseURL + "chat/Message/unread.json")
                .then()
                .statusCode(200));

        Assert.assertEquals((int) response.path("total"), 1);
        Assert.assertEquals((int) response.path(dialogId), 0);
        Assert.assertEquals((int) response.path(dialogId2), 1);
    }

    @Test(dependsOnMethods = {"getMessages"})
    public void getGroupChatMessageById() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("chat_dialog_id", dialogId)
                .queryParam("_id", groupChatMessageID)
                .when()
                .get(baseURL + "chat/Message.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(messages_schema)));

        Assert.assertEquals(response.path("items[0]._id"), groupChatMessageID);
        Assert.assertEquals(response.path("items[0].chat_dialog_id"), dialogId);
        Assert.assertEquals((int) response.path("items[0].recipient_id"), 0);
        Assert.assertEquals((int) response.path("items[0].read"), 0);
    }

    @Test(dependsOnMethods = {"user2GetUnreadMessageCountAfterReturnToDialog"}, alwaysRun = true)
    public void deleteGroupDialog() {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + dialogId + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"user2GetUnreadMessageCountAfterReturnToDialog"}, alwaysRun = true)
    public void deleteGroupDialog2() {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + dialogId2 + ".json")
                .then()
                .statusCode(200));
    }

}
