package com.restApiTests.chat.suite.xmpp;


import com.restApiTests.chat.providers.NewMessageData;
import com.restApiTests.chat.suite.BaseChatTest;
import com.restApiTests.chat.utils.XMPPChatUtils;
import io.restassured.response.Response;
import org.codehaus.jettison.json.JSONException;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.DefaultPacketExtension;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.RoomInfo;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.HashMap;

import com.restApiTests.chat.utils.XMPPChatUtils.BooleanCondition;
import static org.hamcrest.Matchers.equalTo;

public class XMPPChatSuite extends BaseChatTest {

    private static final String password = "11111111";
    private static final int requestTimeout = 30000;

    private static final String groupChatMessage = "group chat message" + "\ue415";
    private static final String privateChatMessage = "private chat message" + "\uD83D\uDE01\uD83D\uDE1A\uD83D\uDE17\uD83D\uDE14";

    private static XMPPTCPConnection connection;
    private static XMPPTCPConnection connection2;

    private static BooleanCondition condition = new BooleanCondition();
    private static BooleanCondition condition2 = new BooleanCondition();

    private int count = 0;

    private Message currentPrivateMessage;
    private Message currentGroupMessage;

    private MultiUserChat groupChat;

    @Test(groups = {"ChatTest"}, dependsOnGroups = {"baseTest"}, timeOut = requestTimeout, alwaysRun = true)
    public void createDialog() {
        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 2)
                .queryParam("name", "test_XMPP_chat")
                .queryParam("occupants_ids", userID2)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
                .assertThat().body("type", equalTo(2))
                .assertThat().body("name", equalTo("test_XMPP_chat")));

        groupDialogId = response.path("_id");
    }

//    @Test(groups = {"ChatTest"}, dependsOnGroups = {"baseTest"}, timeOut = requestTimeout, alwaysRun = true)
//    public void createPublicDialog() {
//        Response response = this.extractResponseAndPrintLog(spec
//                .queryParam("type", 1)
//                .queryParam("name", "test_XMPP_public_chat")
//                .when()
//                .post(baseURL + "chat/Dialog.json")
//                .then()
//                .statusCode(201)
//                .assertThat().body("type", equalTo(1))
//                .assertThat().body("name", equalTo("test_XMPP_public_chat")));
//
//        publicGroupDialogID = response.path("_id");
//    }

    @Test(groups = {"ChatTest"}, dependsOnMethods = {"createDialog"}, timeOut = requestTimeout)
    public void loginUsers() throws XMPPException, IOException, SmackException {

        connection = XMPPChatUtils.createConnection(chatURL);
        connection2 = XMPPChatUtils.createConnection(chatURL);


        PacketListener myListener = new PacketListener() {
            public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                if (packet instanceof Message) {
                    Message message = (Message) packet;

                    // обработка входящего сообщения
                    //  processMessage(message);
//                    logger.info("User1 received message: " + message.getBody() + "\n");
                    System.out.println("User1 received message: " + message.getBody());
                }
            }
        };

        PacketListener myListener2 = new PacketListener() {
            public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                if (packet instanceof Message) {
                    Message message = (Message) packet;

                    if (message.getType() == Message.Type.chat) {
                        condition.isReceived = true;
                        // logger.info("User2 received private message: " + message.getBody() + "\n");
                        System.out.println("User2 received private message: " + message.getBody());
                        currentPrivateMessage = message;
                        if (message.getBody().equals(privateChatMessage)) {
                            condition.isEqual = true;
                        }
                    }
                    if (message.getType() == Message.Type.groupchat) {
                        currentGroupMessage = message;
                        condition2.isReceived = true;
                        //logger.info("User2 received group message: " + message.getBody() + "\n");
                        System.out.println("User2 received group message: " + message.getBody());
                    }
                }
            }
        };

        String user1Login = userID1 + "-" + appID;
        XMPPChatUtils.loginToChat(connection, user1Login, password, myListener);

        String user2Login = userID2 + "-" + appID;
        XMPPChatUtils.loginToChat(connection2, user2Login, password, myListener2);

        Assert.assertTrue(connection.isConnected(), "User1 cannot login to chat");
        Assert.assertTrue(connection2.isConnected(), "User2 cannot login to chat");

        Assert.assertTrue(connection.isAuthenticated(), "User1 cannot login to chat");
        Assert.assertTrue(connection2.isAuthenticated(), "User2 cannot login to chat");


    }

    @Test(groups = {"ChatTest"}, dependsOnMethods = {"loginUsers"}, timeOut = requestTimeout)
    public void sendPrivateMessageTest() throws SmackException.NotConnectedException {
        XMPPChatUtils.sendPrivateMessage(connection, XMPPChatUtils.createUserJID(userID2, appID, chatURL), privateChatMessage, Message.Type.chat, true);
    }

    @Test(groups = {"ChatTest"}, timeOut = requestTimeout, dependsOnMethods = {"sendPrivateMessageTest"})
    public void receivePrivateMessageTest() throws SmackException.NotConnectedException, InterruptedException {
        while (!condition.isReceived & count < 15) {
            Thread.sleep(1000);
            count++;
        }
        count = 0;
        Assert.assertTrue(condition.isReceived, "Private message not received");
        Assert.assertTrue(condition.isEqual);
    }

    @Test(groups = {"ChatTest"}, dependsOnMethods = "receivePrivateMessageTest", timeOut = requestTimeout)
    public void user2Logout() throws SmackException.NotConnectedException {
        connection2.disconnect(new Presence(Presence.Type.unavailable));
        Assert.assertTrue(!connection2.isConnected());
    }

    @Test(groups = {"ChatTest"}, dependsOnMethods = "user2Logout", timeOut = requestTimeout, alwaysRun = true)
    public void sendMessageToOfflineUser() throws SmackException.NotConnectedException {
        condition.isReceived = false;
        condition.isEqual = false;
        XMPPChatUtils.sendPrivateMessage(connection, XMPPChatUtils.createUserJID(userID2, appID, chatURL), privateChatMessage, Message.Type.chat, true);
    }

    @Test(groups = {"ChatTest"}, dependsOnMethods = "sendMessageToOfflineUser", timeOut = requestTimeout)
    public void receiveOfflineMessage() throws SmackException, InterruptedException, IOException, XMPPException {
        connection2.connect();              //логинится не надо
        while (!condition.isReceived && count < 15) {
            Thread.sleep(1000);
            count++;
        }
        count = 0;
        if (!condition.isReceived) {
//            logger.error("Offline chat message not received");
            System.out.println("Offline chat message not received");
        }

        Assert.assertTrue(condition.isReceived, "Offline private message not received");
        Assert.assertTrue(condition.isEqual);
    }


    @Test(groups = {"ChatTest"}, dependsOnMethods = "receiveOfflineMessage", timeOut = requestTimeout, alwaysRun = true)
    public void joinRoom() throws SmackException.NotConnectedException, XMPPException.XMPPErrorException, SmackException.NoResponseException {
        String roomJID = XMPPChatUtils.createRoomJID(groupDialogId, appID, chatURL);
        RoomInfo info = MultiUserChat.getRoomInfo(connection, roomJID);
        Assert.assertTrue(info.isPersistent());

//        MultiUserChat groupChat = XMPPChatUtils.joinRoom(connection, roomJID, XMPPChatUtils.createUserJID(userID1, appID, chatURL));
        groupChat = XMPPChatUtils.joinRoom(connection, roomJID, XMPPChatUtils.createUserJID(userID1, appID, chatURL));
        Assert.assertTrue(groupChat.isJoined(), "User1 cannot join to room");

        MultiUserChat groupChat2 = XMPPChatUtils.joinRoom(connection2, roomJID, XMPPChatUtils.createUserJID(userID2, appID, chatURL));
        Assert.assertTrue(groupChat2.isJoined(), "User2 cannot join to room");
    }


    @Test(groups = {"ChatTest"}, dependsOnMethods = "joinRoom")
    public void sendGroupChatMessage() throws SmackException.NotConnectedException, InterruptedException {
        condition2.isReceived = false;
        XMPPChatUtils.sendMessageGroup(connection, XMPPChatUtils.createRoomJID(groupDialogId, appID, chatURL), groupChatMessage, Message.Type.groupchat);

        while (!condition2.isReceived & count < 15) {
            Thread.sleep(1000);
            count++;
        }
        count = 0;
        Assert.assertTrue(condition2.isReceived, "Group chat message not received");

        if (!condition2.isReceived) {
//            logger.error("Group chat message not received");
            System.out.println("Group chat message not received");
        }

        condition2.isReceived = false;
    }

//    @Test(groups = {"ChatTest"}, dependsOnMethods = "sendGroupChatMessage", timeOut = requestTimeout, alwaysRun = true)
//    public void checkMessages() {
//        DefaultPacketExtension packetExtensionForPrivateMessage = currentPrivateMessage.getExtension("extraParams", "jabber:client");
//        packetExtensionForPrivateMessage.getValue("date_sent");
//
//        DefaultPacketExtension packetExtensionForGroupMessage = currentGroupMessage.getExtension("extraParams", "jabber:client");
//        packetExtensionForGroupMessage.getValue("date_sent");
//
//        Assert.assertEquals(packetExtensionForPrivateMessage.getValue("save_to_history"), "1");
//        Assert.assertEquals(packetExtensionForGroupMessage.getValue("save_to_history"), "1");
//
//        Assert.assertEquals(packetExtensionForGroupMessage.getValue("dialog_id"), groupDialogId);
//
//        Assert.assertNotNull(packetExtensionForGroupMessage.getValue("message_id"));
//        Assert.assertNotNull(packetExtensionForGroupMessage.getValue("date_sent"));
//
//        Assert.assertNotNull(packetExtensionForPrivateMessage.getValue("message_id"));
//        Assert.assertNotNull(packetExtensionForPrivateMessage.getValue("date_sent"));
//
//        Assert.assertEquals(currentPrivateMessage.getFrom(), XMPPChatUtils.createUserJID(userID1, appID, chatURL) + "/Smack");
//        Assert.assertEquals(currentGroupMessage.getFrom(), XMPPChatUtils.createRoomJID(groupDialogId, appID, chatURL) + "/" + XMPPChatUtils.createUserJID(userID1, appID, chatURL));
//
//        Assert.assertEquals(currentPrivateMessage.getTo(), XMPPChatUtils.createUserJID(userID2, appID, chatURL));
//        Assert.assertEquals(currentGroupMessage.getTo(), XMPPChatUtils.createUserJID(userID2, appID, chatURL) + "/Smack");
//
//    }
//
//    @Test(groups = {"ChatTest"}, dependsOnMethods = {"checkMessages"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class, alwaysRun = true)
//    public void User1CreateMessageUsingRestAPI(HashMap message) throws JSONException, InterruptedException {
//
//        message.put("chat_dialog_id", groupDialogId);
//        message.put("send_to_chat", 1);
//
//        Response response = this.extractResponseAndPrintLog(spec
//                .queryParams(message)
//                .when()
//                .post(baseURL + "chat/Message.json")
//                .then()
//                .statusCode(201));
//
//        while (!condition2.isReceived & count < 15) {
//            Thread.sleep(1000);
//            count++;
//        }
//        count = 0;
//        Assert.assertTrue(condition2.isReceived, "Message from REST API not received");
//
////        if (!condition2.isReceived) {
////            logger.error("Message from REST API not received");
////        }
//
//    }

    @Test(groups = {"ChatTest"}, dependsOnMethods = "User1CreateMessageUsingRestAPI", timeOut = requestTimeout, alwaysRun = true)
    public void checkMessages() {
        DefaultPacketExtension packetExtensionForPrivateMessage = currentPrivateMessage.getExtension("extraParams", "jabber:client");
        packetExtensionForPrivateMessage.getValue("date_sent");

        DefaultPacketExtension packetExtensionForGroupMessage = currentGroupMessage.getExtension("extraParams", "jabber:client");
        packetExtensionForGroupMessage.getValue("date_sent");

        Assert.assertEquals(packetExtensionForPrivateMessage.getValue("save_to_history"), "1");
        Assert.assertEquals(packetExtensionForGroupMessage.getValue("save_to_history"), "1");

        Assert.assertEquals(packetExtensionForGroupMessage.getValue("dialog_id"), groupDialogId);
        privateDialogId = packetExtensionForPrivateMessage.getValue("dialog_id");
        Assert.assertNotNull(privateDialogId);

        Assert.assertNotNull(packetExtensionForGroupMessage.getValue("message_id"));
        Assert.assertNotNull(packetExtensionForGroupMessage.getValue("date_sent"));

        Assert.assertNotNull(packetExtensionForPrivateMessage.getValue("message_id"));
        Assert.assertNotNull(packetExtensionForPrivateMessage.getValue("date_sent"));

        Assert.assertEquals(currentPrivateMessage.getFrom(), XMPPChatUtils.createUserJID(userID1, appID, chatURL) + "/Smack");
      //  Assert.assertEquals(currentGroupMessage.getFrom(), XMPPChatUtils.createRoomJID(groupDialogId, appID, chatURL) + "/" + XMPPChatUtils.createUserJID(userID1, appID, chatURL));

        Assert.assertEquals(currentPrivateMessage.getTo(), XMPPChatUtils.createUserJID(userID2, appID, chatURL));
    //    Assert.assertEquals(currentGroupMessage.getTo(), XMPPChatUtils.createUserJID(userID2, appID, chatURL) + "/Smack");

    }

    @Test(groups = {"ChatTest"}, dependsOnMethods = {"checkMessages"}, timeOut = requestTimeout)
    public void sendPrivateMessageWithoutSaveToHistoryTest() throws SmackException.NotConnectedException, InterruptedException {
        condition.isReceived = false;
        XMPPChatUtils.sendPrivateMessage(connection, XMPPChatUtils.createUserJID(userID2, appID, chatURL), privateChatMessage, Message.Type.chat, false);
        while (!condition.isReceived & count < 15) {
            Thread.sleep(1000);
            count++;
        }
        count = 0;
        Assert.assertTrue(condition.isReceived, "Private message not received");

        condition.isReceived = false;
    }

//    @Test(groups = {"ChatTest"}, dependsOnMethods = "sendPrivateMessageWithoutSaveToHistoryTest", timeOut = requestTimeout, alwaysRun = true)
//    public void checkMessages2() {
//
//        SoftAssert softAssert = new SoftAssert();
//
//        DefaultPacketExtension packetExtensionForPrivateMessage = currentPrivateMessage.getExtension("extraParams", "jabber:client");
//        packetExtensionForPrivateMessage.getValue("date_sent");
//
////        DefaultPacketExtension packetExtensionForGroupMessage = currentGroupMessage.getExtension("extraParams", "jabber:client");
////        packetExtensionForGroupMessage.getValue("date_sent");
//
//        softAssert.assertEquals(packetExtensionForPrivateMessage.getValue("save_to_history"), "1", "Save to history flag not found \n");
////        Assert.assertEquals(packetExtensionForGroupMessage.getValue("save_to_history"), "1");
//
////        Assert.assertEquals(packetExtensionForGroupMessage.getValue("dialog_id"), groupDialogId);
//        privateDialogId = packetExtensionForPrivateMessage.getValue("dialog_id");
//        softAssert.assertNotNull(privateDialogId, "privateDialogId is null \n");
//
////        Assert.assertNotNull(packetExtensionForGroupMessage.getValue("message_id"));
////        Assert.assertNotNull(packetExtensionForGroupMessage.getValue("date_sent"));
//
//        softAssert.assertNotNull(packetExtensionForPrivateMessage.getValue("message_id"), "messageID is null \n");
//        softAssert.assertNotNull(packetExtensionForPrivateMessage.getValue("date_sent"), "date sent is null \n");
//
//        softAssert.assertEquals(currentPrivateMessage.getFrom(), XMPPChatUtils.createUserJID(userID1, appID, chatURL) + "/Smack");
//        //  Assert.assertEquals(currentGroupMessage.getFrom(), XMPPChatUtils.createRoomJID(groupDialogId, appID, chatURL) + "/" + XMPPChatUtils.createUserJID(userID1, appID, chatURL));
//
//        softAssert.assertEquals(currentPrivateMessage.getTo(), XMPPChatUtils.createUserJID(userID2, appID, chatURL));
//        softAssert.assertEquals(currentGroupMessage.getTo(), XMPPChatUtils.createUserJID(userID2, appID, chatURL) + "/Smack");
//        softAssert.assertAll();
//
//    }

    ////Добавить тест для отправки сообщения в паблик чат


    @Test(groups = {"ChatTest"}, dependsOnMethods = {"sendGroupChatMessage"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class, alwaysRun = true)
    public void User1CreateMessageUsingRestAPI(HashMap message) throws JSONException, InterruptedException {

        message.put("chat_dialog_id", groupDialogId);
        message.put("send_to_chat", 1);

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201));

        while (!condition2.isReceived & count < 15) {
            Thread.sleep(1000);
            count++;
        }
        count = 0;
        Assert.assertTrue(condition2.isReceived, "Message from REST API not received");
        condition2.isReceived = false;

//        if (!condition2.isReceived) {
//            logger.error("Message from REST API not received");
//        }

    }


    // тест не закончен
//    @Test(groups = {"ChatTest"}, dependsOnMethods = "User1CreateMessageUsingRestAPI", timeOut = requestTimeout, dataProvider = "newMessageForGroupDialog",
//            dataProviderClass = NewMessageData.class, alwaysRun = true, enabled = true)
//    public void User1CreateMessageUsingRestAPIForPublicGroup(HashMap message) throws SmackException.NotConnectedException, XMPPException.XMPPErrorException, SmackException.NoResponseException, InterruptedException {
//        groupChat.leave();
//
//        String roomJID = XMPPChatUtils.createRoomJID(publicGroupDialogID, appID, chatURL);
////        RoomInfo info = MultiUserChat.getRoomInfo(connection, roomJID);
////        Assert.assertTrue(info.isPersistent());
//
//        MultiUserChat multiUserChat = XMPPChatUtils.joinRoom(connection, roomJID, XMPPChatUtils.createUserJID(userID1, appID, chatURL));
//
//        Assert.assertTrue(multiUserChat.isJoined(), "User1 cannot join to room");
//
//        message.put("chat_dialog_id", publicGroupDialogID);
//        message.put("send_to_chat", 1);
//
//        Response response = this.extractResponseAndPrintLog(spec
//                .queryParams(message)
//                .when()
//                .post(baseURL + "chat/Message.json")
//                .then()
//                .statusCode(201));
//
//        while (!condition.isReceived & count < 15) {
//            Thread.sleep(1000);
//            count++;
//        }
//        count = 0;
//        Assert.assertTrue(condition2.isReceived, "Message from REST API not received");
//    }

    @Test(groups = {"ChatTest"}, dependsOnMethods = "checkMessages", timeOut = requestTimeout, alwaysRun = true)
//    @Test(groups = {"ChatTest"}, dependsOnMethods = "User1CreateMessageUsingRestAPI", timeOut = requestTimeout, alwaysRun = true)
    public void user1Logout() throws SmackException.NotConnectedException {
        connection.disconnect(new Presence(Presence.Type.unavailable));
        Assert.assertTrue(!connection.isConnected());
    }

    @Test(groups = {"ChatTest"}, dependsOnMethods = "user1Logout", timeOut = requestTimeout)
    public void user1LoginToChatUsingToken() throws SmackException.NotConnectedException {
        connection = XMPPChatUtils.createConnection(chatURL);
        String user1Login = userID1 + "-" + appID;

        PacketListener myListener = new PacketListener() {
            public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                if (packet instanceof Message) {
                    Message message = (Message) packet;
//                    logger.info("User1 received message: " + message.getBody() + "\n");
                    System.out.println("User1 received message: " + message.getBody());
                }
            }
        };

        try {
            XMPPChatUtils.loginToChat(connection, user1Login, (String) baseParams.get("token"), myListener);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XMPPException e) {
            e.printStackTrace();
        } catch (SmackException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(connection.isAuthenticated(), "User can not login to chat using token");
    }

    @Test(dependsOnMethods = {"user1LoginToChatUsingToken"}, alwaysRun = true)
    public void user1DeleteGroupDialog() throws IOException {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + groupDialogId + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"user1LoginToChatUsingToken"}, alwaysRun = true)
    public void user1DeletePrivateDialog() throws IOException {

        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + privateDialogId + ".json")
                .then()
                .statusCode(200));
    }


}
