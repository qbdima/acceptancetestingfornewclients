package com.restApiTests.chat.utils;

import com.utils.MongoDBObjectId;
import org.jivesoftware.smack.packet.Message;

public class QBChatMessage extends Message{

    private String messageId;

    public QBChatMessage(String text, String to, Type type) {
        super(to);
        this.messageId = MongoDBObjectId.get().toString();
        super.setPacketID(messageId);
        super.setBody(text);
        super.setType(type);
    }

    public String getMessageId() {
        return messageId;
    }

}
