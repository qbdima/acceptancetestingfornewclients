package com.restApiTests.chat.utils;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.DefaultPacketExtension;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jivesoftware.smackx.chatstates.ChatState;
import org.jivesoftware.smackx.chatstates.packet.ChatStateExtension;
import org.jivesoftware.smackx.muc.DiscussionHistory;
import org.jivesoftware.smackx.muc.MultiUserChat;

import java.io.IOException;
import java.util.Optional;

public class XMPPChatUtils {

    private final static String NAMESPACE_FOR_MARKERS = "urn:xmpp:chat-markers:0";
    private final static String ELEMENT_NAME_DISPLAYED = "displayed";
    private final static String ELEMENT_NAME_RECEIVED = "received";
    private final static String ELEMENT_NAME_TYPING = "composing";
    private final static String ELEMENT_NAME_STOP_TYPING = "paused";
    private final static String ELEMENT_NAME = "extraParams";
    private final static String NAMESPACE = "jabber:client";
    private final static String TYPING_NAMESPACE = "http://jabber.org/protocol/chatstates";
    private final static PacketFilter filter = new AndFilter(new PacketTypeFilter(Message.class));

    private final static int port = 5222;

    public static XMPPTCPConnection createConnection(String chat){

        ConnectionConfiguration configuration = new ConnectionConfiguration(chat, port);
        configuration.setReconnectionAllowed(false);
        configuration.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        XMPPTCPConnection connection = new XMPPTCPConnection(configuration);
        return connection;
    }

    public static void loginToChat(XMPPTCPConnection connection, String login, String password, PacketListener myListener)
            throws IOException, XMPPException, SmackException {

        connection.addPacketListener(myListener, filter);
        connection.connect();
        connection.login(login, password);
        connection.sendPacket(new Presence(Presence.Type.available));
    }

    public static MultiUserChat joinRoom(XMPPTCPConnection connection, String roomJID, String login)
            throws SmackException.NotConnectedException, XMPPException.XMPPErrorException, SmackException.NoResponseException {
        MultiUserChat chat = new MultiUserChat(connection, roomJID);
        DiscussionHistory history = new DiscussionHistory();
        history.setMaxStanzas(0);
        chat.join(login, "12344", history, connection.getPacketReplyTimeout());
        return chat;
    }

    public static String sendPrivateMessage(XMPPTCPConnection con, String to, String textMessage, Message.Type type, boolean isSaveToHistory) throws SmackException.NotConnectedException {

//        String messageID = MongoDBObjectId.get().toString();
//
//        Message message = new Message(textMessage);
//        message.setTo(to);
//        message.setPacketID(messageID);
//        message.setType(Message.Type.chat);
//        message.setBody(textMessage);

        QBChatMessage message = new QBChatMessage(textMessage, to, type);

        QBChatMessageExtension ex = new QBChatMessageExtension();
        ex.setSaveToHistory(isSaveToHistory);

        message.addExtension(ex);

        con.sendPacket(message);

        return message.getMessageId();

//        QBChatMarkersExtension extension = new QBChatMarkersExtension();
//        extension.setMessageID(messageID);
//        extension.setDisplayed(true);
//
//        Message message2 = new Message(textMessage);
//        message2.setTo(to);
//        message2.setType(Message.Type.chat);
//
//        message2.addExtension(extension);
//
//        con.sendPacket(message2);
//
//        QBChatMarkersExtension extension2 = new QBChatMarkersExtension();
//        extension2.setMessageID(messageID);
//        extension2.setDisplayed(false);
//
//        Message message3 = new Message(textMessage);
//        message3.setTo(to);
//        message3.setType(Message.Type.chat);
//
//        message3.addExtension(extension2);
//
//        con.sendPacket(message3);
//
//        Message message4 = new Message(textMessage);
//        message4.setTo(to);
//        message4.setPacketID(messageID);
//        message4.setType(Message.Type.headline);
//
//        QBChatMessageExtension ex2 = new QBChatMessageExtension();
//        ex2.setAccept(true);
//        ex2.setSaveToHistory(false);
//
//        message4.addExtension(ex2);
//
//        con.sendPacket(message4);
//
//        Message message5 = new Message(textMessage);
//        message5.setTo(to);
//        message5.setPacketID(messageID);
//        message5.setType(Message.Type.chat);
//
//        ChatStateExtension extension3 = new ChatStateExtension(ChatState.composing);
//
//        message5.addExtension(extension3);
//
//        con.sendPacket(message5);
//
//        Message message6 = new Message(textMessage);
//        message6.setTo(to);
//        message6.setPacketID(messageID);
//        message6.setType(Message.Type.chat);
//
//        ChatStateExtension extension4 = new ChatStateExtension(ChatState.paused);
//
//        message6.addExtension(extension4);
//
//        con.sendPacket(message6);

    }

    public static String sendMessageGroup(XMPPTCPConnection con, String to, String textMessage, Message.Type type) throws SmackException.NotConnectedException {
        if(!textMessage.equals(""))
        {
            QBChatMessage message = new QBChatMessage(textMessage, to, type);

//            Message message = new Message();
//            message.setTo(to);
//            message.setType(Message.Type.groupchat);
//            message.setBody(textMessage);

            QBChatMessageExtension ex = new QBChatMessageExtension();

            message.addExtension(ex);

            con.sendPacket(message);

            return message.getMessageId();
        }

        return null;
    }

    public static void sendTypingStatusMessage(XMPPTCPConnection con, String to, ChatState chatState) throws SmackException.NotConnectedException {

        QBChatMessage message = new QBChatMessage(null, to, Message.Type.chat);

 //       String messageID = MongoDBObjectId.get().toString();

//        Message message = new Message();
//        message.setTo(to);
//        message.setPacketID(messageID);
//        message.setType(Message.Type.chat);

        ChatStateExtension extension = new ChatStateExtension(chatState);

        message.addExtension(extension);

        con.sendPacket(message);
    }

    public static void sendStatusMessage(XMPPTCPConnection con, String to, String messageId, boolean isDisplayed) throws SmackException.NotConnectedException {

        QBChatMessage message = new QBChatMessage(null, to, Message.Type.chat);

//        String packetId = MongoDBObjectId.get().toString();
//
//        Message message = new Message();
//        message.setTo(to);
//        message.setType(Message.Type.chat);

        QBChatMarkersExtension extension = new QBChatMarkersExtension();
        extension.setMessageID(messageId);
        extension.setDisplayed(isDisplayed);

        // добавить передачу id сообщения для прочтения
        message.addExtension(extension);

        con.sendPacket(message);
    }

    public static void sendAcceptMessage(XMPPTCPConnection con, String to) throws SmackException.NotConnectedException {
//        String messageID = MongoDBObjectId.get().toString();
//
//        Message message = new Message();
//        message.setTo(to);
//        message.setPacketID(messageID);
//        message.setType(Message.Type.headline);

        QBChatMessage message = new QBChatMessage(null, to, Message.Type.headline);

        QBChatMessageExtension ex = new QBChatMessageExtension();
        ex.setAccept(true);
        ex.setSaveToHistory(false);

        message.addExtension(ex);

        con.sendPacket(message);
    }

    public static class BooleanCondition {

        public boolean isReceived = false;
        public boolean isEqual = false;

    }

    public static class QBChatMessageExtension implements PacketExtension {

        private boolean isSaveToHistory = true;
        private boolean isAccept = false;

        public boolean isAccept() {
            return isAccept;
        }

        public void setAccept(boolean accept) {
            isAccept = accept;
        }

        public boolean isSaveToHistory() {
            return isSaveToHistory;
        }

        public void setSaveToHistory(boolean saveToHistory) {
            isSaveToHistory = saveToHistory;
        }

        @Override
        public String getElementName() {
            return null;
        }

        @Override
        public String getNamespace() {
            return null;
        }

        @Override
        public CharSequence toXML() {
            XmlStringBuilder buf = new XmlStringBuilder();
            buf.halfOpenElement(ELEMENT_NAME).xmlnsAttribute(NAMESPACE).rightAngelBracket();
            if (isSaveToHistory) {
                buf.element("save_to_history", "1");
            }
            if (isAccept){
                buf.element("signalType", "accept");
            }
            buf.closeElement(ELEMENT_NAME);
            return buf;
        }
    }

    public static class QBChatMarkersExtension implements PacketExtension {

        private String messageID;
        private boolean isDisplayed;

        public boolean isDisplayed() {
            return isDisplayed;
        }

        public void setDisplayed(boolean displayed) {
            isDisplayed = displayed;
        }

        public String getMessageID() {
            return messageID;
        }

        public void setMessageID(String messageID) {
            this.messageID = messageID;
        }

        @Override
        public String getElementName() {
            if(isDisplayed){
                return ELEMENT_NAME_DISPLAYED;
            }else {
                return ELEMENT_NAME_RECEIVED;
            }

        }

        @Override
        public String getNamespace() {
            return null;
        }

        @Override
        public CharSequence toXML() {
            XmlStringBuilder buf = new XmlStringBuilder();
            buf.halfOpenElement(getElementName()).xmlnsAttribute(NAMESPACE_FOR_MARKERS);
            if(messageID != null){
                buf.attribute("id", messageID);
            }
//            buf.closeElement(ELEMENT_NAME_DISPLAYED);
            buf.closeEmptyElement();
            return buf;
        }
    }

    public static String createUserJID(int userID, String appID, String chatDomain){
        final StringBuffer userJID = new StringBuffer(20);

        class UserJID {
            public Optional<String> appID;
            public Optional<String> chatDomain;
        }

        UserJID jid = new UserJID();
        jid.appID = Optional.of(appID);
        jid.chatDomain = Optional.of(chatDomain);

        jid.appID.ifPresent(appIDStr -> {
            jid.chatDomain.ifPresent(chatDomainStr -> {
                userJID.append(userID)
                        .append("-")
                        .append(appIDStr)
                        .append("@")
                        .append(chatDomainStr);
            });
        });

        return userJID.toString();
    }

    public static String createRoomJID(String dialogID, String appID, String chatDomain){

        final StringBuffer groupChatRoom = new StringBuffer(20);

        class RoomJID {
            public Optional<String> appID;
            public Optional<String> chatDomain;
        }

        RoomJID jid = new RoomJID();
        jid.appID = Optional.of(appID);
        jid.chatDomain = Optional.of(chatDomain);

        jid.appID.ifPresent(appIDStr -> {
            jid.chatDomain.ifPresent(chatDomainStr -> {
                groupChatRoom.append(appIDStr)
                        .append("_")
                        .append(dialogID)
                        .append("@muc.")
                        .append(chatDomainStr);
            });
        });

        return groupChatRoom.toString();
    }

    public static String getExtraParamsByName(Message message, String key){
        DefaultPacketExtension packetExtensionForPrivateMessage = message.getExtension("extraParams", "jabber:client");
        return packetExtensionForPrivateMessage.getValue(key);
    }

}
