package com.restApiTests.content;


import com.restApiTests.core.BaseTest;
import com.utils.UtilsMethods;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.io.IOUtils;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;


public class ContentSuite extends BaseTest {

    private static final String imagePath = "./src/test/resources/dog.jpeg";

    private String bucketURL;
    private int publicBlobID;
    private int privateBlobID;
    private String publicBlobUid;
    private String privateBlobUid;

    static HashMap<String, String> arr;

    private String blob_schema;
    private String blobs_schema;

    @BeforeTest
    public void setUp() {
        try {
            blob_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(BLOB_JSON_SCHEMA), "UTF-8");
            blobs_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(BLOBS_JSON_SCHEMA), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnGroups = {"baseTest"})
    public void user1CreatePublicBlobObject() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("blob[content_type]", "image/jpeg")
                // .queryParam("blob[name]", "ReadStatusSuite")
                .queryParam("blob[name]", "dog.jpeg")
                .queryParam("blob[public]", true)
                .when()
                .post(baseURL + "blobs.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(blob_schema)));

        arr = UtilsMethods.parseBlobObject(response.path("blob.blob_object_access.params"));
        bucketURL = UtilsMethods.getBucketUrl(response.path("blob.blob_object_access.params"));
        publicBlobID = response.path("blob.id");
        publicBlobUid = response.path("blob.uid");
        boolean isPublic = response.path("blob.public");

        Assert.assertEquals(isPublic, true);
    }

    @Test(dependsOnMethods = {"user1CreatePublicBlobObject"})
    public void uploadFile() {

        RequestSpecification reqSpec = given().
                contentType("multipart/form-data");

        arr.forEach((key, value) -> {
            try {
                reqSpec.multiPart(key, URLDecoder.decode(value, "UTF-8"), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        });
        reqSpec.multiPart("file", new File(imagePath));

        reqSpec.when()
                .post(bucketURL)
                .then()
                .statusCode(201)
                .log().all();

    }

    @Test(dependsOnMethods = {"uploadFile"})
    public void declaringFileUploaded() {

        this.extractResponseAndPrintLog(spec
                .queryParam("blob[size]", new File(imagePath).length())
                .when()
                .post(baseURL + "blobs/" + publicBlobID + "/complete.json")
                .then()
                .statusCode(200));

    }

    @Test(dependsOnMethods = {"declaringFileUploaded"})
    public void getInformationAboutFileById() {

        Response response = this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "blobs/" + publicBlobID + ".json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(blob_schema)));

    }

    @Test(dependsOnMethods = {"declaringFileUploaded"})
    public void downloadFileById() {


        this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "blobs/" + publicBlobID + "/download.json")
                .then()
                .statusCode(200));

    }

    @Test(dependsOnMethods = {"declaringFileUploaded"})
    public void downloadFileByUid() {

        this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "blobs/" + publicBlobUid + ".json")
                .then()
                .statusCode(200));

    }

    @Test(dependsOnMethods = {"declaringFileUploaded"})
    public void downloadPublicFileByIdWithoutToken() {

        given()
                .when()
                .get(baseURL + "blobs/" + publicBlobID + "/download.json")
                .then()
                .statusCode(200)
                .log().all();

    }

    @Test(dependsOnMethods = {"declaringFileUploaded"})
    public void downloadPublicFileByUidWithoutToken() {

        given()
                .when()
                .get(baseURL + "blobs/" + publicBlobUid + ".json")
                .then()
                .statusCode(200)
                .log().all();

    }

//    @Test(dependsOnMethods = {"getInformationAboutFileById"})
//    public void editFileById() throws IOException {
//
//        String blob_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("blob-blob_schema.json"));
//
//        Response response = this.extractResponseAndPrintLog(spec
//                .queryParam("blob[name]", "updated_blob")
//                .when()
//                .put(baseURL + "blobs/" + publicBlobID + ".json")
//                .then()
//                .statusCode(200)
//                .assertThat().body(matchesJsonSchema(blob_schema)));
//
//        String blobName = response.path("blob.name");
//        Assert.assertEquals(blobName, "updated_blob");
//    }

    // @Test(dependsOnMethods = {"editFileById"})
    @Test(dependsOnMethods = {"getInformationAboutFileById"})
    public void getFilesList() {

        Response response = this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "blobs.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(blobs_schema)));

        ArrayList items = response.path("items");
        Assert.assertEquals(items.size(), 1);
        Assert.assertEquals((int) response.path("total_entries"), 1);
    }

    @Test(dependsOnMethods = {"getInformationAboutFileById"})
    public void editFileByIdWithParamNew() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("blob[new]", 1)
                .queryParam("blob[name]", "cat.jpeg")
                .when()
                .put(baseURL + "blobs/" + publicBlobID + ".json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(blob_schema)));

        String blobName = response.path("blob.name");
        //  Assert.assertEquals(blobName, "updated_blob");

        HashMap<String, String> tmp_arr = UtilsMethods.parseBlobObject(response.path("blob.blob_object_access.params"));
        String tmp_bucketURL = UtilsMethods.getBucketUrl(response.path("blob.blob_object_access.params"));

        RequestSpecification reqSpec = given().
                contentType("multipart/form-data");

        tmp_arr.forEach((key, value) -> {
            try {
                reqSpec.multiPart(key, URLDecoder.decode(value, "UTF-8"), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        });
        reqSpec.multiPart("file", new File("./src/test/resources/cat.jpeg"));

        reqSpec.when()
                .post(tmp_bucketURL)
                .then()
                .statusCode(201)
                .log().all();

        Response declaring = this.extractResponseAndPrintLog(spec
                .queryParam("blob[size]", new File("./src/test/resources/cat.jpeg").length())
                .when()
                .post(baseURL + "blobs/" + publicBlobID + "/complete.json")
                .then());
        // .statusCode(200));
        Assert.assertEquals(declaring.statusCode(), 200, "Declaring file failed");

    }

    @Test(dependsOnMethods = {"getFilesList"})
    public void user2CreateAndUploadFile() {

        this.initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("blob[content_type]", "image/jpeg")
                //.queryParam("blob[name]", "ReadStatusSuite")
                .queryParam("blob[name]", "dog.jpeg")
                .when()
                .post(baseURL + "blobs.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(blob_schema)));

        HashMap<String, String> tmp_arr = UtilsMethods.parseBlobObject(response.path("blob.blob_object_access.params"));
        String tmp_bucketURL = UtilsMethods.getBucketUrl(response.path("blob.blob_object_access.params"));
        privateBlobID = response.path("blob.id");
        privateBlobUid = response.path("blob.uid");
        boolean isPublic = response.path("blob.public");
        Assert.assertEquals(isPublic, false, "By default created blob should be private");

        RequestSpecification reqSpec = given().
                contentType("multipart/form-data");

        tmp_arr.forEach((key, value) -> {
            try {
                reqSpec.multiPart(key, URLDecoder.decode(value, "UTF-8"), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        });
        reqSpec.multiPart("file", new File(imagePath));

        reqSpec.when()
                .post(tmp_bucketURL)
                .then()
                .statusCode(201)
                .log().all();

        this.initSpec("token2");

        this.extractResponseAndPrintLog(spec
                .queryParam("blob[size]", new File(imagePath).length())
                .when()
                .post(baseURL + "blobs/" + privateBlobID + "/complete.json")
                .then()
                .statusCode(200));

    }

    @Test(dependsOnMethods = {"user2CreateAndUploadFile"})
    public void downloadPrivateFileByIdWithoutToken() {

        given()
                .when()
                .get(baseURL + "blobs/" + privateBlobID + "/download.json")
                .then()
                .statusCode(403)
                .log().all();

    }

    @Test(dependsOnMethods = {"user2CreateAndUploadFile"})
    public void downloadPrivateFileByUidWithoutToken() {

        given()
                .when()
                .get(baseURL + "blobs/" + privateBlobUid + ".json")
                .then()
                .statusCode(403)
                .log().all();

    }

    @Test(dependsOnMethods = {"user2CreateAndUploadFile"})
    public void User1GetInformationAboutFileById() {

        Response response = this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "blobs/" + privateBlobID + ".json")
                .then()
                .assertThat().body(matchesJsonSchema(blob_schema))
                .statusCode(200));

    }

    @AfterClass
    public void deleteBlobById() {

        this.extractResponseAndPrintLog(spec
                .when()
                .delete(baseURL + "blobs/" + publicBlobID + ".json")
                .then()
                .assertThat().statusCode(200));
    }
//
//    @AfterClass
//    public void deleteBlobById2() throws IOException {
//
//        this.initSpec("user2Token");
//
//        this.extractResponseAndPrintLog(spec
//                .when()
//                .delete(baseURL + "blobs/" + tmp_blobID + ".json")
//                .then()
//                .assertThat().statusCode(200));
//    }

}
