package com.restApiTests.core;


import com.google.common.base.Utf8;
import com.restApiTests.sessions.providers.NewSessionData;
import com.utils.InputParameters;
import io.restassured.RestAssured;
import io.restassured.config.LogConfig;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;

import org.apache.commons.io.IOUtils;
import org.testng.annotations.BeforeMethod;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.restassured.RestAssured.given;
import static io.restassured.config.RestAssuredConfig.config;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

public class AbstractSuite {

    static {
        try {
            PrintStream stream = new PrintStream(new FileOutputStream("./rest-api-test-automator.log")) {

                @Override
                public void print(String s) {
                    System.out.println(s);
                    super.print(s);
                }
            };

            RestAssured.config = config().logConfig(new LogConfig(stream, true));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }

    protected final static String DIALOGS_JSON_SCHEMA = "json_schemas/dialogs-schema.json";
    protected final static String DIALOG_JSON_SCHEMA = "json_schemas/dialog-schema.json";
    protected final static String DIALOG_WITH_CUSTOM_FIELDS_JSON_SCHEMA = "json_schemas/dialog_with_custom_fields-schema.json";
    protected final static String DIALOG_WITH_JOINABLE_PARAM_JSON_SCHEMA = "json_schemas/dialog_with_joinable_param-schema.json";
    protected final static String BLOB_JSON_SCHEMA = "json_schemas/blob-schema.json";
    protected final static String BLOBS_JSON_SCHEMA = "json_schemas/blobs-schema.json";
    protected final static String CHAT_NOTIFICATIONS_JSON_SCHEMA = "json_schemas/chat_notifications-schema.json";
    protected final static String CUSTOM_OBJECT_JSON_SCHEMA = "json_schemas/custom_object-schema.json";
    protected final static String CUSTOM_OBJECTS_JSON_SCHEMA = "json_schemas/custom_objects-schema.json";
    protected final static String CUSTOM_OBJECT_CLASS_JSON_SCHEMA = "json_schemas/custom_object_class-schema.json";
    protected final static String CUSTOM_OBJECT_FILE_JSON_SCHEMA = "json_schemas/custom_object_file-schema.json";
    protected final static String EVENT_JSON_SCHEMA_FOR_RA = "json_schemas/event-schema-for-RA.json";
    protected final static String EVENTS_JSON_SCHEMA = "json_schemas/events-schema.json";
    protected final static String MESSAGE_JSON_SCHEMA = "json_schemas/message-schema.json";
    protected final static String MESSAGES_JSON_SCHEMA = "json_schemas/messages-schema.json";
    protected final static String OFFLINE_PUSH_JSON_SCHEMA = "json_schemas/offline_push-schema.json";
    protected final static String SESSION_JSON_SCHEMA = "json_schemas/session-schema.json";
    protected final static String SUBSCRIPTION_JSON_SCHEMA = "json_schemas/subscription-schema.json";
    protected final static String SUBSCRIPTIONS_JSON_SCHEMA = "json_schemas/subscriptions-schema.json";
    protected final static String USER_JSON_SCHEMA = "json_schemas/user-schema.json";
    protected final static String USERS_JSON_SCHEMA = "json_schemas/users-schema.json";

    private static final String password = "11111111";

    protected HashMap<String, Object> baseParams = new HashMap<>();

    protected RequestSpecification spec;

    protected String baseURL;
    protected String chatURL;
    protected String appID;

    protected Response extractResponseAndPrintLog(ValidatableResponse valRes) {
        return valRes.log().all().extract().response();
    }

    public AbstractSuite() {
        HashMap<String, String> params = InputParameters.initParams();

        baseURL = params.get("api_endpoint") + "/";
        chatURL = params.get("chat_endpoint");
        appID = params.get("application_id");

        NewSessionData.applicationId = params.get("application_id");
        NewSessionData.authKey = params.get("auth_key");
        NewSessionData.authSecret = params.get("auth_secret");

        NewSessionData.adminLogin = params.get("admin_login");
        NewSessionData.adminPassword = params.get("admin_password");

    }

    protected String createSession(HashMap<String, Object> params) {

        String schema = null;
        try {
            schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(SESSION_JSON_SCHEMA), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Response response =
                given()
//                        .contentType("application/json")
                        .given()
                        .params(params)
                        //.log().all()
                        .when()
                        .post(baseURL + "session.json")
                        .then()
                        .statusCode(201)
                        .log().ifStatusCodeIsEqualTo(404)
                        .assertThat().body(matchesJsonSchema(schema))
                        .extract()
                        .response();

        String QBTokenExpirationDate = response.getHeader("QB-Token-ExpirationDate");
        String tmp = null;
        Pattern z = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2}\\sUTC$");
        Matcher x = z.matcher(QBTokenExpirationDate);
        if (x.find()) {
            tmp = x.group(0);
        }

        Assert.assertNotNull(tmp);

        String token = response.path("session.token");

        return token;
    }

    protected int createUser(HashMap<String, Object> params) {

        String schema = null;
        try {
            schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(USER_JSON_SCHEMA), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Response response = this.extractResponseAndPrintLog(spec
                //.log().all()
                .queryParams(params)
                .when()
                .post(baseURL + "users.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(schema)));

        int userID1 = response.path("user.id");

        if (params.containsKey("resultKey")) {
            baseParams.put((String) params.get("resultKey"), response.path("user.login"));
        }

        return userID1;

    }

    protected void updateSession(String userLoginKey) {

        if(!userLoginKey.equals("userLogin1")){
            this.initSpec("token2");
        }

        String userLogin = (String) baseParams.get(userLoginKey);

//        String schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("user-schema.json"));

        this.extractResponseAndPrintLog(spec
                .queryParam("login", userLogin)
                .queryParam("password", password)
                .when()
                .post(baseURL + "login.json")
                .then()
//                .assertThat().body(matchesJsonSchema(schema))
                .statusCode(202));
    }

    @BeforeMethod
    protected void initSpec() {

        String token = (String) baseParams.get("token");

        if(token == null){
            token = "";
        }

        this.spec = given()
                .header("QB-Token", token)
            //    .contentType("application/json")
                .given();
        // .log().all();

    }

    protected void initSpec(String tokenKey) {

        String token = (String) baseParams.get(tokenKey);

        this.spec = given()
                .header("QB-Token", token)
               // .contentType("application/json")
                .given();
        // .log().all();

    }

}
