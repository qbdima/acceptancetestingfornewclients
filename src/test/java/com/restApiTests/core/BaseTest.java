package com.restApiTests.core;


import com.restApiTests.sessions.providers.NewSessionData;
import com.restApiTests.users.providers.NewApiUserData;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;


public class BaseTest extends AbstractSuite {

    @Test(dataProviderClass = NewSessionData.class, dataProvider = "newSessionWithoutUserData", groups = "baseTest")
    public void createSession1(HashMap<String, Object> params) {
        String token = super.createSession(params);
        baseParams.put("token", token);
    }

    @Test(dependsOnMethods = {"createSession1"}, dataProviderClass = NewApiUserData.class, dataProvider = "newApiUserDataWithOnlyRequiredFields", groups = "baseTest")
    public void createUser1(HashMap<String, Object> params) {
        int userID = super.createUser(params);
        baseParams.put("userID1", userID);
    }

    @Test(dependsOnMethods = {"createUser1"}, groups = "baseTest")
    public void updateSession1() {
        super.updateSession("userLogin1");
    }

    @Test(dependsOnMethods = {"updateSession1"}, dataProviderClass = NewSessionData.class, dataProvider = "newSessionWithoutUserData", groups = "baseTest")
    public void createSession2(HashMap<String, Object> params) {
        String token = super.createSession(params);
        baseParams.put("token2", token);
    }

    @Test(dependsOnMethods = {"createSession2"}, dataProviderClass = NewApiUserData.class, dataProvider = "newApiUserDataWithOnlyRequiredFields2", groups = "baseTest")
    public void createUser2(HashMap<String, Object> params) {
        int userID = super.createUser(params);
        baseParams.put("userID2", userID);
    }

    @Test(dependsOnMethods = {"createUser2"}, groups = "baseTest")
    public void updateSession2() {
        super.updateSession("userLogin2");
    }

    @AfterClass
    public void deleteUser1() {

        this.extractResponseAndPrintLog(spec
                .when()
                .delete(baseURL + "users/" + baseParams.get("userID1") + ".json")
                .then()
                .statusCode(200));
    }

    @AfterClass
    public void deleteUser2() {

        this.initSpec("token2");

        this.extractResponseAndPrintLog(spec
                .when()
                .delete(baseURL + "users/" + baseParams.get("userID2") + ".json")
                .then()
                .statusCode(200));
    }

    public Object getValue(String key) {
        return baseParams.get(key);
    }

    public void setValue(String key, Object value) {
        baseParams.put(key, value);
    }

    public Map<String, Object> getParams() {
        return baseParams;
    }

    public void setSpec(RequestSpecification spec) {
        this.spec = spec;
    }



}
