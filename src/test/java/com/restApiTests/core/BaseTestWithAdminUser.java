package com.restApiTests.core;


import com.restApiTests.sessions.providers.NewSessionData;
import io.restassured.response.Response;
import org.apache.commons.io.IOUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;


public class BaseTestWithAdminUser extends BaseTest{

    @Test(dependsOnGroups = {"baseTest"}, dataProvider = "newSessionWithAdminUserData", dataProviderClass = NewSessionData.class, groups = "baseTestWithAdminUser")
    public void createSessionForAccountOwner(HashMap sessionParams) throws SignatureException, IOException {

        String schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(SESSION_JSON_SCHEMA), "UTF-8");

        Response response =
                given()
                        .contentType("application/json")
                        .given()
                        .queryParams(sessionParams)
                        .log().all()
                        .when()
                        .post(baseURL + "session.json")
                        .then()
                        .assertThat().body(matchesJsonSchema(schema))
                        .statusCode(201).log().all()
                        .extract()
                        .response();

        String QBTokenExpirationDate = response.getHeader("QB-Token-ExpirationDate");
        String tmp = null;
        Pattern z = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2}\\sUTC$");
        Matcher x = z.matcher(QBTokenExpirationDate);
        if (x.find()) {
            tmp = x.group(0);
        }

        Assert.assertNotNull(tmp);

        String token = response.path("session.token");
        baseParams.put("admin_token", token);

    }

}
