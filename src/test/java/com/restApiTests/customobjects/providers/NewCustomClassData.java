package com.restApiTests.customobjects.providers;

import org.codehaus.jettison.json.JSONException;
import org.testng.annotations.DataProvider;

import java.security.SignatureException;
import java.util.HashMap;

public class NewCustomClassData {

    @DataProvider(name = "newCustomClassData")
    public static Object[][] newCustomClassData() throws SignatureException, JSONException {

        HashMap<String, Object> customClassData = new HashMap<>();
        customClassData.put("name", "ClassWithAllFieldsType");
        customClassData.put("fields[integer]", "Integer");
        customClassData.put("fields[float]", "Float");
        customClassData.put("fields[boolean]", "Boolean");
        customClassData.put("fields[string]", "String");

        customClassData.put("fields[file]", "File");
        customClassData.put("fields[location]", "Location");
        customClassData.put("fields[date]", "Date");

        customClassData.put("fields[integer_array]", "Integer_a");
        customClassData.put("fields[float_array]", "Float_a");
        customClassData.put("fields[boolean_array]", "Boolean_a");
        customClassData.put("fields[string_array]", "String_a");

        customClassData.put("permissions[create][access]", "open");
        customClassData.put("permissions[read][access]", "open");
        customClassData.put("permissions[update][access]", "open");

        return new Object[][]{{customClassData}};

    }


}
