package com.restApiTests.customobjects.providers;

import org.codehaus.jettison.json.JSONException;
import org.testng.annotations.DataProvider;

import java.security.SignatureException;
import java.util.HashMap;

public class NewCustomObjectData {

    @DataProvider(name = "newCustomObjectData")
    public static Object[][] newCustomObjectData() throws SignatureException, JSONException {

        HashMap<String, Object> customObjectData = new HashMap<>();
        customObjectData.put("boolean", true);
        customObjectData.put("boolean_array", "true,false");
        customObjectData.put("date", "2016-06-11 00:15:17 +0000");
        customObjectData.put("float", 1.5);
        customObjectData.put("float_array", "1.5,3.8,4.5");
        customObjectData.put("integer", 4);
        customObjectData.put("integer_array", "1,5,8,7");
        customObjectData.put("location", "16.5,36.8");
        customObjectData.put("string", "my_str");
        customObjectData.put("string_array", "str1, str2, str3");

        return new Object[][]{{customObjectData}};
    }

}
