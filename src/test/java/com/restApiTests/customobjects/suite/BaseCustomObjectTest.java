package com.restApiTests.customobjects.suite;


import com.restApiTests.core.BaseTestWithAdminUser;
import com.restApiTests.customobjects.providers.NewCustomClassData;
import io.restassured.response.Response;
import org.apache.commons.io.IOUtils;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;


public class BaseCustomObjectTest extends BaseTestWithAdminUser {

    protected final static String CustomObjectsClass = "ClassWithAllFieldsType2";

    private static AtomicInteger count = new AtomicInteger(0);

    protected String custom_object_class_schema;
    protected String custom_object_schema;
    protected String custom_object_file_schema;
    protected String custom_objects_schema;

    @BeforeTest
    public void setUp(){
        try {
            custom_object_class_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(CUSTOM_OBJECT_CLASS_JSON_SCHEMA), "UTF-8");
            custom_object_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(CUSTOM_OBJECT_JSON_SCHEMA), "UTF-8");
            custom_object_file_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(CUSTOM_OBJECT_FILE_JSON_SCHEMA), "UTF-8");
            custom_objects_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(CUSTOM_OBJECTS_JSON_SCHEMA), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnGroups = {"baseTestWithAdminUser"})
    public void user1InitCustomObjectsClass() {

        //решение работает но нужно разнести тесты по разным концам сьюта
        if (count.get() == 0) {
            count.incrementAndGet();
        } else {
            return;
        }

        Response response = this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "data/" + CustomObjectsClass + ".json")
                .then());

        if (response.statusCode() == 403) {

            this.initSpec("admin_token");

            this.extractResponseAndPrintLog(spec
                    .queryParam("name", CustomObjectsClass)
                    .queryParam("fields[integer]", "Integer")
                    .queryParam("fields[float]", "Float")
                    .queryParam("fields[boolean]", "Boolean")
                    .queryParam("fields[string]", "String")
                    .queryParam("fields[file]", "File")
                    .queryParam("fields[location]", "Location")
                    .queryParam("fields[date]", "Date")
                    .queryParam("fields[integer_array]", "Integer_a")
                    .queryParam("fields[float_array]", "Float_a")
                    .queryParam("fields[boolean_array]", "Boolean_a")
                    .queryParam("fields[string_array]", "String_a")
                    .queryParam("permissions[create][access]", "open")
                    .queryParam("permissions[read][access]", "open")
                    .queryParam("permissions[update][access]", "open")
                    .when()
                    .post(baseURL + "class.json")
                    .then()
                    .statusCode(201)
                    .assertThat().body(matchesJsonSchema(custom_object_class_schema)));
        }
    }

}
