package com.restApiTests.customobjects.suite;

import com.restApiTests.core.BaseTestWithAdminUser;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CustomObjectDateField extends BaseTestWithAdminUser {
    private final static String CustomObjectsClass = "TestAPI";
    private Pattern z = Pattern.compile("\\d{4}\\-\\d{2}\\-\\d{2}T\\d{2}\\:\\d{2}\\:\\d{2}Z");


    @Test(dependsOnGroups = {"baseTestWithAdminUser"})
    public void createCustomObjectsClass() {

        Response response = this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "data/" + CustomObjectsClass + ".json")
                .then());

        if (response.statusCode() == 403) {

            this.initSpec("admin_token");

            this.extractResponseAndPrintLog(spec
                    .queryParam("name", CustomObjectsClass)
                    .queryParam("fields[name]", "String")
                    .queryParam("fields[test_date]", "Date")
                    .when()
                    .post(baseURL + "class.json")
                    .then()
                    .statusCode(201));
        }
    }

    @Test(dependsOnMethods = {"createCustomObjectsClass"})
    public void correctFormatDate() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("test_date", "2016-04-09T12:04:00")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));

        String date_format = response.path("test_date");
        String tmp = null;
        Matcher x = z.matcher(date_format);
        if (x.find()) {
            tmp = x.group(0);
        }
        //  Assert.assertNotNull(tmp, "Custom object date type field has incorrect format");
        Assert.assertNotNull(tmp, "Expected format: " + z.toString() + " but found: " + date_format);
    }

    @Test(dependsOnMethods = {"createCustomObjectsClass"})
    public void shortFormatWithBash() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("test_date", "2016-04-09")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));

        String date_format = response.path("test_date");
        String tmp = null;
        Matcher x = z.matcher(date_format);
        if (x.find()) {
            tmp = x.group(0);
        }
        Assert.assertNotNull(tmp, "Custom object date type field has incorrect format");
    }

    @Test(dependsOnMethods = {"createCustomObjectsClass"})
    public void shortFormatWithoutSymbols() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("test_date", "20160420")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));

        String date_format = response.path("test_date");
        String tmp = null;
        Matcher x = z.matcher(date_format);
        if (x.find()) {
            tmp = x.group(0);
        }
        Assert.assertNotNull(tmp, "Custom object date type field has incorrect format");
    }

    @Test(dependsOnMethods = {"createCustomObjectsClass"})
    public void dateTimeFormat() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("test_date", "2016-04-30 16:40")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));

        String date_format = response.path("test_date");
        String tmp = null;
        Matcher x = z.matcher(date_format);
        if (x.find()) {
            tmp = x.group(0);
        }
        Assert.assertNotNull(tmp, "Custom object date type field has incorrect format");
    }

    @Test(dependsOnMethods = {"createCustomObjectsClass"})
    public void shortFormatWithSlash() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("test_date", "2016/04/20")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));

        String date_format = response.path("test_date");
        String tmp = null;
        Matcher x = z.matcher(date_format);
        if (x.find()) {
            tmp = x.group(0);
        }
        Assert.assertNotNull(tmp, "Custom object date type field has incorrect format");
    }

    @Test(dependsOnMethods = {"createCustomObjectsClass"})
    public void shortFormatWithDots() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("test_date", "2016.07.22")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));

        String date_format = response.path("test_date");
        String tmp = null;
        Matcher x = z.matcher(date_format);
        if (x.find()) {
            tmp = x.group(0);
        }
        Assert.assertNotNull(tmp, "Custom object date type field has incorrect format");
    }

    @Test(dependsOnMethods = {"createCustomObjectsClass"})
    public void dateTimeSecFormat() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("test_date", "2016-04-30 16:40:00")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));

        String date_format = response.path("test_date");
        String tmp = null;
        Matcher x = z.matcher(date_format);
        if (x.find()) {
            tmp = x.group(0);
        }
        Assert.assertNotNull(tmp, "Custom object date type field has incorrect format");
    }

    @Test(dependsOnMethods = {"createCustomObjectsClass"})
    public void dateDayMonthYearFormat() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("test_date", "30-04-2016")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));

        String date_format = response.path("test_date");
        String tmp = null;
        Matcher x = z.matcher(date_format);
        if (x.find()) {
            tmp = x.group(0);
        }
        Assert.assertNotNull(tmp, "Custom object date type field has incorrect format");
    }

    @Test(dependsOnMethods = {"createCustomObjectsClass"})
    public void dateDayMonthYearTimeFormat() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("test_date", "30-04-2016 11:12:00")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));

        String date_format = response.path("test_date");
        String tmp = null;
        Matcher x = z.matcher(date_format);
        if (x.find()) {
            tmp = x.group(0);
        }
        Assert.assertNotNull(tmp, "Custom object date type field has incorrect format");
    }

    @Test(dependsOnMethods = {"createCustomObjectsClass"})
    public void incorrectStringFormat() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("test_date", "testtest")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));
        Assert.assertEquals(response.path("test_date"), "testtest");

    }
}
