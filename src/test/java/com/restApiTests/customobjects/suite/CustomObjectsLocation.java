package com.restApiTests.customobjects.suite;


import com.utils.Distance;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CustomObjectsLocation extends BaseCustomObjectTest {

    private final static String CustomObjectsClass = "ClassWithAllFieldsType2";

//    @Test(dependsOnGroups = {"baseTest"})
    @Test(dependsOnMethods = {"user1InitCustomObjectsClass"})
    public void createMultipleRecord() {

        this.extractResponseAndPrintLog(spec
                .params("location", "11.2,36.8")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));

        this.extractResponseAndPrintLog(spec
                .params("location", "15.2,21.4")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));

        this.extractResponseAndPrintLog(spec
                .params("location", "11.2,36.2")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));

        this.extractResponseAndPrintLog(spec
                .params("location", "33.2,-21.4")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));

        this.extractResponseAndPrintLog(spec
                .params("location", "10.9,36.5")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));
    }

    @Test(dependsOnMethods = {"createMultipleRecord"})
    public void getLocationsNear() {

        Response response = this.extractResponseAndPrintLog(spec
                .params("location[near]", "11.3,36.8;100000")
                .when()
                .get(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(200));

        List<HashMap<String, List<Float>>> items = response.path("items");
        Assert.assertEquals(items.size(), 3);

        List<Float> fixPoints = new ArrayList<>();
        fixPoints.add(11.3f);
        fixPoints.add(36.8f);

        for (int i = 0; i < items.size() - 1; i++) {
            List<Float> firstPoint = items.get(i).get("location");
            List<Float> secondPoint = items.get(i + 1).get("location");

            Assert.assertTrue(Distance.isLt(firstPoint, secondPoint, fixPoints));
        }
    }

}
