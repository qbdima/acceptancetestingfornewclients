package com.restApiTests.customobjects.suite;

import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomObjectsOperationsWithCriteria extends BaseCustomObjectTest {

    private final static String CustomObjectsClass = "ClassWithAllFieldsType2";

    @Test(dependsOnGroups = {"baseTest"})
    public void createMultipleRecord() {

        this.extractResponseAndPrintLog(spec
                .params("integer", "11")
                .params("string", "first record")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));

        this.extractResponseAndPrintLog(spec
                .params("integer", "103")
                .params("string", "second record")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));

        this.extractResponseAndPrintLog(spec
                .params("integer", "5")
                .params("string", "third record")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));

        this.extractResponseAndPrintLog(spec
                .params("integer", "52")
                .params("string", "fourth record")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));

        this.extractResponseAndPrintLog(spec
                .params("integer", "110")
                .params("string", "fifth record")
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201));
    }

    @Test(dependsOnMethods = "createMultipleRecord")
    public void updateRecordsByCriteria(){
        Response response = this.extractResponseAndPrintLog(spec
                .params("boolean", true)
                .params("search_criteria[integer][lt]", 100)
                .when()
                .put(baseURL + "data/" + CustomObjectsClass + "/by_criteria.json")
                .then()
                .statusCode(200));

        ArrayList<HashMap<String, Object>> items = response.path("items");

        Assert.assertEquals((int)response.path("total_found"), 3, "total_found field contained incorrect counter");
        Assert.assertEquals(items.size(), 3, "incorrect count of items returned");

        for(HashMap item : items){
            Assert.assertEquals(item.get("boolean"), true);
            boolean isLess = false;
            if((int)item.get("integer") < 100){
                isLess = true;
            }
            Assert.assertTrue(isLess);

        }
    }

    @Test(dependsOnMethods = "updateRecordsByCriteria", alwaysRun = true)
    public void deleteRecordsByCriteria(){
        Response response = this.extractResponseAndPrintLog(spec
                .params("integer[lt]", 100)
                .when()
                .delete(baseURL + "data/" + CustomObjectsClass + "/by_criteria.json")
                .then()
                .statusCode(200));

        Assert.assertEquals((int)response.path("total_deleted"), 3);
    }

    @Test(dependsOnMethods = "deleteRecordsByCriteria")
    public void getRecords(){
        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("user_id", (int)baseParams.get("userID1"))
                .when()
                .get(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(200)
                .assertThat());

        ArrayList<HashMap<String, Object>> items = response.path("items");
        Assert.assertEquals(items.size(), 2);

        for(HashMap item : items){

            boolean isLess = (int)item.get("integer") > 100 ? true : false;

            Assert.assertTrue(isLess);
        }
    }

}
