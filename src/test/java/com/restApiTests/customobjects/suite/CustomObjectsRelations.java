package com.restApiTests.customobjects.suite;


import com.restApiTests.customobjects.providers.NewCustomObjectData;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;


public class CustomObjectsRelations extends BaseCustomObjectTest {

    private final static String CustomObjectsClassForRelationsTest = "ClassForRelationsTest";
    private final static String CustomObjectsClass = "ClassWithAllFieldsType2";

    private String customObjectId;

    private String relatedObjectId1;
    private String relatedObjectId2;
    private String relatedObjectId3;

    @Test(dependsOnGroups = {"baseTestWithAdminUser"})
    public void user1InitCustomObjectsClassForRelationsTest() {

        Response response = this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "data/" + CustomObjectsClassForRelationsTest + ".json")
                .then());

        if (response.statusCode() == 403) {

            this.initSpec("admin_token");

            this.extractResponseAndPrintLog(spec
                    .queryParam("name", CustomObjectsClassForRelationsTest)
                    .queryParam("fields[related_integer]", "Integer")
                    .queryParam("fields[float]", "Float")
                    .queryParam("fields[related_boolean]", "Boolean")
                    .queryParam("fields[string]", "String")
                    .queryParam("permissions[create][access]", "open")
                    .queryParam("permissions[read][access]", "open")
                    .queryParam("permissions[update][access]", "open")
                    .when()
                    .post(baseURL + "class.json")
                    .then()
                    .statusCode(201));
        }
    }

    @Test(dependsOnMethods = {"user1InitCustomObjectsClass"}, dataProvider = "newCustomObjectData", dataProviderClass = NewCustomObjectData.class)
    public void createRecord(HashMap customObjectData) {

        customObjectData.put("integer", 101);

        Response response = this.extractResponseAndPrintLog(spec
                .params(customObjectData)
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(custom_object_schema)));
        customObjectId = response.path("_id");
    }

    @Test(dependsOnMethods = {"createRecord"})
    public void createRelations() {

        Response response = this.extractResponseAndPrintLog(spec
                .params("related_integer", 1)
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + "/" + customObjectId + "/" + CustomObjectsClassForRelationsTest + ".json")
                .then()
                .statusCode(201));

        relatedObjectId1 = response.path("_id");
        Assert.assertEquals(response.path("_parent_id"), customObjectId);

        Response response2 = this.extractResponseAndPrintLog(spec
                .params("related_integer", 2)
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + "/" + customObjectId + "/" + CustomObjectsClassForRelationsTest + ".json")
                .then()
                .statusCode(201));

        relatedObjectId2 = response2.path("_id");
        Assert.assertEquals(response2.path("_parent_id"), customObjectId);

        Response response3 = this.extractResponseAndPrintLog(spec
                .params("related_integer", 3)
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + "/" + customObjectId + "/" + CustomObjectsClassForRelationsTest + ".json")
                .then()
                .statusCode(201));

        relatedObjectId3 = response3.path("_id");
        Assert.assertEquals(response3.path("_parent_id"), customObjectId);
    }

    @Test(dependsOnMethods = {"createRelations"})
    public void getRelatedRecords() {

        Response response = this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "data/" + CustomObjectsClass + "/" + customObjectId + "/" + CustomObjectsClassForRelationsTest + ".json")
                .then()
                .statusCode(200));

        ArrayList<HashMap> items = response.path("items");

        Assert.assertEquals(items.size(), 3);
        Assert.assertEquals(response.path("class_name"), CustomObjectsClassForRelationsTest);
        for (HashMap item : items) {
            Assert.assertEquals(item.get("_parent_id"), customObjectId);
        }

    }

    @Test(dependsOnMethods = {"getRelatedRecords"})
    public void deleteParentRecord() {
        this.extractResponseAndPrintLog(spec
                .when()
                .delete(baseURL + "data/" + CustomObjectsClass + "/" + customObjectId + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"deleteParentRecord"})
    public void getRecords() {

        Response response = this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "data/" + CustomObjectsClassForRelationsTest + ".json")
                .then()
                .statusCode(200));

        ArrayList items = response.path("items");

        Assert.assertEquals(items.size(), 0);
    }


}
