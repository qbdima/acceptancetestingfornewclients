package com.restApiTests.customobjects.suite;


import com.restApiTests.customobjects.providers.NewCustomObjectData;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.util.HashMap;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;


public class CustomObjectsSuite extends BaseCustomObjectTest {

    private String customObjectId;


    @Test(dependsOnMethods = {"user1InitCustomObjectsClass"}, dataProvider = "newCustomObjectData", dataProviderClass = NewCustomObjectData.class)
    public void createRecord(HashMap customObjectData) {

        Response response = this.extractResponseAndPrintLog(spec
                .params(customObjectData)
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(custom_object_schema)));
        customObjectId = response.path("_id");
    }

    @Test(dependsOnMethods = {"createRecord"})
    public void createFileInCO() {

        RequestSpecification reqSpec = given().
                header("QB-Token", baseParams.get("token")).
                contentType("multipart/form-data");

        reqSpec.multiPart("file", new File("./src/test/resources/dog.jpeg"));
        reqSpec.multiPart("field_name", "file");

        reqSpec.when()
                .post(baseURL + "data/" + CustomObjectsClass + "/" + customObjectId + "/file.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(custom_object_file_schema))
                .log().all();

    }

    @Test(dependsOnMethods = {"createFileInCO"})
    public void getRecordById() {

        Response response = this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "data/" + CustomObjectsClass + "/" + customObjectId + ".json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(custom_object_schema)));
    }

    @Test(dependsOnMethods = {"getRecordById"})
    public void updateRecord() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("boolean", false)
                .queryParam("string", "updated_string")
                .queryParam("integer", 8)
                .when()
                .put(baseURL + "data/" + CustomObjectsClass + "/" + customObjectId + ".json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(custom_object_schema)));

        Assert.assertEquals((boolean) response.path("boolean"), false);
        Assert.assertEquals(response.path("string"), "updated_string");
        Assert.assertEquals((int) response.path("integer"), 8);
    }

    @Test(dependsOnMethods = {"updateRecord"}, dataProvider = "newCustomObjectData", dataProviderClass = NewCustomObjectData.class, enabled = false)
    public void createRecordAndUploadFileToIncorrectField(HashMap customObjectData) {

        Response response = this.extractResponseAndPrintLog(spec
                .params(customObjectData)
                .when()
                .post(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(custom_object_schema)));
        String objectId = response.path("_id");

        RequestSpecification reqSpec = given().
                header("QB-Token", baseParams.get("token")).
                contentType("multipart/form-data");

        reqSpec.multiPart("file", new File("./src/test/resources/dog.jpeg"));
        reqSpec.multiPart("field_name", "file2");

        reqSpec.when()
                .post(baseURL + "data/" + CustomObjectsClass + "/" + objectId + "/file.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(custom_object_file_schema))
                .log().all();

        this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "data/" + CustomObjectsClass + "/" + objectId + ".json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(custom_object_schema)));
    }

    @Test(dependsOnMethods = {"updateRecord"})
    public void getRecords() {

        Response response = this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "data/" + CustomObjectsClass + ".json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(custom_objects_schema)));
    }

    @Test(dependsOnMethods = {"getRecords"})
    public void deleteRecord() {
        this.extractResponseAndPrintLog(spec
                .when()
                .delete(baseURL + "data/" + CustomObjectsClass + "/" + customObjectId + ".json")
                .then()
                .statusCode(200));
    }

}
