package com.restApiTests.pushnotifications.providers;


import org.codehaus.jettison.json.JSONObject;
import org.testng.annotations.DataProvider;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;

public class EventData {

    @DataProvider(name = "eventData")
    public static Object[][] eventData() throws UnsupportedEncodingException {

        HashMap<String, String> eventData = new HashMap<>();

        String prefix = "payload=";

        HashMap<String, Object> pushData = new HashMap<>();

        Date d = new Date();
        SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy 'at' HH:mm:ss");

        HashMap<String, String> aps = new HashMap<>();
        aps.put("alert", "Push notification from autotest send " + format1.format(d));
        aps.put("badge", "9");
        aps.put("sound", "default");

        JSONObject apsObject = new JSONObject(aps);

        pushData.put("aps", apsObject);
        pushData.put("key1", "value1");
        pushData.put("key2", "value2");
        pushData.put("key3", "value3");

        String msgBase64;

        JSONObject object = new JSONObject(pushData);

        msgBase64 = Base64.getEncoder().encodeToString(object.toString().getBytes("utf-8"));
        String message = prefix + msgBase64;

        eventData.put("event[notification_type]", "push");
        eventData.put("event[push_type]", "apns");
        eventData.put("event[environment]", "development");
        eventData.put("event[message]", message);

        return new Object[][]{{eventData}};
    }

    @DataProvider(name = "eventData2")
    public static Object[][] eventData2() throws UnsupportedEncodingException {

        HashMap<String, String> eventData = new HashMap<>();

        String prefix = "payload=";

        HashMap<String, Object> pushData = new HashMap<>();

        Date d = new Date();
        SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy 'at' HH:mm:ss");

        HashMap<String, String> aps = new HashMap<>();
        aps.put("alert", "Dima's push");
        aps.put("badge", "9");
        aps.put("sound", "default");

        JSONObject apsObject = new JSONObject(aps);

        pushData.put("aps", apsObject);
        pushData.put("key1", "value1");
        pushData.put("key2", "value2");
        pushData.put("key3", "value3");

        String msgBase64;

        JSONObject object = new JSONObject(pushData);

        msgBase64 = Base64.getEncoder().encodeToString(object.toString().getBytes("utf-8"));
        String message = prefix + msgBase64;

        eventData.put("event[notification_type]", "push");
        eventData.put("event[push_type]", "apns");
        eventData.put("event[environment]", "development");
        eventData.put("event[message]", message);

        return new Object[][]{{eventData}};
    }
}
