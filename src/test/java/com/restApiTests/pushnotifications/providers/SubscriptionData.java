package com.restApiTests.pushnotifications.providers;


import org.testng.annotations.DataProvider;

import java.util.HashMap;

public class SubscriptionData {

    @DataProvider(name = "subscriptionDataAPNS")
    public static Object[][] subscriptionDataAPNS() {

        HashMap<String, String> subscriptionData = new HashMap<>();
        subscriptionData.put("notification_channels", "apns");
        subscriptionData.put("push_token[environment]", "development");
        subscriptionData.put("push_token[client_identification_sequence]", "df23ee81bad8be0575cf457081c695913b040fca0b2cc4fd89a972fe50c66f55");
        subscriptionData.put("device[platform]", "ios");
        subscriptionData.put("device[udid]", "5FF7AFC0-7A09-4DCE-9E12-685EF475CEC7");

        return new Object[][]{{subscriptionData}};
    }

    @DataProvider(name = "subscriptionDataAPNSAutotest")
    public static Object[][] subscriptionDataAPNSAutotest() {

        HashMap<String, String> subscriptionData = new HashMap<>();
        subscriptionData.put("notification_channels", "apns");
        subscriptionData.put("push_token[environment]", "development");
        subscriptionData.put("push_token[client_identification_sequence]", "aa7115e02fa35f9f8fc1efc4bc14822b73e5bf0b52b205b6ae346ce375eb1bc3");
        subscriptionData.put("device[platform]", "ios");
        subscriptionData.put("device[udid]", "push-autotest");

        return new Object[][]{{subscriptionData}};
    }

    @DataProvider(name = "subscriptionDataGCM")
    public static Object[][] subscriptionDataGCM() {
        HashMap<String, String> subscriptionData = new HashMap<>();
        subscriptionData.put("notification_channels", "gcm");
        subscriptionData.put("push_token[environment]", "development");
        subscriptionData.put("push_token[client_identification_sequence]", "df23ee81bad8be0575cf457081c695913b040fca0b2cc4fd89a972fe50c66f33");
        subscriptionData.put("device[platform]", "android");
        subscriptionData.put("device[udid]", "5FF7AFC0-7A09-4DCE-9E12-685EF475CEC71");

        return new Object[][]{{subscriptionData}};
    }

}
