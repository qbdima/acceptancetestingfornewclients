package com.restApiTests.pushnotifications.suite;

import com.restApiTests.chat.providers.NewMessageData;
import com.restApiTests.core.BaseTest;
import com.restApiTests.pushnotifications.providers.EventData;
import com.restApiTests.pushnotifications.providers.SubscriptionData;
import com.restApiTests.sessions.providers.NewSessionData;
import com.utils.ConfigParser;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.HashMap;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;


public class PushNotificationsDelivery extends BaseTest {

    //    private final static String PUSH_SERVICE_URL = "http://192.168.1.52:1337/pushstatus";
    private final static String PUSH_SERVICE_URL = "http://tsung2.quickblox.com:6888/pushstatus";

    private final static String OFFLINE_SCHEMA = "json_schemas/offline_push-schema.json";

    private int eventID;
    private int subscriptionID;

    private String groupDialogId;

    @Test(dependsOnGroups = {"baseTest"})
    public void setUp() {
        this.extractResponseAndPrintLog(spec
                .given()
                .log().all()
                .when()
//                .get("http://192.168.1.52:1337/reset")
                .get("http://tsung2.quickblox.com:6888/reset")
                .then());
    }

    @Test(dependsOnGroups = {"baseTest"}, dataProvider = "subscriptionDataAPNSAutotest", dataProviderClass = SubscriptionData.class)
    public void createSubscription(HashMap subscriptionData) throws IOException {

        String schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(SUBSCRIPTION_JSON_SCHEMA));

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(subscriptionData)
                .when()
                .post(baseURL + "subscriptions.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(schema)));

        subscriptionID = response.path("[0].subscription.id");
    }

    @Test(dependsOnGroups = {"baseTest"})
    public void user1CreateGroupDialog() throws IOException {

        String schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(DIALOG_JSON_SCHEMA));

        int userID1 = (int) baseParams.get("userID1");
        int userID2 = (int) baseParams.get("userID2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("type", 2)
                .queryParam("name", "offline_push_test")
                .queryParam("occupants_ids", userID2)
                .when()
                .post(baseURL + "chat/Dialog.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(schema))
                .assertThat().body("type", equalTo(2))
                .and()
                .assertThat().body("name", equalTo("offline_push_test"))
                .assertThat().body("occupants_ids", contains(userID1, userID2)));


        groupDialogId = response.path("_id");
    }

    @Test(dependsOnMethods = {"createSubscription"}, dataProvider = "eventData2", dataProviderClass = EventData.class, alwaysRun = true)
    public void createEvent(HashMap eventData) throws IOException {

        String schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(EVENT_JSON_SCHEMA_FOR_RA));

        eventData.put("event[user][ids]", baseParams.get("userID1"));

        Response response = this.extractResponseAndPrintLog(spec
                .given()
                .log().all()
                .queryParams(eventData)
                .when()
                .post(baseURL + "events.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(schema)));

        eventID = response.path("event.id");
    }

    @Test(dependsOnMethods = {"createEvent"})
    public void checkEventDelivery() throws IOException {

        boolean isEventReceived = false;
        int count = 0;

        Response response = null;

        while (!isEventReceived && count < 10) {
            response = this.extractResponseAndPrintLog(spec
                    .given()
                    .log().all()
                    .when()
                    .get(PUSH_SERVICE_URL)
                    .then());

            if (response.statusCode() == 404) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                count++;
            } else {
                isEventReceived = true;
            }

        }

        Assert.assertTrue(isEventReceived, "Event didn't deliver");
        Assert.assertEquals(response.path("aps.alert"), "Dima's push");

    }

    @Test(dependsOnMethods = {"user1CreateGroupDialog", "checkEventDelivery"}, dataProvider = "newMessageForGroupDialog", dataProviderClass = NewMessageData.class)
    public void user1СreateMessageForGroupDialog(HashMap message) throws IOException {
        String schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(MESSAGE_JSON_SCHEMA));

        message.put("chat_dialog_id", groupDialogId);
        message.put("send_to_chat", 1);

        initSpec("token2");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(message)
                .when()
                .post(baseURL + "chat/Message.json")
                .then()
                .statusCode(201));
//                .assertThat().body(matchesJsonSchema(schema)));

    }

    @Test(dependsOnMethods = {"user1СreateMessageForGroupDialog"})
    public void checkOfflinePushDelivery() throws IOException {

        String schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(OFFLINE_SCHEMA));

        boolean isEventReceived = false;
        int count = 0;

        Response response = null;

        while (!isEventReceived && count < 10) {


            ValidatableResponse valRes = when()
                    .get(PUSH_SERVICE_URL)
                    .then();

            response = valRes.extract().response();

//            response = this.extractResponseAndPrintLog(spec
//                    .given()
//                    .log().all()
//                    .when()
//                    .get(PUSH_SERVICE_URL)
//                    .then()
//                    .assertThat().body(matchesJsonSchema(schema)));

            if (response.statusCode() == 404) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                count++;
            } else {
                isEventReceived = true;
                valRes.assertThat().body(matchesJsonSchema(schema));
            }

        }

        Assert.assertTrue(isEventReceived, "Event didn't deliver");
        Assert.assertEquals(((String) response.path("aps.alert")).split(":")[0], ConfigParser.getClientName());

    }

    @Test(dependsOnMethods = {"checkOfflinePushDelivery"})
    public void deleteDialog() {
        this.extractResponseAndPrintLog(spec
                .queryParam("force", 1)
                .when()
                .delete(baseURL + "chat/Dialog/" + groupDialogId + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"checkOfflinePushDelivery"})
    public void deleteEvent() {
        this.extractResponseAndPrintLog(spec
                .when()
                .delete(baseURL + "events/" + eventID + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"checkOfflinePushDelivery"})
    public void deleteSubscription() {
        this.extractResponseAndPrintLog(spec
                .when()
                .delete(baseURL + "subscriptions/" + subscriptionID + ".json")
                .then()
                .statusCode(200));
    }


}
