package com.restApiTests.pushnotifications.suite;


import com.restApiTests.core.BaseTest;
import com.restApiTests.pushnotifications.providers.EventData;
import com.restApiTests.pushnotifications.providers.SubscriptionData;
import com.restApiTests.sessions.providers.NewSessionData;
import io.restassured.response.Response;
import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.HashMap;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;


public class PushNotificationsSuiteForRA extends BaseTest {

    private int eventID;
    private int subscriptionID;

    private String customToken;

    private String subscription_schema;
    private String subscriptions_schema;

    private String event_schema;
    private String events_schema;

    @BeforeTest
    public void setUp() {
        try {
            subscription_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(SUBSCRIPTION_JSON_SCHEMA), "UTF-8");
            subscriptions_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(SUBSCRIPTIONS_JSON_SCHEMA), "UTF-8");

            event_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(EVENT_JSON_SCHEMA_FOR_RA), "UTF-8");
            events_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(EVENTS_JSON_SCHEMA), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnGroups = {"baseTest"}, dataProvider = "subscriptionDataAPNS", dataProviderClass = SubscriptionData.class)
    public void createSubscription(HashMap subscriptionData) {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(subscriptionData)
                .when()
                .post(baseURL + "subscriptions.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(subscription_schema)));

        subscriptionID = response.path("[0].subscription.id");
    }

    @Test(dependsOnMethods = {"createSubscription"}, dataProvider = "subscriptionDataAPNS", dataProviderClass = SubscriptionData.class)
    public void createSubscriptionSecondTime(HashMap subscriptionData) {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(subscriptionData)
                .when()
                .post(baseURL + "subscriptions.json")
                .then()
                .statusCode(201));

        Assert.assertEquals((int) response.path("[0].subscription.id"), subscriptionID);
    }

    @Test(dependsOnMethods = {"createSubscriptionSecondTime"}, alwaysRun = true)
    public void createSessionSecondTime() {
        HashMap params = null;
        try {
            params = (HashMap) NewSessionData.newSessionWithoutUserData()[0][0];
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        customToken = super.createSession(params);
        baseParams.put("customToken", customToken);

        this.initSpec("customToken");

        String userLogin = (String) baseParams.get("userLogin1");

        this.extractResponseAndPrintLog(spec
                .queryParam("login", userLogin)
                .queryParam("password", 11111111)
                .when()
                .post(baseURL + "login.json")
                .then()
                .statusCode(202));

    }

    @Test(dependsOnMethods = {"createSessionSecondTime"}, dataProvider = "subscriptionDataGCM", dataProviderClass = SubscriptionData.class, alwaysRun = true)
    public void createGCMSubscription(HashMap subscriptionData) {

        this.initSpec("customToken");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(subscriptionData)
                .when()
                .post(baseURL + "subscriptions.json")
                .then()
                .statusCode(201));

        Assert.assertEquals(response.path("[0].subscription.notification_channel.name"), "gcm");
        Assert.assertEquals(response.path("[0].subscription.device.udid"), subscriptionData.get("device[udid]"));
        Assert.assertEquals(response.path("[0].subscription.device.platform.name"), "android");
    }

    @Test(dependsOnMethods = {"createGCMSubscription"})
    public void getSubscriptionsAfterSubscribing() {

        Response response = this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "subscriptions.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(subscriptions_schema)));

        ArrayList subscriptions = response.path("$");
        Assert.assertEquals(subscriptions.size(), 2, "User has some incorrect subscriptions");
    }

    @Test(dependsOnMethods = {"getSubscriptionsAfterSubscribing"}, dataProvider = "eventData", dataProviderClass = EventData.class, alwaysRun = true)
    public void createEvent(HashMap eventData) {

        eventData.put("event[user][ids]", baseParams.get("userID1"));

        Response response = this.extractResponseAndPrintLog(spec
                .given()
                .log().all()
                .queryParams(eventData)
                .when()
                .post(baseURL + "events.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(event_schema)));

        eventID = response.path("event.id");
    }

    @Test(dependsOnMethods = {"createEvent"})
    public void updateEvent() {

        Response response = this.extractResponseAndPrintLog(spec
                .given()
                .log().all()
                .queryParam("event[name]", "updated")
                .when()
                .put(baseURL + "events/" + eventID + ".json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(event_schema)));

        Assert.assertEquals(response.path("event.name"), "updated");

    }

    @Test(dependsOnMethods = {"createEvent"})
    public void getEvents() {

        Response response = this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "events.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(events_schema)));
    }

    @Test(dependsOnMethods = {"createEvent"})
    public void getEventById() {

        Response response = this.extractResponseAndPrintLog(spec
                .when()
//                .log().path()
                .get(baseURL + "events/" + eventID + ".json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(event_schema)));
    }

    @Test(dependsOnMethods = {"createEvent"})
    public void getSubscriptions() {

        Response response = this.extractResponseAndPrintLog(spec
                .when()
                .get(baseURL + "subscriptions.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(subscriptions_schema)));

        ArrayList subscriptions = response.path("$");
        Assert.assertEquals(subscriptions.size(), 2, "User has some incorrect subscriptions");
    }

    @Test(dependsOnMethods = {"getEventById"})
    public void deleteEvent() {
        this.extractResponseAndPrintLog(spec
                .when()
                .delete(baseURL + "events/" + eventID + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"getEventById"})
    public void deleteSubscription() {
        this.extractResponseAndPrintLog(spec
                .when()
                .delete(baseURL + "subscriptions/" + subscriptionID + ".json")
                .then()
                .statusCode(200));
    }

}
