package com.restApiTests.sessions.providers;



import com.utils.UtilsMethods;
import org.codehaus.jettison.json.JSONException;
import org.testng.annotations.DataProvider;

import java.security.SignatureException;
import java.util.HashMap;
import java.util.Random;

public class NewSessionData {

    public static String applicationId;
    public static String authKey;
    public static String authSecret;

    public static String adminLogin;
    public static String adminPassword;

    private final static String FB = "EAAWbg02jZAwYBAP5rAX3sVDL9HQR32v6MgATSmt793I1mpIGhR2XhKgJwffPhkdCgXy" +
            "B1dUA9gU5vXgU7bcmaEk5ZBzIT5oZConQZBhzPMUspRR1XlMYOovP1ZCrmga6lvBkchzCy42Nq1hZAxE21g1E58z9y5TJcCTGxXvlD1560h9sgUCWvV"; //stagedevelop

    @DataProvider(name = "newSessionWithoutUserData")
    public static Object[][] newSessionWithoutUserData() throws SignatureException, JSONException {

        Random random = new Random();

        String nonce = Integer.toString(random.nextInt());
        long time = System.currentTimeMillis() / 1000;
        String timestamp = Long.toString(time);
        String signature;

        String str = "application_id=" + applicationId + "&" + "auth_key=" + authKey + "&" + "nonce="
                + nonce + "&" + "timestamp=" + timestamp;

        signature = UtilsMethods.calculateHMAC_SHA(str, authSecret);

        HashMap<String, Object> sessionParams = new HashMap<>();
        sessionParams.put("application_id", applicationId);
        sessionParams.put("auth_key", authKey);
        sessionParams.put("nonce", nonce);
        sessionParams.put("timestamp", timestamp);
        sessionParams.put("signature", signature);

        return new Object[][]{{sessionParams}};

    }

    @DataProvider(name = "newSessionWithAdminUserData")
    public static Object[][] newSessionWithAdminUserData() throws SignatureException, JSONException {

        Random random = new Random();

        String nonce = Integer.toString(random.nextInt());
        long time = System.currentTimeMillis() / 1000;
        String timestamp = Long.toString(time);
        String signature;

        String str = "application_id=" + applicationId + "&" + "auth_key=" + authKey + "&" + "nonce="
                + nonce + "&" + "timestamp=" + timestamp + "&" + "user[login]=" + adminLogin + "&" + "user[password]="
                + adminPassword;

        signature = UtilsMethods.calculateHMAC_SHA(str, authSecret);

        HashMap<String, Object> sessionParams = new HashMap<>();
        sessionParams.put("application_id", applicationId);
        sessionParams.put("auth_key", authKey);
        sessionParams.put("nonce", nonce);
        sessionParams.put("timestamp", timestamp);
        sessionParams.put("user[login]", adminLogin);
        sessionParams.put("user[password]", adminPassword);
        sessionParams.put("signature", signature);

        return new Object[][]{{sessionParams}};

    }

    @DataProvider(name = "newSessionWithFacebookProvider")
    public static Object[][] newSessionWithFacebookProvider() throws SignatureException, JSONException {

        Random random = new Random();

        String nonce = Integer.toString(random.nextInt());
        long time = System.currentTimeMillis() / 1000;
        String timestamp = Long.toString(time);
        String signature;

        String str = "application_id=" + applicationId + "&" + "auth_key=" + authKey + "&" + "keys[token]=" + FB + "&" + "nonce="
                + nonce + "&" + "provider=" + "facebook" + "&" + "timestamp=" + timestamp;

        signature = UtilsMethods.calculateHMAC_SHA(str, authSecret);

        HashMap<String, Object> sessionParams = new HashMap<>();
        sessionParams.put("application_id", applicationId);
        sessionParams.put("auth_key", authKey);
        sessionParams.put("nonce", nonce);
        sessionParams.put("timestamp", timestamp);
        sessionParams.put("keys[token]", FB);
        sessionParams.put("provider", "facebook");
        sessionParams.put("signature", signature);

        return new Object[][]{{sessionParams}};

    }



}
