package com.restApiTests.users.providers;


import org.testng.annotations.DataProvider;

import java.util.HashMap;
import java.util.Random;

public class NewApiUserData {

    @DataProvider(name = "newApiUserDataWithAllFields")
    public static Object[][] newApiUserDataWithAllFields() {

        Random random = new Random();

        String login = Integer.toString(random.nextInt(Integer.MAX_VALUE) + 1);

        HashMap<String,Object> user = new HashMap<>();
        user.put("user[login]", login);
        user.put("user[password]", "11111111");
        user.put("user[email]", login + "@test.com");
    //    user.put("user[blob_id]", 4154263);
        user.put("user[external_user_id]", random.nextInt(Integer.MAX_VALUE) + 1);
        user.put("user[facebook_id]", random.nextInt(Integer.MAX_VALUE) + 1);
        user.put("user[twitter_id]", random.nextInt(Integer.MAX_VALUE) + 1);
        user.put("user[full_name]", "test_" + login);
        user.put("user[phone]", "+" + Integer.toString(random.nextInt(Integer.MAX_VALUE) + 1));
        user.put("user[website]", "http://quickblox.com");
        user.put("user[custom_data]", "my_custom_data");
        user.put("user[tag_list]", "first, second, third");

        return new Object[][]{{user}};
    }

    @DataProvider(name = "newApiUserDataWithOnlyRequiredFields")
    public static Object[][] newApiUserDataWithOnlyRequiredFields() {

            Random random = new Random();

            String login = Integer.toString(random.nextInt(Integer.MAX_VALUE) + 1);

            HashMap<String,Object> user = new HashMap<>();
            user.put("user[login]", login + "test");
            user.put("user[password]", "11111111");
            user.put("resultKey", "userLogin1");

        return new Object[][]{{user}};
    }

    @DataProvider(name = "newApiUserDataWithOnlyRequiredFields2")
    public static Object[][] newApiUserDataWithOnlyRequiredFields2() {

        Random random = new Random();

        String login = Integer.toString(random.nextInt(Integer.MAX_VALUE) + 1);

        HashMap<String,Object> user = new HashMap<>();
        user.put("user[login]", login + "test");
        user.put("user[password]", "11111111");
        user.put("resultKey", "userLogin2");

        return new Object[][]{{user}};
    }


}
