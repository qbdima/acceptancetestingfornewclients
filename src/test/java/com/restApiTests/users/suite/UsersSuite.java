package com.restApiTests.users.suite;


import com.restApiTests.core.BaseTest;
import com.restApiTests.sessions.providers.NewSessionData;
import com.restApiTests.users.providers.NewApiUserData;
import io.restassured.response.Response;
import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.HashMap;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;


public class UsersSuite extends BaseTest {

    private String login;
    private String password;
    private int userID;
    private int userID2;
    private String fullName;
    private String facebookID;
    private String twitterID;
    private String email;
    private int externalUserID;

    private String login2;

    private int userIDForDelete;

    ArrayList users;

    private String user_schema;
    private String users_schema;

    @BeforeTest
    public void setUp() {
        try {
            user_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(USER_JSON_SCHEMA), "UTF-8");
            users_schema = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(USERS_JSON_SCHEMA), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test(dependsOnMethods = {"createSession1"}, dataProvider = "newApiUserDataWithAllFields", dataProviderClass = NewApiUserData.class, groups = "baseTest")
    public void createUser1(HashMap user) {

        login = (String) user.get("user[login]");
        password = (String) user.get("user[password]");
        fullName = (String) user.get("user[full_name]");
        facebookID = user.get("user[facebook_id]").toString();
        twitterID = user.get("user[twitter_id]").toString();
        email = (String) user.get("user[email]");
        externalUserID = (int) user.get("user[external_user_id]");

        Response response = this.extractResponseAndPrintLog(spec
                .queryParams(user)
                .when()
                .post(baseURL + "users.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(user_schema)));

        Assert.assertEquals(response.path("user.login"), login);
        Assert.assertEquals(response.path("user.facebook_id"), facebookID);
        Assert.assertEquals(response.path("user.full_name"), fullName);
        Assert.assertEquals(response.path("user.twitter_id"), twitterID);
        Assert.assertEquals(response.path("user.email"), email);
        Assert.assertEquals((int) response.path("user.external_user_id"), externalUserID);
    }

    @Test(dependsOnMethods = {"createUser1"}, groups = "baseTest")
    public void updateSession1() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("login", login)
                .queryParam("password", password)
                .when()
                .post(baseURL + "login.json")
                .then()
                .statusCode(202)
                .assertThat().body(matchesJsonSchema(user_schema)));

        userID = response.path("user.id");
        baseParams.put("userID1", userID);

    }

    @Test(enabled = false)
    public void createSession2(HashMap<String, Object> params) {
    }

    @Test(enabled = false)
    public void createUser2(HashMap<String, Object> params) {
    }

    @Test(enabled = false)
    public void updateSession2() {
    }

    @AfterClass(enabled = false)
    public void deleteUser1() {
    }

    @AfterClass(enabled = false)
    public void deleteUser2() {
    }

    @Test(dependsOnGroups = {"baseTest"}, dataProvider = "newApiUserDataWithOnlyRequiredFields", dataProviderClass = NewApiUserData.class)
    public void createUserWithOnlyRequiredFields(HashMap user) {

        user.put("user[tag_list]", "tmp");

        Response response = this.extractResponseAndPrintLog(spec
                .log().all()
                .queryParams(user)
                .when()
                .post(baseURL + "users.json")
                .then()
                .statusCode(201)
                .assertThat().body(matchesJsonSchema(user_schema)));

        userIDForDelete = response.path("user.id");
        userID2 = response.path("user.id");
        login2 = user.get("user[login]").toString();
    }

    @Test(dependsOnGroups = {"baseTest"})
    public void getUserById() {

        Response response = this.extractResponseAndPrintLog(spec
                .get(baseURL + "users/" + userID + ".json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(user_schema)));

        Assert.assertEquals((int) response.path("user.id"), userID);
        Assert.assertEquals(response.path("user.login"), login);
        Assert.assertEquals(response.path("user.facebook_id"), facebookID);
        Assert.assertEquals(response.path("user.full_name"), fullName);
        Assert.assertEquals(response.path("user.twitter_id"), twitterID);
        Assert.assertEquals(response.path("user.email"), email);
        Assert.assertEquals((int) response.path("user.external_user_id"), externalUserID);
    }

    @Test(dependsOnGroups = {"baseTest"})
    public void getUsers() {

        Response response = this.extractResponseAndPrintLog(spec
                .params("per_page", 100)
                .get(baseURL + "users.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(users_schema)));

        users = response.path("items");
        ArrayList<HashMap<String, HashMap<String, Object>>> items = response.path("items");
        boolean condition = true;
        for (int i = 0; i < items.size() - 1; i++) {
            condition &= (int) items.get(i).get("user").get("id") < (int) items.get(i + 1).get("user").get("id") ? true : false;
        }

        Assert.assertTrue(condition, "By default users should be sort using ask sort");
    }

    @Test(dependsOnGroups = {"baseTest"})
    public void getUsersWithDescSort() {

        Response response = this.extractResponseAndPrintLog(spec
                .params("per_page", 100)
                .params("order", "desc number id")
                .get(baseURL + "users.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(users_schema)));

        ArrayList<HashMap<String, HashMap<String, Object>>> items = response.path("items");
        boolean condition = true;
        for (int i = 0; i < items.size() - 1; i++) {
            condition &= (int) items.get(i).get("user").get("id") > (int) items.get(i + 1).get("user").get("id") ? true : false;
        }

        Assert.assertTrue(condition, "DESK sort works incorrect");
    }

    @Test(dependsOnGroups = {"baseTest"})
    public void getUserByLogin() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("login", login)
                .get(baseURL + "users/by_login.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(user_schema)));

        Assert.assertEquals(response.path("user.login"), login);
    }

    @Test(dependsOnGroups = {"baseTest"})
    public void getUsersByFullName() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("full_name", fullName)
                .get(baseURL + "users/by_full_name.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(users_schema)));

        Assert.assertEquals(response.path("items[0].user.full_name"), fullName);
    }

    @Test(dependsOnGroups = {"baseTest"})
    public void getUserByFacebookIdentifier() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("facebook_id", facebookID)
                .get(baseURL + "users/by_facebook_id.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(user_schema)));

        Assert.assertEquals(response.path("user.facebook_id"), facebookID);
    }

    @Test(dependsOnGroups = {"baseTest"})
    public void getUserByTwitterIdentifier() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("twitter_id", twitterID)
                .get(baseURL + "users/by_twitter_id.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(user_schema)));

        Assert.assertEquals(response.path("user.twitter_id"), twitterID);
    }

    @Test(dependsOnGroups = {"baseTest"})
    public void getUserByEmail() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("email", email)
                .get(baseURL + "users/by_email.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(user_schema)));

        Assert.assertEquals(response.path("user.email"), email);
    }

    @Test(dependsOnGroups = {"baseTest"})
    public void getUsersByTags() {

        Response response = this.extractResponseAndPrintLog(spec
                .queryParam("tags", "first")
                .get(baseURL + "users/by_tags.json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(users_schema)));

        ArrayList<HashMap> users = response.path("items");
        for (HashMap user : users) {
            String tags = (String) ((HashMap) user.get("" +
                    "user")).get("user_tags");
            Assert.assertTrue(tags.contains("first"));
        }
    }

    @Test(dependsOnGroups = {"baseTest"})
    public void getUserByExternalUserId() {

        Response response = this.extractResponseAndPrintLog(spec
                .given()
                .log().all()
                .get(baseURL + "users/external/" + externalUserID + ".json")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchema(user_schema)));

        Assert.assertEquals((int) response.path("user.external_user_id"), externalUserID);
    }


    @Test(dependsOnMethods = {"createUserWithOnlyRequiredFields"})
    public void getUsersBySeveralTags() {

        this.extractResponseAndPrintLog(spec
                .queryParam("tags", "first,tmp")
                .get(baseURL + "users/by_tags.json")
                .then()
                .statusCode(404));
    }

    @Test(dependsOnMethods = {"getUsersBySeveralTags"})
    public void updateUserById() {

        Response response = this.extractResponseAndPrintLog(spec
                        .queryParam("user[full_name]", "updated_user")
                        .queryParam("user[website]", "http://quickblox.com")
                        .queryParam("user[email]", login + "@domain.com")
                        .put(baseURL + "users/" + userID + ".json")
                        .then()
//                .statusCode(200)
//                .assertThat().body(matchesJsonSchema(schema)));
        );

        Assert.assertEquals(response.path("user.full_name"), "updated_user", "User full_name field not updated");
        Assert.assertEquals(response.path("user.website"), "http://quickblox.com", "User website field not updated");
        Assert.assertEquals(response.path("user.email"), login + "@domain.com", "User email field not updated");
    }

    @Test(dependsOnMethods = {"updateUserById"})
    public void resetApiUserPasswordByEmail() {  // This test check only API request, not really sending email!!!

        this.extractResponseAndPrintLog(spec
                .queryParam("email", login + "@domain.com")
                .get(baseURL + "users/password/reset.json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"resetApiUserPasswordByEmail"}, alwaysRun = true)
    public void deleteUser1ByExternalId() {

        this.extractResponseAndPrintLog(spec
                .when()
                .delete(baseURL + "users/external/" + externalUserID + ".json")
                .then()
                .statusCode(200));
    }

    @Test(dependsOnMethods = {"resetApiUserPasswordByEmail"}, alwaysRun = true)
    public void deleteUser2ById() {

        HashMap params = null;
        try {
            params = (HashMap) NewSessionData.newSessionWithoutUserData()[0][0];
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        baseParams.put("customToken", super.createSession(params));

        this.initSpec("customToken");

        this.extractResponseAndPrintLog(spec
                .queryParam("login", login2)
                .queryParam("password", 11111111)
                .when()
                .post(baseURL + "login.json")
                .then()
                .statusCode(202));

        this.extractResponseAndPrintLog(spec
                .when()
                .delete(baseURL + "users/" + userIDForDelete + ".json")
                .then()
                .statusCode(200));
    }

}
